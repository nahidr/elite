
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

function upload_file (id){
	var name = document.getElementById(id).files[0].name;
	var form_data = new FormData();
	var ext = name.split('.').pop().toLowerCase();
	if(jQuery.inArray(ext, ['pdf','png','jpg','jpeg']) == -1) 
	{
		alert("Invalid Image File");
	}
	var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById(id).files[0]);
	var f = document.getElementById(id).files[0];
	var fsize = f.size||f.fileSize;
	if(fsize > 2000000)
	{
		alert("Image File Size is very big");
	}
	else
	{
		form_data.append(id, document.getElementById(id).files[0]);
		console.log(form_data);
		$.ajax({
			url:BASE_URL+"can-upload/"+id,
			method:"POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend:function(){
//				$('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
			},   
			success:function(data)
			{
//				$('#uploaded_image').html(data);
				console.log(data);
				return data;
			}
		});
	}
}

$(".next").click(function(){
	if(animating) return false;
	animating = true;
	
	var clicked = this.id;
	
	var data = new Object();
	
	if(clicked == "can-next1"){
		if($("[name='fullname']:first").val() == ""){
			data.can_full_name = $("[name='fullname']:first").val();
			data.can_phone_number = $("[name='phonenumber']:first").val();
			data.can_address = $("[name='address']:first").val();
			data.can_state = $("[name='state']:first").val();
			data.can_postcode = $("[name='postcode']:first").val();
			data.can_country = $("[name='country']:first").val();
			data.can_education = $("[name='education']:first").val();
			data.can_experience = $("[name='experience']:first").val();
			data.can_about = $("[name='about']:first").val();
			var options = [];
			options[0] = $("[name='opt1']:checked").val();
			options[1] = $("[name='opt2']:checked").val();
			options[2] = $("[name='opt3']:checked").val();
			options[3] = $("[name='opt4']:checked").val();
			options[4] = $("[name='opt5']:checked").val();
			options[5] = $("[name='opt6']").val();
			data.can_options = options;
			var availability = [];
			$("input:checkbox[name='availability[]']:checked").each(function(){
				availability.push($(this).val());
			});
			data.can_availability = availability;
			var conditions = [];
			$("input:checkbox[name='conditions[]']:checked").each(function(){
				conditions.push($(this).val());
			});
			data.can_conditions = conditions;
			var interests = [];
			$("input:checkbox[name='interests[]']:checked").each(function(){
				interests.push($(this).val());
			});
			data.can_interests = interests;
			var send = JSON.stringify(data);
			console.log(send);
//			data.availability = $("[name='availability[]']:checked").val();
//			data.conditions = $("[name='conditions[]']:checked").val();
//			data.interests = $("[name='interests[]']:checked").val();
			$.ajax({
	            type:"post",
	            url: BASE_URL+"add-can-personal-details",
	            data:{ 'data':send},
	            success:function(response)
	            {
	                console.log(response);
//	                alert("Personal Details Saved");
//	                $("#message").html(response);
//	                $('#cartmessage').show();
	            },
	            error: function() 
	            {
//	                alert("Invalide!");
	            }
	        });
//			console.log(data);
		}
		
	}else if(clicked == "can-next2"){
		if($("[name='available_from']:first").val()){
			data.can_contract = $("[name='contract']:first").val();
			data.can_role = $("[name='role']:first").val();
			data.can_preferred_country = $("[name='preferred_country']:first").val();
			data.can_preferred_language = $("[name='preferred_language']:first").val();
			data.can_travel_with_family = $("[name='travel_with_family']:first").val();
			data.can_driver = $("[name='driver']:first").val();
			data.can_swimmer = $("[name='swimmer']:first").val();
			data.can_available_from = $("[name='available_from']:first").val();
			var send = JSON.stringify(data);
			console.log(send);
			$.ajax({
	            type:"post",
	            url: BASE_URL+"add-can-job-requirements",
	            data:{ 'data':send},
	            success:function(response)
	            {
	                console.log(response);
//	                alert("Job Requirements Saved");
//	                $("#message").html(response);
//	                $('#cartmessage').show();
	            },
	            error: function() 
	            {
//	                alert("Invalide!");
	            }
	        });
		}
		
	}else if(clicked == "can-next3"){
		if($("[name='ref1_name']:first").val()){
			cv = upload_file("file_cv");
			id = upload_file("file_id");
			training = upload_file("file_training");
			first_aid = upload_file("file_first_aid");
			var ref = new Object();
			ref.name = $("[name='ref1_name']:first").val();
			ref.title = $("[name='ref1_title']:first").val();
			ref.comapny = $("[name='ref1_company']:first").val();
			ref.phone = $("[name='ref1_phone']:first").val();
			ref.email = $("[name='ref1_email']:first").val();
			data.can_reference1 = JSON.stringify(ref);
			var ref = new Object();
			ref.name = $("[name='ref2_name']:first").val();
			ref.title = $("[name='ref2_title']:first").val();
			ref.comapny = $("[name='ref2_company']:first").val();
			ref.phone = $("[name='ref2_phone']:first").val();
			ref.email = $("[name='ref2_email']:first").val();
			data.can_reference2 = JSON.stringify(ref);
			var send = JSON.stringify(data);
			console.log(send);
			$.ajax({
	            type:"post",
	            url: BASE_URL+"add-can-documents",
	            data:{ 'data':send},
	            success:function(response)
	            {
	                console.log(response);
//	                alert("Documents Saved");
//	                $("#message").html(response);
//	                $('#cartmessage').show();
	            },
	            error: function() 
	            {
//	                alert("Invalide!");
	            }
	        });
		}
		
	}else if(clicked == "parent-next1"){
		var err = 0;
		if($("#title").val() == ""){
			alert("Job Title Cannot Be Empty");
			err = 1;
		}else if($("#type").val() == ""){
			alert("Job Type Cannot Be Empty");
			err = 1;
		}else if($("#position").val() == ""){
			alert("Position Cannot Be Empty");
			err = 1;
		}else if($("#availability").val() == ""){
			alert("Availability Cannot Be Empty");
			err = 1;
		}else if($("#experience").val() == ""){
			alert("Experience Cannot Be Empty");
			err = 1;
		}else if($("#minsal").val() == ""){
			alert("Minimum Salary Cannot Be Empty");
			err = 1;
		}else if($("#maxsal").val() == ""){
			alert("Maximum Salary Cannot Be Empty");
			err = 1;
		}else if($("#country").val() == ""){
			alert("Country Cannot Be Empty");
			err = 1;
		}else if($("#postcode").val() == ""){
			alert("Postcode Cannot Be Empty");
			err = 1;
		}else if($("#language").val() == ""){
			alert("Preferred Language Cannot Be Empty");
			err = 1;
		}else if($("#swimmer").val() == ""){
			alert("Swimmer Cannot Be Empty");
			err = 1;
		}else if($("#travel").val() == ""){
			alert("Willing to Travel Cannot Be Empty");
			err = 1;
		}else if($("#driver").val() == ""){
			alert("Driver Cannot Be Empty");
			err = 1;
		}
	}else if(clicked == "parent-next2"){
		var err = 0;
		if($("#description").val() == ""){
			alert("Job Descrption Cannot Be Empty");
			err = 1;
		}else if($("#requirements").val() == ""){
			alert("Job Requirements Cannot Be Empty");
			err = 1;
		}else if($("#benefits").val() == ""){
			alert("Benefits Cannot Be Empty");
			err = 1;
		}
	}else if(clicked == "parent-next3"){
		var err = 0;
		var selected = $('input[name=option]:checked').val();
		if(selected == 1){
			if($("#paypal_address").val() == ""){
				alert("Paypal Address Cannot Be Empty");
				err = 1;
			}
		}else{
			if($("#card_name").val() == ""){
				alert("Card Name Cannot Be Empty");
				err = 1;
			}else if($("#card_number").val() == ""){
				alert("Card Number Cannot Be Empty");
				err = 1;
			}else if($("#card_expiry_month").val() == ""){
				alert("Card Expiry Month Cannot Be Empty");
				err = 1;
			}else if($("#card_expiry_year").val() == ""){
				alert("Card Expiry Year Cannot Be Empty");
				err = 1;
			}else if($("#card_cvv").val() == ""){
				alert("Card CVV Cannot Be Empty");
				err = 1;
			}
		}
		if(err == 0){
			var secret = $("#secret").val();
			var data = new Object();
			data.parent_job_title = $("#title").val();
			data.parent_job_type = $("#type").val();
			data.parent_position = $("#position").val();
			data.parent_availability = $("#availability").val();
			data.parent_experience = $("#experience").val();
			data.parent_min_salary = $("#minsal").val();
			data.parent_max_salary = $("#maxsal").val();
			data.parent_country = $("#country").val();
			data.parent_postcode = $("#postcode").val();
			data.parent_preferred_language = $("#language").val();
			data.parent_swimmer = $("#swimmer").val();
			data.parent_travel = $("#travel").val();
			data.parent_driver = $("#driver").val();
			data.tag = secret;
			var send = JSON.stringify(data);
			console.log(send);
			$.ajax({
	            type:"post",
	            url: BASE_URL+"add-parent-basic-info",
	            data:{ 'data':send},
	            success:function(response)
	            {
	                console.log(response);
	            },
	            error: function() 
	            {
	            }
	        });
			var data = new Object();
			data.parent_job_description = $("#description").val();
			data.parent_job_requirements = $("#requirements").val();
			data.parent_benefits = $("#benefits").val();
			data.tag = secret;
			var send = JSON.stringify(data);
			console.log(send);
			$.ajax({
	            type:"post",
	            url: BASE_URL+"add-parent-job-requirements",
	            data:{ 'data':send},
	            success:function(response)
	            {
	                console.log(response);
	            },
	            error: function() 
	            {
	            }
	        });
			var data = new Object();
			var selected = $('input[name=option]:checked').val();
			data.parent_payment_mode = selected;
			if(selected == 1){
				data.parent_paypal_address = $("#paypal_address").val();
			}else{
				data.parent_card_name = $("#card_name").val();
				data.parent_card_number = $("#card_number").val();
				data.parent_card_exp_month = $("#card_expiry_month").val();
				data.parent_card_exp_year = $("#card_expiry_year").val();
				data.parent_card_cvv = $("#card_cvv").val();
			}
			data.tag = secret;
			var send = JSON.stringify(data);
			console.log(send);
			$.ajax({
	            type:"post",
	            url: BASE_URL+"add-parent-payment-details",
	            data:{ 'data':send},
	            success:function(response)
	            {
	                console.log(response);
	            },
	            error: function() 
	            {
	            }
	        });
		}
	}
	
	if(err == 0){
		current_fs = $(this).parent();
		next_fs = $(this).parent().next();
		
		//activate next step on progressbar using the index of next_fs
		$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
		
		//show the next fieldset
		next_fs.show(); 
		
		current_fs.hide();
	}
	
	animating = false;
	
});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.hide();
	animating = false;
});

$(".radio").click(function(){
	
	var selected = $('input[name=option]:checked').val();
//	alert(selected);
	if(selected == 1){
		$("#paypal_address").prop('disabled', false);
		$("#card_name").prop('disabled', true);
		$("#card_number").prop('disabled', true);
		$("#card_expiry_month").prop('disabled', true);
		$("#card_expiry_year").prop('disabled', true);
		$("#card_cvv").prop('disabled', true);
	}else{
		$("#paypal_address").prop('disabled', true);
		$("#card_name").prop('disabled', false);
		$("#card_number").prop('disabled', false);
		$("#card_expiry_month").prop('disabled', false);
		$("#card_expiry_year").prop('disabled', false);
		$("#card_cvv").prop('disabled', false);
	}

});

$( document ).ready(function() {//$(".next").click();
//	registration_step = 3;
	if (typeof registration_step !== 'undefined') {
		if(registration_step == 1){
			$("#li1").addClass("active");
			$("#li2").removeClass("active");
			$("#li3").removeClass("active");
			$("#li4").removeClass("active");
			$("#f1").show();
			$("#f2").hide();
			$("#f3").hide();
			$("#f4").hide();
			next_fs = $("#f2");
			current_fs = $("#f1");
			previous_fs = $("#f1");
		}else if(registration_step == 2){
			$("#li1").addClass("active");
			$("#li2").addClass("active");
			$("#li3").removeClass("active");
			$("#li4").removeClass("active");
			$("#f1").hide();
			$("#f2").show();
			$("#f3").hide();
			$("#f4").hide();
			next_fs = $("#f3");
			current_fs = $("#f2");
			previous_fs = $("#f1");
		}else if(registration_step == 3){
			$("#li1").addClass("active");
			$("#li2").addClass("active");
			$("#li3").addClass("active");
			$("#li4").removeClass("active");
			$("#f1").hide();
			$("#f2").hide();
			$("#f3").show();
			$("#f4").hide();
			next_fs = $("#f4");
			current_fs = $("#f3");
			previous_fs = $("#f2");
		}else if(registration_step == 4){
			$("#li1").addClass("active");
			$("#li2").addClass("active");
			$("#li3").addClass("active");
			$("#li4").addClass("active");
			$("#f1").hide();
			$("#f2").hide();
			$("#f3").hide();
			$("#f4").show();
			next_fs = $("#f4");
			current_fs = $("#f4");
			previous_fs = $("#f3");
		}
	}
	
	
//	registration_step = 2;
//	for(var i = 1; i < registration_step; i++){
//		console.log(i);
//		$(".next").click();
//	}
//	setIntervalX(function () {
//		$(".next").click();
//		console.log(changes);
//	}, 100, changes);
});

function setIntervalX(callback, delay, repetitions) {
    var x = 0;
    var intervalID = window.setInterval(function () {

       callback();

       if (++x === repetitions) {
           window.clearInterval(intervalID);
       }
    }, delay);
}

$(".submit").click(function(){
	return false;
})