-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 12 mars 2019 à 09:03
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `elite`
--

-- --------------------------------------------------------

--
-- Structure de la table `tbl_activityfeed`
--

DROP TABLE IF EXISTS `tbl_activityfeed`;
CREATE TABLE IF NOT EXISTS `tbl_activityfeed` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `created_at` timestamp NOT NULL,
  `status` tinyint(1) NOT NULL,
  `user_fk` int(11) NOT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_can_documents`
--

DROP TABLE IF EXISTS `tbl_can_documents`;
CREATE TABLE IF NOT EXISTS `tbl_can_documents` (
  `documents_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id_fk` varchar(45) DEFAULT NULL,
  `can_cv` varchar(256) DEFAULT NULL,
  `can_id` varchar(256) DEFAULT NULL,
  `can_training` varchar(256) DEFAULT NULL,
  `can_first_aid` varchar(256) DEFAULT NULL,
  `can_reference1` varchar(512) DEFAULT NULL,
  `can_reference2` varchar(512) DEFAULT NULL,
  `can_video` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`documents_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_can_documents`
--

INSERT INTO `tbl_can_documents` (`documents_id`, `user_id_fk`, `can_cv`, `can_id`, `can_training`, `can_first_aid`, `can_reference1`, `can_reference2`, `can_video`) VALUES
(10, '14', '1549283841_139_generational business mind logo.png', '1549283841_785_generational business mind logo.png', '1549283841_187_generational business mind logo.png', '1549283841_714_generational business mind logo.png', '{\"name\":\"1\",\"title\":\"2\",\"comapny\":\"3\",\"phone\":\"4\",\"email\":\"5\"}', '{\"name\":\"6\",\"title\":\"7\",\"comapny\":\"8\",\"phone\":\"9\",\"email\":\"0\"}', '1549288801_210_SampleVideo_1280x720_1mb.mp4');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_can_interested`
--

DROP TABLE IF EXISTS `tbl_can_interested`;
CREATE TABLE IF NOT EXISTS `tbl_can_interested` (
  `parent_user_id_fk` varchar(45) NOT NULL,
  `candidate_user_id_fk` varchar(45) NOT NULL,
  `tag_fk` varchar(45) NOT NULL,
  `interested_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `interested_status` varchar(45) DEFAULT '0',
  PRIMARY KEY (`tag_fk`,`candidate_user_id_fk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_can_interested`
--

INSERT INTO `tbl_can_interested` (`parent_user_id_fk`, `candidate_user_id_fk`, `tag_fk`, `interested_on`, `interested_status`) VALUES
('13', '14', '1', '2019-02-15 13:36:33', '0'),
('13', '14', '10983_1549876196', '2019-02-15 13:36:23', '0');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_can_job_requirement`
--

DROP TABLE IF EXISTS `tbl_can_job_requirement`;
CREATE TABLE IF NOT EXISTS `tbl_can_job_requirement` (
  `can_job_requirement_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id_fk` varchar(45) NOT NULL,
  `can_contract` varchar(45) DEFAULT NULL,
  `can_role` varchar(45) DEFAULT NULL,
  `can_preferred_country` varchar(45) DEFAULT NULL,
  `can_preferred_language` varchar(45) DEFAULT NULL,
  `can_travel_with_family` varchar(45) DEFAULT NULL,
  `can_driver` varchar(45) DEFAULT NULL,
  `can_swimmer` varchar(45) DEFAULT NULL,
  `can_available_from` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`can_job_requirement_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_can_job_requirement`
--

INSERT INTO `tbl_can_job_requirement` (`can_job_requirement_id`, `user_id_fk`, `can_contract`, `can_role`, `can_preferred_country`, `can_preferred_language`, `can_travel_with_family`, `can_driver`, `can_swimmer`, `can_available_from`) VALUES
(1, '15', '1 Year', 'Fully Involved', 'Uk', 'English', 'Yes', 'Yes', 'Yes', '2019-02-01'),
(4, '14', '1 Year', 'Fully Involved', 'Uk', 'English', 'Yes', 'Yes', 'Yes', '2019-02-04'),
(5, '15', '1 Year', 'Fully Involved', 'Uk', 'English', 'Yes', 'Yes', 'Yes', '2019-02-04');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_can_personal_details`
--

DROP TABLE IF EXISTS `tbl_can_personal_details`;
CREATE TABLE IF NOT EXISTS `tbl_can_personal_details` (
  `personal_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id_fk` int(11) NOT NULL,
  `can_full_name` varchar(128) DEFAULT NULL,
  `can_phone_number` varchar(45) DEFAULT NULL,
  `can_address` varchar(256) DEFAULT NULL,
  `can_state` varchar(45) DEFAULT NULL,
  `can_postcode` varchar(45) DEFAULT NULL,
  `can_country` varchar(45) DEFAULT NULL,
  `can_education` varchar(45) DEFAULT NULL,
  `can_experience` varchar(45) DEFAULT NULL,
  `can_about` varchar(45) DEFAULT NULL,
  `can_options` varchar(1024) DEFAULT NULL,
  `can_availability` varchar(1024) DEFAULT NULL,
  `can_conditions` varchar(1024) DEFAULT NULL,
  `can_interests` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`personal_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_can_personal_details`
--

INSERT INTO `tbl_can_personal_details` (`personal_details_id`, `user_id_fk`, `can_full_name`, `can_phone_number`, `can_address`, `can_state`, `can_postcode`, `can_country`, `can_education`, `can_experience`, `can_about`, `can_options`, `can_availability`, `can_conditions`, `can_interests`) VALUES
(2, 14, 'Syed Moazzam Ali Shah', '3144491119', 'CB148, Bilal Lane, Khuwaja Corporation Chowk, Adyala Road, Rawalpindi,, Punjab, Pakistan', 'Punjab', '46000', 'Uk', 'Masters', 'Many', 'qwerty', '[\"1\",\"0\",\"1\",\"0\",\"1\",\"Native\"]', '[\"1\",\"2\",\"3\"]', '[\"1\",\"3\",\"5\",\"7\",\"8\"]', '[\"1\",\"2\",\"3\"]');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_can_shortlisted`
--

DROP TABLE IF EXISTS `tbl_can_shortlisted`;
CREATE TABLE IF NOT EXISTS `tbl_can_shortlisted` (
  `tag_fk` varchar(45) NOT NULL,
  `candidate_user_id_fk` varchar(45) NOT NULL,
  `shortlisted_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tag_fk`,`candidate_user_id_fk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_can_shortlisted`
--

INSERT INTO `tbl_can_shortlisted` (`tag_fk`, `candidate_user_id_fk`, `shortlisted_on`) VALUES
('1', '14', '2019-02-15 08:20:27');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_messages`
--

DROP TABLE IF EXISTS `tbl_messages`;
CREATE TABLE IF NOT EXISTS `tbl_messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `sent_to` int(11) NOT NULL,
  `sent_by` int(11) NOT NULL,
  `message_text` mediumtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_byR` tinyint(1) NOT NULL,
  `deleted_byS` tinyint(1) NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_messages`
--

INSERT INTO `tbl_messages` (`message_id`, `sent_to`, `sent_by`, `message_text`, `created_at`, `is_read`, `deleted_byR`, `deleted_byS`) VALUES
(1, 15, 14, 'ghg edghbdjhvsf gdjhjsbf', '2019-03-04 20:43:14', 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_parent_basic_info`
--

DROP TABLE IF EXISTS `tbl_parent_basic_info`;
CREATE TABLE IF NOT EXISTS `tbl_parent_basic_info` (
  `parent_basic_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id_fk` varchar(45) DEFAULT NULL,
  `parent_job_title` varchar(255) DEFAULT NULL,
  `parent_job_type` varchar(255) DEFAULT NULL,
  `parent_position` varchar(255) DEFAULT NULL,
  `parent_availability` varchar(255) DEFAULT NULL,
  `parent_experience` varchar(255) DEFAULT NULL,
  `parent_min_salary` varchar(255) DEFAULT NULL,
  `parent_max_salary` varchar(255) DEFAULT NULL,
  `parent_country` varchar(255) DEFAULT NULL,
  `parent_postcode` varchar(255) DEFAULT NULL,
  `parent_preferred_language` varchar(255) DEFAULT NULL,
  `parent_swimmer` varchar(255) DEFAULT NULL,
  `parent_travel` varchar(255) DEFAULT NULL,
  `parent_driver` varchar(255) DEFAULT NULL,
  `tag` varchar(45) DEFAULT NULL,
  `live_status` varchar(45) DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`parent_basic_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_parent_basic_info`
--

INSERT INTO `tbl_parent_basic_info` (`parent_basic_info_id`, `user_id_fk`, `parent_job_title`, `parent_job_type`, `parent_position`, `parent_availability`, `parent_experience`, `parent_min_salary`, `parent_max_salary`, `parent_country`, `parent_postcode`, `parent_preferred_language`, `parent_swimmer`, `parent_travel`, `parent_driver`, `tag`, `live_status`, `created`, `updated`) VALUES
(1, '13', 'Nanny to 2 and 5 year old kids', 'full time', 'nanny', '9 to 6', '5', '3000', '5000', 'uk', '123456', 'English/Spanish', 'No Preference', 'Once a year', 'Preferred', '1', '1', '2019-02-10 10:03:41', '2019-02-13 10:31:16'),
(4, '13', 'generic title', 'full time', 'caretaker', '24/7', '5', '3000', '5000', 'uk', '123456', 'English', 'no', 'no', 'yes', '10983_1549876196', '1', '2019-02-13 10:03:41', '0000-00-00 00:00:00'),
(5, '13', 'Tutor for a 12 years old kid', '1', '3', '3', '2', '1500', '2099', '1', 'N.A.', '1', '2', '3', '1', '59260_1550224378', '1', '2019-02-15 09:55:15', '2019-02-15 09:56:57');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_parent_favourite`
--

DROP TABLE IF EXISTS `tbl_parent_favourite`;
CREATE TABLE IF NOT EXISTS `tbl_parent_favourite` (
  `parent_user_id_fk` varchar(45) NOT NULL,
  `candidate_user_id_fk` varchar(45) NOT NULL,
  `favourited_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`parent_user_id_fk`,`candidate_user_id_fk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_parent_favourite`
--

INSERT INTO `tbl_parent_favourite` (`parent_user_id_fk`, `candidate_user_id_fk`, `favourited_on`) VALUES
('13', '14', '2019-02-13 13:15:45');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_parent_jobs`
--

DROP TABLE IF EXISTS `tbl_parent_jobs`;
CREATE TABLE IF NOT EXISTS `tbl_parent_jobs` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_fk` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `job_position` varchar(60) NOT NULL,
  `job_type` varchar(30) NOT NULL,
  `job_title` varchar(200) NOT NULL,
  `job_description` text NOT NULL,
  `job_requirements` text NOT NULL,
  `job_benefits` text NOT NULL,
  `experience` varchar(30) NOT NULL,
  `salary_min` float NOT NULL,
  `salary_max` float NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country` varchar(25) NOT NULL,
  `language` varchar(15) NOT NULL,
  `is_swimmer` tinyint(1) NOT NULL,
  `is_traveller` tinyint(1) NOT NULL,
  `is_driver` tinyint(1) NOT NULL,
  `paid` tinyint(1) NOT NULL,
  `payment_method` varchar(10) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_parent_job_requirement`
--

DROP TABLE IF EXISTS `tbl_parent_job_requirement`;
CREATE TABLE IF NOT EXISTS `tbl_parent_job_requirement` (
  `parent_job_requirement_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id_fk` varchar(45) NOT NULL,
  `parent_job_description` text,
  `parent_job_requirements` text,
  `parent_benefits` text,
  `tag` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`parent_job_requirement_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_parent_job_requirement`
--

INSERT INTO `tbl_parent_job_requirement` (`parent_job_requirement_id`, `user_id_fk`, `parent_job_description`, `parent_job_requirements`, `parent_benefits`, `tag`) VALUES
(2, '15', 'some description', 'some requirements', 'some benefits', '1'),
(3, '15', '1', '1', '1', '2'),
(4, '15', '1', '1', '1', '3'),
(5, '15', 'Job Description', 'Job Requirements', 'Benefits', '10983_1549876196'),
(6, '15', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '59260_1550224378');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_parent_payment`
--

DROP TABLE IF EXISTS `tbl_parent_payment`;
CREATE TABLE IF NOT EXISTS `tbl_parent_payment` (
  `parent_payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id_fk` varchar(45) DEFAULT NULL,
  `parent_payment_mode` varchar(45) DEFAULT NULL,
  `parent_paypal_address` varchar(45) DEFAULT NULL,
  `parent_card_name` varchar(255) DEFAULT NULL,
  `parent_card_number` varchar(45) DEFAULT NULL,
  `parent_card_exp_month` varchar(45) DEFAULT NULL,
  `parent_card_exp_year` varchar(45) DEFAULT NULL,
  `parent_card_cvv` varchar(45) DEFAULT NULL,
  `tag` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`parent_payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_parent_payment`
--

INSERT INTO `tbl_parent_payment` (`parent_payment_id`, `user_id_fk`, `parent_payment_mode`, `parent_paypal_address`, `parent_card_name`, `parent_card_number`, `parent_card_exp_month`, `parent_card_exp_year`, `parent_card_cvv`, `tag`) VALUES
(1, '13', '2', NULL, 'jon doe', '1234567890', '12', '21', '333', '1'),
(2, '13', '1', '1', NULL, NULL, NULL, NULL, NULL, '2'),
(3, '13', '1', '1', NULL, NULL, NULL, NULL, NULL, '3'),
(4, '13', '2', NULL, 'Jon Doe', '1213141516', '12', '22', '333', '10983_1549876196'),
(5, '13', '2', NULL, 'abc', '123', '12', '99', '123', '59260_1550224378');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_payments`
--

DROP TABLE IF EXISTS `tbl_payments`;
CREATE TABLE IF NOT EXISTS `tbl_payments` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `txn_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payment_gross` float(10,2) NOT NULL,
  `currency_code` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payer_email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_payments`
--

INSERT INTO `tbl_payments` (`payment_id`, `user_id`, `product_id`, `txn_id`, `payment_gross`, `currency_code`, `payer_email`, `payment_status`) VALUES
(1, 14, 1, '4E934744K78520838', 19.99, 'GBP', 'syedmoazzamalishah123@gmail.com', 'Completed'),
(2, 13, 2, '5V240360G1881573P', 79.99, 'GBP', 'syedmoazzamalishah123@gmail.com', 'Completed'),
(3, 59260, 2, '0EY70040D6494020K', 79.99, 'GBP', 'syedmoazzamalishah123@gmail.com', 'Completed');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_products`
--

DROP TABLE IF EXISTS `tbl_products`;
CREATE TABLE IF NOT EXISTS `tbl_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` float(10,2) NOT NULL,
  `status` enum('1','0') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_products`
--

INSERT INTO `tbl_products` (`id`, `name`, `image`, `price`, `status`) VALUES
(1, 'Registration Fee', '', 19.99, '1'),
(2, 'Vacancy Creation Fee', '', 79.99, '1');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45) DEFAULT NULL,
  `user_first_name` varchar(45) DEFAULT NULL,
  `user_last_name` varchar(45) DEFAULT NULL,
  `user_email` varchar(45) NOT NULL,
  `user_password` varchar(45) NOT NULL,
  `user_contact` varchar(45) DEFAULT NULL,
  `user_type` int(11) NOT NULL DEFAULT '3',
  `user_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_last_logged_in` timestamp NULL DEFAULT NULL,
  `user_status` int(11) NOT NULL DEFAULT '0',
  `user_about` text,
  `user_picture` varchar(255) DEFAULT 'review.png',
  `temp_pin` int(11) DEFAULT NULL,
  `temp_email` varchar(255) DEFAULT NULL,
  `user_live` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_name`, `user_first_name`, `user_last_name`, `user_email`, `user_password`, `user_contact`, `user_type`, `user_created`, `user_updated`, `user_last_logged_in`, `user_status`, `user_about`, `user_picture`, `temp_pin`, `temp_email`, `user_live`) VALUES
(13, NULL, 'Moazzam', 'Ali', 'moazzam@weblinerz.co.uk', '3cc31cd246149aec68079241e71e98f6', NULL, 2, '2019-01-30 11:53:41', '2019-03-01 08:02:37', NULL, 1, NULL, '1551426772_286_testimonials7.jpg', 15682, 'moazzam@weblinerz.co.ukkk', 1),
(14, NULL, 'Moazzam', 'Ali', 'potioneye6020@gmail.com', '3cc31cd246149aec68079241e71e98f6', NULL, 1, '2019-01-31 10:10:06', '2019-03-01 07:50:32', NULL, 1, NULL, '1551426632_504_testimonials1.jpg', 72283, '', 1),
(15, NULL, 'Elisa', 'Anderson', 'andersonelisea49@gmail.com', '733d7be2196ff70efaf6913fc8bdcabf', NULL, 2, '2019-03-04 17:42:54', '2019-03-11 12:36:18', NULL, 1, NULL, '1551733329_397_logo-3.jpg', 18014, NULL, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
