<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Candidate extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->helper('email');
        $this->load->model('Candidate_model');
        ob_clean();
    }
    
    public function view_jobs($filtered = "") {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 1) {
            if($filtered == ""){
                $jobs = $this->Candidate_model->get_jobs();
            }else{
                $jobs = $filtered;
            }
            $data['jobs'] = $jobs;
            $this->session->set_userdata("page_name", "View Jobs");
            $this->load->view('candidate/header');
            $this->load->view('candidate/viewjobs',$data);
            $this->load->view('candidate/footer');
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Candidate Login");
            redirect(base_url('candidate-login'));
        }
    }
    
    public function search_jobs() {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 1) {
            $this->form_validation->set_rules('keyword', 'Kekword', 'trim');
            $this->form_validation->set_rules('category', 'Category', 'trim');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata("error_msg", validation_errors());
                redirect(base_url('view-jobs'));
            } else {
                $key = $this->input->post();
                if($key['keyword'] == "" && $key['category'] == ""){
                    redirect(base_url('view-jobs'));
                }else{
                    $this->session->set_userdata("key", $key);
                    $data = $this->Candidate_model->search_jobs($key);
                    if($data){
                        $this->view_jobs($data);
                    }else{
                        redirect(base_url('view-jobs'));
                    }
                }
            }
            
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Candidate Login");
            redirect(base_url('candidate-login'));
        }
    }
    
    public function shortlisted_jobs() {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 1) {
            $jobs = $this->Candidate_model->get_shortlisted_jobs($this->session->userdata['user']['user_id']);
            $data['jobs'] = $jobs;
            $this->session->set_userdata("page_name", "Shortlisted Jobs");
            $this->load->view('candidate/header');
            $this->load->view('candidate/shortlisted',$data);
            $this->load->view('candidate/footer');
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Candidate Login");
            redirect(base_url('candidate-login'));
        }
    }
    
    public function inbox() {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 1) {
            $this->session->set_userdata("page_name", "Candidate Inbox");
            $this->load->view('candidate/header');
            $this->load->view('candidate/inbox');
            $this->load->view('candidate/footer');
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Candidate Login");
            redirect(base_url('candidate-login'));
        }
    }
    
    public function settings() {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 1) {
            $this->session->set_userdata("page_name", "Candidate Settings");
            $this->load->view('candidate/header');
            $this->load->view('settings');
            $this->load->view('candidate/footer');
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Candidate Login");
            redirect(base_url('candidate-login'));
        }
    }
    
    public function register() {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 1) {
            $data['user_id_fk'] = $this->session->userdata['user']['user_id'];
            $status = $this->Candidate_model->registration_status($data);
            if($status != 5){
                $data['status'] = $status;
                $this->session->set_userdata("page_name", "Candidate registration");
                $this->load->view('candidate/header',$data);
                $this->load->view('candidate/registration');
                $this->load->view('candidate/footer');
            }else{
                $check = $this->Candidate_model->check_live_status($this->session->userdata['user']['user_id']);
                if(!$check){
                    $this->Candidate_model->send_registration_email($this->session->userdata['user']);
                }
                redirect(base_url('candidate-dash'));
            }
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Candidate Login");
            redirect(base_url('candidate-login'));
        }
    }
    
    public function add_personal_details() {
//         echo $this->input->post("data"); exit;
        $data = json_decode($this->input->post("data"), true);
        $data['can_options'] = json_encode($data['can_options']);
        $data['can_availability'] = json_encode($data['can_availability']);
        $data['can_conditions'] = json_encode($data['can_conditions']);
        $data['can_interests'] = json_encode($data['can_interests']);
//         print_r($data);exit;
        $data['user_id_fk'] = $this->session->userdata['user']['user_id'];
        $result = $this->Candidate_model->add_personal_details($data);
        return $result;
    }
    
    public function add_job_requirements() {
        //         echo $this->input->post("data"); exit;
        $data = json_decode($this->input->post("data"), true);
        $data['user_id_fk'] = $this->session->userdata['user']['user_id'];
//         print_r($data);exit;
        $result = $this->Candidate_model->add_job_requirements($data);
        return $result;
    }
    
    public function add_documents() {
        //         echo $this->input->post("data"); exit;
        $data = json_decode($this->input->post("data"), true);
        $data['user_id_fk'] = $this->session->userdata['user']['user_id'];
        //         print_r($data);exit;
        $result = $this->Candidate_model->add_documents($data);
        return $result;
    }
    
    public function add_video() {
        //         echo $this->input->post("data"); exit;
        $temp_name = time()."_".rand(100, 999) . '_' . $_FILES['file_video']["name"];
        $location = "./uploads/candidate/" . $temp_name;
        move_uploaded_file($_FILES['file_video']["tmp_name"], $location);
        $data['user_id_fk'] = $this->session->userdata['user']['user_id'];
        $data['can_video'] = $temp_name;
        $result = $this->Candidate_model->add_documents($data);
        redirect(base_url('candidate-registration'));
                
    }
    
    public function shortlist($tag) {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 1) {
            $candidate_id = $this->session->userdata['user']['user_id'];
            $this->Candidate_model->toggle_shortlisted($candidate_id,$tag);
            $prev = $this->session->userdata("page_name");
            if($prev == "View Jobs"){
                redirect(base_url('view-jobs'));
            }else{
                redirect(base_url('shortlisted-jobs'));
            }
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Candidate Login");
            redirect(base_url('candidate-login'));
        }
    }
    
    public function show_interest($tag) {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 1) {
            $candidate_id = $this->session->userdata['user']['user_id'];
            $this->Candidate_model->show_interest($candidate_id,$tag);
            $prev = $this->session->userdata("page_name");
            if($prev == "View Jobs"){
                redirect(base_url('view-jobs'));
            }else{
                redirect(base_url('shortlisted-jobs'));
            }
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Candidate Login");
            redirect(base_url('candidate-login'));
        }
    }
    
    public function upload($name) {
        $temp_name = time()."_".rand(100, 999) . '_' . $_FILES[$name]["name"];
        $location = "./uploads/candidate/" . $temp_name;
        move_uploaded_file($_FILES[$name]["tmp_name"], $location);
        $data = array();
        $result = $temp_name;
        switch($name){
            case "file_cv":
                $data['can_cv'] = $result;
                break;
            case "file_id":
                $data['can_id'] = $result;
                break;
            case "file_training":
                $data['can_training'] = $result;
                break;
            case "file_first_aid":
                $data['can_first_aid'] = $result;
                break;
        }
        $data['user_id_fk'] = $this->session->userdata['user']['user_id'];
        //         print_r($data);exit;
        $res = $this->Candidate_model->add_documents($data);
        return $res;
    }
    
    public function upload_old($name) {
        $config = array(
            'upload_path' => base_url()."uploads/candidate",
            // 	        'allowed_types' => "gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp",
            'allowed_types' => "pdf|jpg|jpeg|png",
            'overwrite' => TRUE,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
        );
        
        $this->load->library('upload');
//         echo json_encode(($_FILES));exit;
        $files = $_FILES;
        // 	    print_r($files);
        $cpt = count($_FILES[$name]['name']);
        $upload = array();
        for($i=0; $i<$cpt; $i++)
        {
            $temp_name = time()."_".$files[$name]['name'][$i];
            $_FILES[$name]['name']= $temp_name;
            $_FILES[$name]['type']= $files[$name]['type'][$i];
            $_FILES[$name]['tmp_name']= $files[$name]['tmp_name'][$i];
            $_FILES[$name]['error']= $files[$name]['error'][$i];
            $_FILES[$name]['size']= $files[$name]['size'][$i];
            
            $this->upload->initialize($config);
            if($this->upload->do_upload($name)){
                array_push($upload,$temp_name);
            }
        }
        $data = array();
        $result = json_encode($upload);
        switch($name){
            case "file_cv":
                $data['can_cv'] = $result;
                break;
            case "file_id":
                $data['can_id'] = $result;
                break;
            case "file_training":
                $data['can_training'] = $result;
                break;
            case "file_first_aid":
                $data['can_first_aid'] = $result;
                break;
        }
        $data['user_id_fk'] = $this->session->userdata['user']['user_id'];
//         print_r($data);exit;
        $res = $this->Candidate_model->add_documents($data);
        return $res;
    }
    
//     public function add_personal_details() {
//         $data = $this->input->post();
//         $data['user_id_fk'] = $this->session->userdata['user']['user_id'];
//         $result = $this->Candidate_model->add_personal_detail($data);
//         return $result;
//     }
    
//     public function add_personal_details() {
//         $data = $this->input->post();
//         $data['user_id_fk'] = $this->session->userdata['user']['user_id'];
//         $result = $this->Candidate_model->add_personal_detail($data);
//         return $result;
//     }
    
}