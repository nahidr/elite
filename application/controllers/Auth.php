<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->helper('email');
        $this->load->model('Auth_model');
        ob_clean();
    }

    public function chk_password_expression($str){
        if (1 !== preg_match("/^.*(?=.{6,})/", $str)) 
        {
            $this->form_validation->set_message('chk_password_expression', '%s must be at least 6 characters');
            return FALSE;
        }
        else    {
            return TRUE;
        }
    }

    public function check_user() { 
        $this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $user = $this->input->post('user_type');
            if($user == 1){
                $this->session->set_userdata("page_name", "Candidate Login");
                $this->load->view('header');
                $this->load->view('login-candidate');
                $this->load->view('footer');
            }else if($user == 2){
                $this->session->set_userdata("page_name", "Parent Login");
                $this->load->view('header');
                $this->load->view('login-parent');
                $this->load->view('footer');
            }
        } else {
            $user = $this->Auth_model->checkUser($this->input->post('user_email'), $this->input->post('user_password')); 
            $this->session->set_userdata("page_name", "");

            if ($user['login'] == "1") {
                $arrayuser = array(
                    'user_id' => $user['user_id'],
                    'user_first_name' => $user['user_first_name'],
                    'user_last_name' => $user['user_last_name'],
                    'user_name' => $user[''],
                    'user_password' => $user['user_password'],
                    'user_type' => $user['user_type'],
                    'user_status' => $user['user_status'],
                    'user_email' => $user['user_email'],
                    'user_picture' => $user['user_picture']
                ); 
                $this->session->set_userdata("user", $arrayuser);
                
                if($this->input->post('user_type') == $user['user_type']){
                    if ($user['user_type'] == 2) {
                        redirect(base_url('parent-dashboard'));
                    } else if ($user['user_type'] == 1) {
                        redirect(base_url('candidate-registration'));
                    }
                }
            } elseif ($user['login'] == "0") { 
                $this->session->set_flashdata('error_msg', 'The email/password combination is incorrect!');
                $type = $this->input->post('user_type');
                if ($type == 2) {
                    redirect(base_url('parent-login'));
                } else if ($type == 1) {
                    redirect(base_url('candidate-login'));
                }
            }else{ 
                $this->session->set_flashdata('error_msg', 'Please check your inbox to activate your account!');
                $type = $this->input->post('user_type');
                if ($type == 2) {
                    redirect(base_url('parent-login'));
                } else if ($type == 1) {
                    redirect(base_url('candidate-login'));
                }
            }
        }
    }

    public function index(){
        $this->session->set_userdata("page_name", "Home");
        $this->load->view('header');
        $this->load->view('home');
        $this->load->view('footer');
    }
    
    public function dashboard($user = 2)  {
        $data['interested'] = $this->Auth_model->get_interested($this->session->userdata['user']['user_id'],$user);  
        $data['activities'] = $this->Auth_model->getActivities($this->session->userdata['user']['user_id']);
        $data['messages'] = $this->Auth_model->getTopMessages($this->session->userdata['user']['user_id']);

        if($user == 1){
            $this->session->set_userdata("page_name", "Candidate Dashboard");
            $this->load->view('candidate/header');
            $this->load->view('candidate/dashboard',$data);
            $this->load->view('candidate/footer');
        }else if($user == 2){
            $this->session->set_userdata("page_name", "Parent Dashboard");
            $this->load->view('parent/header');
            $this->load->view('parent/dashboard-parent',$data);
            $this->load->view('parent/footer');
        }
    }
    
    public function mark_read(){
        $type = $this->session->userdata['user']['user_type'];
        $this->Auth_model->mark_read($this->session->userdata['user']['user_id'],$type);
        if($type == 1){
            redirect(base_url('candidate-dashboard'));
        }else{
            redirect(base_url('parent-dashboard'));
        }
    }

    public function login($user = 2) {
        if($user == 1){
            $this->session->set_userdata("page_name", "Candidate Log In");
            $this->load->view('header');
            $this->load->view('login-candidate');
            $this->load->view('footer');
        }else if($user == 2){
            $this->session->set_userdata("page_name", "Parent Log In");
            $this->load->view('header');
            $this->load->view('login-parent');
            $this->load->view('footer');
        }
    }

    public function signup($user = 2)  {
        if($user == 1){
            $this->session->set_userdata("page_name", "Candidate Sign Up");
            $this->load->view('header');
            $this->load->view('signup-candidate');
            $this->load->view('footer');
        }else if($user == 2){
            $this->session->set_userdata("page_name", "Parent Sign Up");
            $this->load->view('header');
            $this->load->view('signup-parent');
            $this->load->view('footer');
        }
    }

    public function logout() {
        $this->session->unset_userdata("user");
        redirect(base_url());
    }

    public function about()  {
        $this->session->set_userdata("page_name", "About");
        $this->load->view('header');
        $this->load->view('about');
        $this->load->view('footer');
    }

    public function contact()  {
        $this->session->set_userdata("page_name", "Contact");
        $this->load->view('header');
        $this->load->view('contact');
        $this->load->view('footer');
    }
    
    public function gdpr()  {
        $this->session->set_userdata("page_name", "GDPR");
        $this->load->view('header');
        $this->load->view('GDPR');
        $this->load->view('footer');
    }
    
    public function hiring_process()  {
        $this->session->set_userdata("page_name", "Hiring Process");
        $this->load->view('header');
        $this->load->view('hiring-process');
        $this->load->view('footer');
    }
    
    public function employer_toolkit()  {
        $this->session->set_userdata("page_name", "Employer Toolkit");
        $this->load->view('header');
        $this->load->view('employer-toolkit');
        $this->load->view('footer');
    }
    
    public function household_staff() {
        $this->session->set_userdata("page_name", "Household Staff");
        $this->load->view('header');
        $this->load->view('household-staff');
        $this->load->view('footer');
    } 
    
    public function cdp() {
        $this->session->set_userdata("page_name", "CDP");
        $this->load->view('header');
        $this->load->view('CDP');
        $this->load->view('footer');
    }
    
    public function get_hired() {
        $this->session->set_userdata("page_name", "Get Hired");
        $this->load->view('header');
        $this->load->view('get-hired');
        $this->load->view('footer');
    }

    public function home() {
        $this->index();
    }

    public function register()  {  
        $this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email|is_unique[tbl_user.user_email]');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required|min_length[6]|max_length[15]|callback_chk_password_expression');
        $this->form_validation->set_rules('user_password_confirm', 'Confirm Password', 'trim|required|matches[user_password]');
        $this->form_validation->set_rules('user_first_name', 'First Name', 'required');
        $this->form_validation->set_rules('user_last_name', 'Last Name', 'required');

        if ($this->form_validation->run() == FALSE) {
            $type = $this->input->post('user_type');
            if($type == 1){
                $this->session->set_userdata("page_name", "Candidate SignUp");
                $this->session->set_flashdata('error_msg', "Unsuccessful registration. Please try again.");
                $this->load->view('header');
                $this->load->view('signup-candidate');
                $this->load->view('footer');
            }else if($type == 2){
                $this->session->set_userdata("page_name", "Parent SignUp");
                $this->session->set_flashdata('error_msg', "Unsuccessful registration. Please try again.");
                $this->load->view('header');
                $this->load->view('signup-parent');
                $this->load->view('footer');
            }
        } else {
            $username = $this->input->post('user_name');
            $firstname = $this->input->post('user_first_name');
            $lastname = $this->input->post('user_last_name');
            $email = $this->input->post('user_email');
            $password = md5($this->input->post('user_password'));
            $type = $this->input->post('user_type');
            $status = 0; // require user email verification

            $data = array(
                "user_name" => $username,
                "user_first_name" => $firstname,
                "user_last_name" => $lastname,
                "user_email" => $email,
                "user_password" => $password,
                "user_type" => $type,
                "user_status" => $status
            );

            $user = $this->Auth_model->register($data);

            if ($user) {
                if($type = 2){
                    $this->session->set_userdata("page_name", "Parent Log In");
                    $this->session->set_flashdata('success_msg', "Registered Successfully. Please check your inbox and activate your account.");
                    redirect(base_url('parent-login'));
                }else if($type == 1){
                    $this->session->set_userdata("page_name", "Candidate Log In");
                    $this->session->set_flashdata('success_msg', "Registered Successfully. Please check your inbox and activate your account.");
                    redirect(base_url('candidate-login'));
                }
                
            } else {
                if($type = 2){
                    $this->session->set_userdata("page_name", "Candidate SignUp");
                    $this->session->set_flashdata('error_msg', "Unsuccessful registration. Please try again.");
                    redirect(base_url('candidate-signup'));
                }else if($type == 1){
                    $this->session->set_userdata("page_name", "Parent SignUp");
                    $this->session->set_flashdata('error_msg', "Unsuccessful registration. Please try again.");
                    redirect(base_url('parent-signup'));
                }
            }
        }
    }

    public function profile()  {
        if (isset($this->session->userdata['user']['user_id'])) {
            $user_id = $this->session->userdata['user']['user_id'];
            $data = $this->Auth_model->get_user_data($user_id);
            $this->session->set_userdata("page_name", "My Profile");
            $this->load->view('header');
            $this->load->view('profile', $data);
            $this->load->view('footer');
        }
    }
    
    public function update_user_info() {
        $data = $this->input->post();
        $type = $this->session->userdata['user']['user_type'];
        $email = $this->session->userdata['user']['user_email'];
        $route = ($type == 1)? "can-settings" : "parent-settings";
        $new_email = $data['user_email'];
        
        $data['user_id'] = $this->session->userdata['user']['user_id'];
        
        if($new_email == $email){
            $result = $this->Auth_model->update_user($data);
            if($result){
                $this->session->set_flashdata('success_msg', "Changes Saved.");
            }else{
                $this->session->set_flashdata('error_msg', "Changes Could Not Be Saved.");
            }
        }else{
            $pin = $this->Auth_model->generatePIN();
            $data['temp_email'] = $new_email;
            $data['temp_pin'] = $pin;
            unset($data['user_email']);
            $result = $this->Auth_model->update_user($data);
            if($result){
                $result = $this->Auth_model->sendEmailChangeEmail($pin,$new_email,$email);
            }
            if($result){
                $this->session->set_flashdata('success_msg', "Email Sent to Original Email. Please Verify To Change The Email Address.");
            }else{
                $this->session->set_flashdata('error_msg', "Changes Could Not Be Made.");
            }
        }
        $this->refresh_session();
        redirect(base_url($route));
    }

    public function update_user_password()  {
        $data = $this->input->post();
        $type = $this->session->userdata['user']['user_type'];
        $route = ($type == 1)? "can-settings" : "parent-settings"; 
        $data['user_id'] = $this->session->userdata['user']['user_id']; 
        $user = $this->Auth_model->checkUser($this->session->userdata['user']['user_email'], $data['user_password']); 
        if ($user['login'] == "1") {
            $this->form_validation->set_rules('user_password', 'Password', 'trim|required');
            $this->form_validation->set_rules('new_password', 'Password', 'trim|required|min_length[6]|max_length[15]|callback_chk_password_expression');
            $this->form_validation->set_rules('confirm_new_password', 'Confirm Password', 'trim|required|matches[new_password]');
            $this->form_validation->set_message('chk_password_expression', '%s must be at least 6 characters & must contain at least one lower case letter, one upper case letter and one digit');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_msg', validation_errors());
                redirect(base_url($route));
            } else {
                $data['user_password'] = md5($data['new_password']);
                unset($data['new_password']);
                unset($data['confirm_new_password']); 
                $result = $this->Auth_model->update_user($data);
                if($result){
                    $this->session->set_flashdata('success_msg', "Password Changed.");
                }else{
                    $this->session->set_flashdata('error_msg', "Password Not Changed.");
                }
            }
        } else {
            $this->session->set_flashdata('error_msg', "Incorrect Current Password.");
        } 
        redirect(base_url($route)); 
    }
    
    public function refresh_session(){
        $user = $this->Auth_model->get_user_data($this->session->userdata['user']['user_id']); 
        
        $arrayuser = array(
            'user_id' => $user['user_id'],
            'user_first_name' => $user['user_first_name'],
            'user_last_name' => $user['user_last_name'],
            'user_name' => $user['user_name'],
            'user_password' => $user['user_password'],
            'user_type' => $user['user_type'],
            'user_status' => $user['user_status'],
            'user_email' => $user['user_email'],
            'user_picture' => $user['user_picture']
        ); 
        $this->session->set_userdata("user", $arrayuser);
    }
    
    public function change_email($pin){
        $this->Auth_model->change_email($pin);
        redirect(base_url("parent-login"));
    }

    public function delete() {
        $result = $this->Auth_model->delete_user();
        $this->session->set_flashdata('success_msg', "Profile Successfully Deleted.");
        if ($result) {
            redirect(base_url('logout'));
        }
    }

    public function validateEmail($pin, $code){ 
        $userdets = $this->Auth_model->validateEmail($pin, $code); 

        $arrayuser = array(
            'user_id' => $userdets->user_id,
            'user_first_name' => $userdets->user_first_name,
            'user_last_name' => $userdets->user_last_name,
            'user_name' => '',
            'user_password' => $userdets->user_password,
            'user_type' => $userdets->user_type,
            'user_status' => $userdets->user_status,
            'user_email' => $userdets->user_email,
            'user_picture' => $userdets->user_picture
        ); 
        $this->session->set_userdata("user", $arrayuser); 
        
        if($userdets){ 
            if($userdets->user_type==2){ 
                redirect(base_url('parent-dashboard'));
            } else {
                redirect(base_url('candidate-registration'));
            }  
        }else{ 
            if($userdets->user_type==2){  
                $this->session->set_flashdata('error_msg', "Account Not Activated! Please Try Again.");
                redirect(base_url('parent-login'));
            } else{
                $this->session->set_flashdata('error_msg', "Account Not Activated! Please Try Again.");
                redirect(base_url('candidate-login'));
            }  
        }  
    }

    public function recover_password() {
        $email = $this->input->post("user_email");

        $result = $this->Auth_model->recover_email($email);
        if ($result) {
            $this->session->set_flashdata('success_msg', "Password Reset Email Sent! Please use the email we have sent to " . $email . " in order to reset your password and log in.");
        } else {
            $this->session->set_flashdata('error_msg', "Password Reset Email Could Not Be Sent! Please Try Again.");
        }
        redirect(base_url('login'));
    }

    public function reset_password($pin, $code) {
        $validated = $this->Auth_model->reset_password($pin, $code);
        if ($validated) {
            $this->session->set_flashdata('success_msg', "Password Reset Successful! Please Choose your New Password.");
            $this->session->set_userdata("temp_pin", $pin);
            redirect(base_url('password-reset-form'));
        } else {
            $this->session->set_flashdata('error_msg', "Password Reset Unsuccessful Due to Data Mismatch! Please Try Again.");
            redirect(base_url('login'));
        }
    }

    public function password_reset_form()
    {
        $this->session->set_userdata("page_name", "Password Reset");
        $this->load->view('header');
        $this->load->view('password-reset');
        $this->load->view('footer');
    }

    public function password_reset()  {
        $data = $this->input->post();

        if ($data['user_password'] != "") {
            if ($data['confirm_user_password'] != "") {
                if ($data['user_password'] == $data['confirm_user_password']) {
                    $this->form_validation->set_rules('user_password', 'Password', 'trim|required|min_length[6]|max_length[15]|callback_chk_password_expression');
                    if ($this->form_validation->run() == FALSE) {
                        $this->session->set_flashdata('error_msg', "New Password Does Not Fit Criteria.");
                    } else {
                        $data = $this->input->post();
                        $password = md5($data['user_password']);
                        $pin = $data['temp_pin'];
                        $email = $data['user_email'];
                        // print_r($data);exit;
                        $result = $this->Auth_model->set_new_password($password, $pin, $email);
                        if ($result) {
                            $this->session->set_flashdata('success_msg', "Password Changed Successfully. You can now log in.");
                        } else {
                            $this->session->set_flashdata('error_msg', "Password Change Unsuccessful. Please Try Again");
                        }
                    }
                } else {
                    $this->session->set_flashdata('error_msg', "New Password Values Do Not Match.");
                }
            } else {
                $this->session->set_flashdata('error_msg', "Confirm New Password.");
            }
        } else {
            $this->session->set_flashdata('error_msg', "Enter New Password.");
        }
        redirect(base_url('login'));
    }

    public function send_email() {
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', "Email Not Sent.");
        } else {
            $data = $this->input->post();
            $this->Auth_model->sendemail($data['message'], $data['email'], $data['name']);
            $this->session->set_flashdata('success_msg', "Email Sent Successfully.");
        }
        redirect(base_url('contact'));
    }
    
    public function change_profile_picture($type=1) {
        if($type==1){ //candidate
            if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 1) {
                $temp_name = time()."_".rand(100, 999) . '_' . $_FILES["userfile"]["name"];
                $location = "./uploads/candidate/" . $temp_name;
                move_uploaded_file($_FILES["userfile"]["tmp_name"], $location);
                $data = array();
                $data['user_picture'] = $temp_name;
                $data['user_id'] = $this->session->userdata['user']['user_id'];
                $res = $this->Auth_model->update_user($data);
                $this->refresh_session();
                echo 1;
            }else{
                echo 0;
            }
        }else if($type==2){ //parent
            if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 2) {
                $temp_name = time()."_".rand(100, 999) . '_' . $_FILES["userfile"]["name"];
                $location = "./uploads/parent/" . $temp_name;
                move_uploaded_file($_FILES["userfile"]["tmp_name"], $location);
                $data = array();
                $data['user_picture'] = $temp_name;
                $data['user_id'] = $this->session->userdata['user']['user_id'];
                $res = $this->Auth_model->update_user($data);
                $this->refresh_session();
                echo 1;
            }else{
                echo 0;
            }
        }
    }
    
}
