<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paypal extends CI_Controller{
    
     function  __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->helper('email');
        // Load paypal library & product model
        $this->load->library('paypal_lib');
        $this->load->model('product');
     }
     
    function success(){
        // Get the transaction data
        $paypalInfo = $this->input->post();
        
//         print_r($paypalInfo);
        
        $data['item_name']      = $paypalInfo['item_name'];
        $data['item_number']    = $paypalInfo['item_number'];
        $data['txn_id']         = $paypalInfo["txn_id"];
        $data['payment_amt']    = $paypalInfo["mc_gross"];
        $data['currency_code']  = $paypalInfo["mc_currency"];
        $data['status']         = $paypalInfo["payment_status"];
        $data['id']             = $paypalInfo["custom"];
        
        $save['user_id']        = $paypalInfo["custom"];
        $save['product_id']        = $paypalInfo["item_number"];
        $save['txn_id']            = $paypalInfo["txn_id"];
        $save['payment_gross']    = $paypalInfo["mc_gross"];
        $save['currency_code']    = $paypalInfo["mc_currency"];
        $save['payer_email']    = $paypalInfo["payer_email"];
        $save['payment_status'] = $paypalInfo["payment_status"];
        
        $this->product->insertTransaction($save);
        
        if($paypalInfo["item_number"] == 1){
            if($paypalInfo["payment_status"] == "Completed"){
                $this->load->model('Candidate_model');
                $this->Candidate_model->change_user_live_status($paypalInfo["custom"],1);
            }
        }else if($paypalInfo["item_number"] == 2){
            if($paypalInfo["payment_status"] == "Completed"){
                $this->load->model('Parent_model');
                $this->Parent_model->change_job_live_status($paypalInfo["custom"],1);
            }
        }
        // Pass the transaction data to view
        $this->load->view('paypal/success', $data);
    }
     
     function cancel(){
        // Load payment failed view
        $this->load->view('paypal/cancel');
     }
     
     function ipn(){
        // Paypal posts the transaction data
         $paypalInfo = $this->input->post();
//          print_r($paypalInfo);exit;
        
        if(!empty($paypalInfo)){
            // Validate and get the ipn response
            $ipnCheck = $this->paypal_lib->validate_ipn($paypalInfo);
//             $ipnCheck = true;
            // Check whether the transaction is valid
            if($ipnCheck){
                // Insert the transaction data in the database
                $data['user_id']        = $paypalInfo["custom"];
                $data['product_id']        = $paypalInfo["item_number"];
                $data['txn_id']            = $paypalInfo["txn_id"];
                $data['payment_gross']    = $paypalInfo["mc_gross"];
                $data['currency_code']    = $paypalInfo["mc_currency"];
                $data['payer_email']    = $paypalInfo["payer_email"];
                $data['payment_status'] = $paypalInfo["payment_status"];

                $this->product->insertTransaction($data);
                
                if($paypalInfo["payment_status"] == "Completed"){
                    $this->load->model('Candidate_model');
                    $this->Candidate_model->change_user_live_status($paypalInfo["custom"],1);
                }
            }
        }
    }
}