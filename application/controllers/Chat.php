<?php defined('BASEPATH') or exit('No direct script access allowed');

class Chat extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->helper('email');
        $this->load->model('Chat_model');
        ob_clean();
    }
    
    public function get_all(){ 
        $id = $this->session->userdata['user']['user_id'];
        $type = $this->session->userdata['user']['user_type'];
        $data['messages'] = $this->Chat_model->get_user_messages($id,$type);
        if($type==1){
            $this->session->set_userdata("page_name", "Candidate Inbox");
            $this->load->view('candidate/header');
            $this->load->view('inbox',$data);
            $this->load->view('candidate/footer');
        }else if($type==2){
            $this->session->set_userdata("page_name", "Parent Inbox");
            $this->load->view('parent/header');
            $this->load->view('inbox',$data);
            $this->load->view('parent/footer');
        }
    }
    
    public function send(){ 
        $id = $this->session->userdata['user']['user_id'];
        $type = $this->session->userdata['user']['user_type'];
        $messages = $this->Chat_model->get_user_messages($type); 
    }
    
}