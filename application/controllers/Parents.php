<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Parents extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->helper('email');
        $this->load->model('Parent_model');
        ob_clean();
    }
    
    public function view_profiles() {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 2) {
            $this->session->set_userdata("page_name", "View Profiles");
            $profiles = $this->Parent_model->get_all_profiles(); 
            $data['profiles'] = $profiles;
            $this->load->view('parent/header');
            $this->load->view('parent/searchprofiles',$data);
            $this->load->view('parent/footer');
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Parent Login");
            redirect(base_url('parent-login'));
        }
    }
    
    public function view_candidate($id) {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 2) {
            $this->session->set_userdata("page_name", "View Candidate");
            $candidate = $this->Parent_model->get_profile_info($id);
            $candidate['favourite'] = $this->Parent_model->check_favourite($id,$this->session->userdata['user']['user_id']);
            $data = $candidate;
            $this->load->view('parent/header');
            $this->load->view('parent/candidate-profile',$data);
            $this->load->view('parent/footer');
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Parent Login");
            redirect(base_url('parent-login'));
        }
    }
    
    public function favourited() {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 2) {
            $favourited = $this->Parent_model->get_favourite_profiles($this->session->userdata['user']['user_id']);
            $data['profiles'] = $favourited;
//             print_r($data);
            $this->session->set_userdata("page_name", "Favourited");
            $this->load->view('parent/header');
            $this->load->view('parent/favourited',$data);
            $this->load->view('parent/footer');
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Parent Login");
            redirect(base_url('parent-login'));
        }
    }
    
    public function my_jobs() {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 2) {
            $data['jobs'] = $this->Parent_model->get_my_jobs($this->session->userdata['user']['user_id']);
              
            $this->session->set_userdata("page_name", "My Jobs");
            $this->load->view('parent/header');
            $this->load->view('parent/my-jobs',$data);
            $this->load->view('parent/footer');
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Parent Login");
            redirect(base_url('parent-login'));
        }
    }
    
    public function settings() {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 2) {
            $this->session->set_userdata("page_name", "Parent Settings");
            $this->load->view('parent/header');
            $this->load->view('settings');
            $this->load->view('parent/footer');
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Parent Login");
            redirect(base_url('parent-login'));
        }
    }
    
    public function advertize_vacancy() {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 2) {
            $data['user_id_fk'] = $this->session->userdata['user']['user_id'];
            $this->session->set_userdata("page_name", "Advertize Vacancy");
            $this->load->view('parent/header',$data);
            $this->load->view('parent/create_vacancy');
            $this->load->view('parent/footer');
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Parent Login");
            redirect(base_url('parent-login'));
        }
    }
    
    public function payment($tag) {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 2) {
            $user_id = $this->session->userdata['user']['user_id'];
            $data = $this->Parent_model->get_payment_details($user_id,$tag);
            $this->session->set_userdata("page_name", "Payment");
            $this->load->view('parent/header');
            $this->load->view('parent/payment',$data);
            $this->load->view('parent/footer');
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Parent Login");
            redirect(base_url('parent-login'));
        }
    }
    
    public function add_basic_info() {
//         echo $this->input->post("data"); exit;
        $data = json_decode($this->input->post("data"), true);
//         print_r($data);exit;
        $data['user_id_fk'] = $this->session->userdata['user']['user_id'];
        $result = $this->Parent_model->add_basic_info($data);
        return $result;
    }
    
    public function add_job_requirements() {
        //         echo $this->input->post("data"); exit;
        $data = json_decode($this->input->post("data"), true);
        $data['user_id_fk'] = $this->session->userdata['user']['user_id'];
//         print_r($data);exit;
        $result = $this->Parent_model->add_job_requirements($data);
        return $result;
    }
    
    public function add_payment_details() {
        //         echo $this->input->post("data"); exit;
        $data = json_decode($this->input->post("data"), true);
        $data['user_id_fk'] = $this->session->userdata['user']['user_id'];
        //         print_r($data);exit;
        $result = $this->Parent_model->add_payment_details($data);
        return $result;
    }
    
    public function favourite($candiate_id) {
        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 2) {
            $parent_id = $this->session->userdata['user']['user_id'];
            $this->Parent_model->toggle_favorite($candiate_id,$parent_id);
            redirect(base_url('view-candidate/'.$candiate_id));
        }else{
            $this->session->unset_userdata("user");
            $this->session->set_userdata("page_name", "Parent Login");
            redirect(base_url('parent-login'));
        }
    }
    
}