<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
$route['default_controller'] = 'auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['home'] = 'auth/index';
$route['contact'] = 'auth/contact';
$route['about'] = 'auth/about';
$route['gdpr'] = 'auth/gdpr';
$route['hiring-process'] = 'auth/hiring_process';
$route['employer-toolkit'] = 'auth/employer_toolkit';
$route['household-staff'] = 'auth/household_staff';
$route['performance-management'] = 'auth/performance_management';
$route['cdp'] = 'auth/cdp';
$route['get-hired'] = 'auth/get_hired';
$route['candidate-login'] = 'auth/login/1';
$route['parent-login'] = 'auth/login/2';
$route['candidate-signup'] = 'auth/signup/1';
$route['parent-signup'] = 'auth/signup/2';
$route['register'] = 'auth/register';
$route['login'] = 'auth/check_user';
$route['candidate-dashboard'] = 'auth/dashboard/1';
$route['parent-dashboard'] = 'auth/dashboard/2';
$route['logout'] = 'auth/logout';
$route['mark-read'] = 'auth/mark_read';
$route['update-profile-picture/(:num)'] = 'auth/change_profile_picture/$1';

$route['change-user-info'] = 'auth/update_user_info';
$route['change-user-password'] = 'auth/update_user_password';
$route['change-email/(:any)'] = 'auth/change_email/$1';

$route['products/buy/(:any)/(:any)'] = 'products/buy/$1/$2';
$route['products/buy/(:any)'] = 'products/buy/$1';

$route['candidate-registration'] = 'candidate/register';
$route['add-can-personal-details'] = 'candidate/add_personal_details';
$route['add-can-job-requirements'] = 'candidate/add_job_requirements';
$route['add-can-documents'] = 'candidate/add_documents';
$route['save-video'] = 'candidate/add_video';
$route['can-upload/(:any)'] = 'candidate/upload/$1';
$route['view-jobs'] = 'candidate/view_jobs';
$route['shortlisted-jobs'] = 'candidate/shortlisted_jobs'; 

$route['can-settings'] = 'candidate/settings';
$route['shortlist/(:any)'] = 'candidate/shortlist/$1';
$route['search-jobs'] = 'candidate/search_jobs';
$route['show-interest/(:any)'] = 'candidate/show_interest/$1';

$route['advertise-vacancy'] = 'parents/advertize_vacancy';
$route['add-parent-basic-info'] = 'parents/add_basic_info';
$route['add-parent-job-requirements'] = 'parents/add_job_requirements';
$route['add-parent-payment-details'] = 'parents/add_payment_details';
$route['products/buy/(:any)'] = 'products/buy/$1';
$route['view-profiles'] = 'parents/view_profiles';
$route['parent-favourited'] = 'parents/favourited';
$route['my-jobs'] = 'parents/my_jobs'; 

$route['parent-settings'] = 'parents/settings';
$route['view-candidate/(:any)'] = 'parents/view_candidate/$1';
$route['payment/(:any)'] = 'parents/payment/$1';
$route['favourite/(:any)'] = 'parents/favourite/$1';

$route['can-inbox'] = 'chat/get_all';
$route['parent-inbox'] = 'chat/get_all';


$route['validateEmail/(:any)/(:any)'] = 'auth/validateEmail/$1/$2';
