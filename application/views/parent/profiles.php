<div class="container pb-5">
    <div class="row text-center pt-5 pb-5">
    	<?php
    	if(is_array($profiles)){
    	    foreach ($profiles as $profile){
    	        $options = json_decode($profile['options'], true);
    	        $lang = ($options[4] == 1)? "English" : "Other";
    	        $profile['user_created'] = date('F j, Y', strtotime($profile['user_created']));
    	        $profile['user_last_logged_in'] = ($profile['user_last_logged_in'] = " ")? "Never Logged In" : $profile['user_last_logged_in'];
    	?>
        <div class="col-md-4">
            <div class="card-details">
                <div class="profile-img text-center">
                    <img src="<?= base_url() ?>uploads/candidate/<?=$profile['user_picture']?>">
                </div>
                <div class="profile-text">
                    <div class="profile-name p-4">
                        <h3><?= $profile['name'] ?></h3>
                    </div>
                    <div class="profile-details text-left pt-3" style='color:#242424'>
                        <h4><b>Talent ID:</b> <?= $profile['temp_pin'] ?></h4>
                        <h4><b>Date Joined:</b> <?= $profile['user_created'] ?></h4>
                        <h4><b>Last login:</b> <?= $profile['user_last_logged_in'] ?></h4>
                        <h4><b>Nationality:</b> <?= $profile['nationality'] ?></h4>
                        <h4><b>Native Language:</b> <?= $lang ?></h4>
                        <h4><b>Current Location:</b> <?= $profile['currentlocation'] ?></h4>
                    </div>
                    <div class="btns pt-4 pb-4">
                        <a href="<?= base_url("view-candidate/".$profile['id']) ?>" class="site-btn m-2">View Profile</a>
                        <a href="<?= base_url("parent-inbox") ?>" class="site-btn-1 m-2">Contact me</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
    	    }
    	}
    	?>
	</div>
</div>