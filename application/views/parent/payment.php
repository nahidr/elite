	<style>
        input[type=text], select{
            width: 100%;
            padding: 7px;
            border: 1px solid #ccc;
            border-radius: 0px;
            box-sizing: border-box;
            margin-bottom: 16px;
            resize: vertical;
        }
        textarea {
            outline: 0;
            width: 100%;
            padding: 7px;
            border: 1px solid #ccc;
            border-radius: 20px;
            box-sizing: border-box;
            margin-bottom: 16px;
            resize: none;
        }
        .site-btn {
            padding: 12px 20px !important;
            min-width: 100px;
            color: #fff !important;
        }
        .pull-right{
            float: right !important;
        }
        .deactivate-btn{
            font-family: Segoe UI;
            position: relative;
            display: inline-block;
            padding: 12px 0px;
            font-size: 14px;
            font-weight: 600;
            line-height: 16px;
            letter-spacing: 2px;
            border-radius: 20px;
            min-width: 200px;
            text-align: center;
            background: #5c0100;
            cursor: pointer;
            border: none;
            color: #fff;
        }
        input.radio {
            padding: 8px;
            border: 1px solid #ccc;
            border-radius: 0px;
            margin-bottom: 10px;
            width: 20px;
            display: inline-block;
            box-sizing: border-box;
            color: #2C3E50;
            font-size: 15px;
        }
        h3.total{
            color: #666;
            font-weight: 600;
        }
        h3.price{
            font-weight: 600;
            color: #e5b4a5;
        }
        .border-bottom {
            border-bottom: 2px solid #e6e6f2 !important;
        }

        </style>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-md-12 mt-3">
                            <div class="page-header pl-3 pt-3">
                                <!-- <h2 class="pageheader-title">Dashboard</h2> -->
                            </div>
                        </div>
                        <div class="col-lg-8 mx-auto p-2">
                            <div class="hiring-box pt-3 pb-3">
                                <div class="container">
                                  <div class="row">
                                      <div class="col-md-8 mx-auto mt-4 border-bottom">
                                          <div class="row">
                                              <div class="col-md-6 text-center">
                                                  <h3 class="total">TOTAL</h3>
                                              </div>
                                              <div class="col-md-6 text-center">
                                                  <h3 class="price">&pound; 79.99</h3>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="row pt-3 pb-3">
                                      <div class="col-md-6 mx-auto">
                                          <div class="row">
                                              <div class="col-md-6 text-center">
                                                  <label><input type="radio" id="radio1" name="type" class="radio" value="Fresher"> Bank Card</label>
                                              </div>
                                              <div class="col-md-6 text-center">
                                                  <label><input type="radio" name="type" class="radio" value="Experienced"> PayPal</label>
                                              </div>
                                          </div>
                                      </div>
                                  </div>


                                  <div id="textboxes" style="display: none">
										<div class="col-md-12"  style="text-align: center;">
                                            <a href="<?= base_url("products/buy/".$tag."/2") ?>" class="site-btn">Process Payment</a>
                                        </div>
                                  </div>
                                  <div id="textboxes1" style="display: none">
                                    <div class="row pt-3">
                                        <div class="col-md-10 mx-auto text-center">
                                            <div class="row">
                                                <div class="col-md-6 text-left pb-3">
                                                    <h3 class="m-0 mb-1">Name of Cardholder</h3>
                                                    <input type="text" name="fname" value="<?= $parent_card_name ?>"/>
                                                </div>
                                                <div class="col-md-6 text-left pb-3">
                                                    <h3 class="m-0 mb-1">Card Number</h3>
                                                    <input type="text" name="fname" value="<?= $parent_card_number ?>"/>
                                                </div>
                                                <div class="col-md-6 text-left pb-3">
                                                    <h3 class="m-0 mb-1">Expiry Date</h3>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input type="text" name="fname" value="<?= $parent_card_exp_month ?>"/>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" name="fname" value="<?= $parent_card_exp_year ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-left pb-3">
                                                    <h3 class="m-0 mb-1">CVV</h3>
                                                    <input type="text" name="fname" value="<?= $parent_card_cvv ?>"/>
                                                </div>
                                                <div class="col-md-12" style="text-align: center;">
                                                    <a href="" class="site-btn">Process Payment</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>