<link rel="stylesheet" href="<?= base_url() ?>assets/css/multiforms.css">
<style>
    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 700;
        font-size: 18px;
        color: #666;
    }
</style> 
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content">
                    <div class="row">
                        <div class="col-md-10 hiring-box mx-auto">
                        	<?php
                        	$num = mt_rand(10000,99999);
                        	$time = time();
                        	$secret = $num."_".$time;
                        	?>
                        	<input type="hidden" id="secret" name="secret" value="<?= $secret ?>">
                            <form id="msform">
                                <!-- progressbar -->
                                <ul id="progressbar">
                                    <li class="active">Basic Information</li>
                                    <li>Job Requirements</li>
                                    <li>Payment</li>
                                    <li>Submit</li>
                                </ul>
                                <!-- fieldsets -->
                                <fieldset>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12 text-left pb-3">
                                                <h3 class="m-0 mb-1">Job Title</h3>
                                                <input type="text" id="title" name="title"/>
                                            </div>
                                            <div class="col-md-4 text-left pb-3">
                                                <h3 class="m-0 mb-1">Job Type</h3>
                                                <select class="" id="type" name="type"> 
                                                   <option value="1">Day Care</option>
                                                   <option value="2">Night Care</option>
                                                   <option value="3">Full-Time</option>
                                                   <option value="4">Stay-In (Weekend Only)</option>
                                             	</select>
                                            </div>
                                            <div class="col-md-4 text-left pb-3">
                                                <h3 class="m-0 mb-1">Position</h3>
                                                <select class="" id="position" name="position"> 
                                                   <option value="1">Nanny</option>
                                                   <option value="2">Personal Assistant</option>
                                                   <option value="3">Governess/Tutor/Teacher</option>
                                                   <option value="4">Maternity Nurse</option>
                                                   <option value="5">Housekeeper</option>
                                                   <option value="6">Chauffer</option>
                                                   <option value="7">Butler</option>
                                             	</select>
                                            </div>
                                            <div class="col-md-4 text-left pb-3">
                                                <h3 class="m-0 mb-1">Availability</h3>
                                                <select class="" id="availability" name="availability"> 
                                                   <option value="1">As Soon As Possible</option>
                                                   <option value="2">In Two Weeks</option>
                                                   <option value="3">From The Start Of Coming Month</option>
                                             	</select>
                                            </div>

                                            <div class="col-md-4 text-left pb-3">
                                                <h3 class="m-0 mb-1">Years of Experience</h3>
                                                <input type="number" id="experience" name="experience"/>
                                            </div>
                                            <div class="col-md-4 text-left pb-3">
                                                <h3 class="m-0 mb-1">Min Salary</h3>
                                                <input type="number" id="minsal" name="minsal"/>
                                            </div>
                                            <div class="col-md-4 text-left pb-3">
                                                <h3 class="m-0 mb-1">Max Salary</h3>
                                                <input type="number" id="maxsal" name="maxsal"/>
                                            </div>

                                            <div class="col-md-4 text-left pb-3">
                                                <h3 class="m-0 mb-1">Country</h3>
                                                <select class="" id="country" name="country"> 
                                                   <option value="1">England</option>
                                                   <option value="2">Scotland</option>
                                                   <option value="3">Ireland</option>
                                                   <option value="4">Wales</option>
                                                   <option value="5">Isle of Man</option>
                                             	</select>
                                            </div>
                                            <div class="col-md-4 text-left pb-3">
                                                <h3 class="m-0 mb-1">Postcode</h3>
                                                <input type="text" id="postcode" name="postcode"/>
                                            </div>
                                            <div class="col-md-4 text-left pb-3">
                                                <h3 class="m-0 mb-1">Preffered Language</h3>
                                                <select class="" id="language" name="language"> 
                                                   <option value="1">English</option>
                                                   <option value="2">Welsh</option>
                                                   <option value="3">Polish</option>
                                                   <option value="4">Other</option>
                                             	</select>
                                            </div>

                                            <div class="col-md-4 text-left pb-3">
                                                <h3 class="m-0 mb-1">Swimmer?</h3>
                                                <select class="" id="swimmer" name="swimmer">
                                                   <option value="">Please Select</option>
                                                   <option value="1">Yes</option>
                                                   <option value="2">No</option>
                                                   <option value="3">Maybe</option>
                                             	</select>
                                            </div>
                                            <div class="col-md-4 text-left pb-3">
                                                <h3 class="m-0 mb-1">Willing to Travel</h3>
                                                <select class="" id="travel" name="travel">
                                                   <option value="">Please Select</option>
                                                   <option value="1">Yes</option>
                                                   <option value="2">No</option>
                                                   <option value="3">Maybe</option>
                                             	</select>
                                            </div>
                                            <div class="col-md-4 text-left pb-3">
                                                <h3 class="m-0 mb-1">Driver</h3>
                                                <select class="" id="driver" name="driver"> 
                                                   <option value="1">Yes</option>
                                                   <option value="2">No</option>
                                                   <option value="3">Maybe</option>
                                             	</select>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="button" id="parent-next1" name="next" class="mt-5 next action-button" value="Next"/>
                                </fieldset>
                                <fieldset>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12 text-left pb-3">
                                                <h3 class="m-0 mb-1">Job Description</h3>
                                                <textarea id="description" rows="7"></textarea>
                                            </div>
                                            <div class="col-md-12 text-left pb-3">
                                                <h3 class="m-0 mb-1">Job Requirements</h3>
                                                <textarea id="requirements" rows="7"></textarea>
                                            </div>
                                            <div class="col-md-12 text-left pb-3">
                                                <h3 class="m-0 mb-1">Benefits</h3>
                                                <textarea id="benefits" rows="7"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                                    <input type="button" id="parent-next2" name="next" class="next action-button" value="Next"/>
                                </fieldset>
                                <fieldset>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12 text-left">
                                                 <label><input type="radio" name="option" value="1" class="radio" /> PayPal</label>
                                            </div>
                                            <div class="col-md-11 mx-auto text-left pb-3">

                                                <h3 class="m-0 mb-1">PayPal Address</h3>
                                                <input type="text" id="paypal_address" name="fname"/>
                                            </div>                      
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-left">
                                                 <label><input type="radio" name="option" value="2" class="radio" /> Credit/Debit Card</label>
                                            </div>
                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-md-6 text-left pb-3">
                                                        <h3 class="m-0 mb-1">Name of Cardholder</h3>
                                                        <input type="text" id="card_name" name="fname"/>
                                                    </div>
                                                    <div class="col-md-6 text-left pb-3">
                                                        <h3 class="m-0 mb-1">Card Number</h3>
                                                        <input type="text" id="card_number" name="fname"/>
                                                    </div>
                                                    <div class="col-md-6 text-left pb-3">
                                                        <h3 class="m-0 mb-1">Expiry Date</h3>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="text" id="card_expiry_month" name="fname"/>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <input type="text" id="card_expiry_year" name="fname"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 text-left pb-3">
                                                        <h3 class="m-0 mb-1">CVV</h3>
                                                        <input type="text" id="card_cvv" name="fname"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                                    <input type="button" id="parent-next3" name="next" class="next action-button" value="Next"/>
                                </fieldset>
                                <fieldset>
                                    <div class="container">
                                        <div class="row mt-5 mb-5">
                                            <div class="col-md-12 pt-5 pb-5">
                                                <img src="<?= base_url() ?>assets/images/icons/icons8-good-quality-64.png"><br>
                                                 <label class="pt-2">You Job form has been submitted successfully</label>
                                                 <p>Once your payment has been verified, a confirmation email will be sent you.</p>
                                            </div>                    
                                        </div>
                                    </div>
                                    <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                                    <a href="<?= base_url("payment/".$secret) ?>" class="site-btn action-button">Submit</a>
                                </fieldset>
                            </form>
                            <!-- link to designify.me code snippets -->
                            <!-- /.link to designify.me code snippets -->
                        </div>
                    </div>
                    <!-- /.MultiStep Form -->
                </div>