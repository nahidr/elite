<style>
    .modal-body {
      position:relative;
      padding:0px;
  }
  .close {
      position:absolute;
      right:-30px;
      top:0;
      z-index:999;
      font-size:2rem;
      font-weight: normal;
      color:#fff;
      opacity:1;
  }
  .modal-dialog {
      max-width: 800px;
      margin: 50px auto;
  }
  .job-specs h2, .job-specs h3{
    color: #5c0100;
    font-weight: 700;
}
.jobs-row .fa{
    font-size: 20px;
}
.job-specs ul li{
    list-style: none;
}
.jobs-row:hover{
    background-color: transparent;
}
.jobs-row-single{
    padding: 15px;
    border-bottom: 1px solid #ddd;
}
.jobs-row-single:hover{
    background-color: transparent;
    /*border: 1px solid #ddd;*/
}
</style> 
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content"> 
                    <div class="container">
                        <div class="row pb-2">
                            <div class="col-md-12">
                                <div class="page-header pl-3 mt-4">
                                    <h2 class="pageheader-title">My Jobs</h2>
                                </div>
                            </div>
                        </div>
                        <?php if(!empty($jobs)){?>
                        <div class="row jobs-row">
                            <div class="col-md-12">
                                    <div class="container">
                                        <?php if(is_array($jobs)){
                                        foreach ($jobs as $job){
                                            $type = $job['parent_job_type'];
                                            $job_type = "";
                                            if($type == 1){
                                                $job_type = "Day Care";
                                            }else if($type == 2){
                                                $job_type = "Night Care";
                                            }else if($type == 3){
                                                $job_type = "Full-Time";
                                            }else if($type == 4){
                                                $job_type = "Stay-In (Weekend Only)";
                                            }else{
                                                $job_type = $type;
                                            }
                                            $avail = $job['parent_availability'];
                                            $availability = "";
                                            if($avail == 1){
                                                $availability = "As Soon As Possible";
                                            }else if($avail == 2){
                                                $availability = "In Two Weeks";
                                            }else if($avail == 3){
                                                $availability = "From The Start Of Coming Month";
                                            }else{
                                                $availability = $avail;
                                            }
                                        ?>
                                        <div class="row mb-4 jobs-row-single">
                                            <div class="col-md-12 p-0">
                                                <h3>
                                                    <a href=""><?= $job['parent_job_title'] ?></a>
                                                    <a href=""><span class="fa fa-trash-o active pull-right"></span></a>
                                                    <a href=""><span class="fa fa-pencil active  pull-right"></span></a>
                                                </h3>
                                            </div>
                                            <div class="col-md-10 p-0">
                                                <div class="jobs">
                                                    <div class="title">
                                                        <div class="container">
                                                            <div class="row">
        
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="type">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <label class="type-1">Job Type</label>
                                                                    <h5 class="type-2"><?= $job_type ?></h5>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label class="type-1">Availability</label>
                                                                    <h5 class="type-2"><?= $availability ?></h5>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label class="type-1">Experience</label>
                                                                    <h5 class="type-2"><?= $job['parent_experience'] ?> Years</h5>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label class="type-1">Salary</label>
                                                                    <h5 class="type-2">&pound;<?= $job['parent_min_salary'] ?> - &pound;<?= $job['parent_max_salary'] ?></h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="job-detail pb-4">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <p><?= $job['parent_job_description'] ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php   }  }  ?>
                                    </div>
                            </div>
                        </div>
                        <div class="row mt-5 mb-5">
                            <div class="col-md-12 pt-5 pb-5 text-center">
                                <a href="#" class="site-btn">Load More</a>
                            </div>
                        </div>
                        <?php }else{?>
                        <div class="row jobs-row">
                            <div class="col-md-12"> 
                                <div class='container'>
                                You haven't posted any jobs yet. 
                                <a href="<?= base_url("advertise-vacancy") ?>">Advertise your vacancy</a> now.
                                </div> 
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>