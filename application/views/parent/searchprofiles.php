<style>
    form{
      display:none;
  }

  .btns-as .site-btn,.btns-as .site-btn-1 {
    padding: 15px 55px;
}

#formButton{
  border-radius: 28px;
  font-family: Arial;
  color: #ffffff;
  font-size: 20px;
  background: #3498db;
  padding: 10px 20px 10px 20px;
  text-decoration: none;
  margin:20px;

}

#formButton:hover{
  background: #3cb0fd;
  text-decoration: none;
}

#submit{
  border-radius: 28px;
  font-family: Arial;
  color: #ffffff;
  font-size: 15px;
  background: #3498db;
  padding: 10px 20px 10px 20px;
  text-decoration: none;
}

#submit:hover{
  background: #3cb0fd;
  text-decoration: none;
}

.border-check {
  display: inline-block;
  position: relative;
}

.border-check:after {
  position: absolute;
  content: '';
  border-top: 1px dashed #5c0100;
  width: 80%;
  transform: translateX(-50%);
  top: -25px;
  left: 50%;
}
.lookingfor select{
    width: 50%;
    border: 2px solid #5c0100;
    border-radius: 20px;
    box-sizing: border-box;
    margin-bottom: 16px;
    resize: vertical;
}
.lookingfor select {
  /* for Firefox */
  -moz-appearance: none;
  /* for Chrome */
  -webkit-appearance: none;
}

</style>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content">
                    <div class="row mt-5">
                        <div class="col-md-8 mx-auto text-center">
                            <div class="row">
                                <div class="col-md-5 p-0 mt-3">
                                    <h2 class="pageheader-title">I m looking for</h2>
                                </div>
                                <div class="col-md-7 p-0 mt-3">
                                    <div class="search-header lookingfor">
                                        <select style="text-indent: 10px;position:relative;background-color: transparent;color:black;width: 100%;background-image: none;" class=" form-control">
                                            
                                            <option>Nanny</option>
                                            <option>Nanny/PA</option>
                                            <option>Governess/Tutor/Teacher</option>
                                            <option>Maternity Nurse</option>
                                            <option>Nanny/Housekeeper</option>
                                            <option>Housekeeper</option>
                                            <option>Chauffer</option>
                                            <option>Butle</option>
                                        </select>
                                        <span style="position:relative; right: 30px;top:10px;color: #5c0100" class="fa fa-angle-down fa-2x"></span>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="container mb-5" style="max-width: 1050px;background-color: #fff;box-shadow: 0px 1px 5px 2px rgba(92,1,0,0.6);">
                    <div class="row pt-5 pb-5 text-center">
                        <div class="col-md-6 search-container">
                            <div class="form-group">
                                <select name="<portlet:namespace/>Country" id="countryId" onchange="javascript:countryChange()">
                                    <option value="0">Select Country</option>
                                    <option value='US'>United States</option>
                                    <option value='CA'>Canada</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 search-container">
                            <div class="form-group"> 
                                <select placeholder="State" name="<portlet:namespace/>State" id="stateId">
                                    <option>Select State (optional)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 mx-auto search-container">
                            <div class="form-group">
                                <select id="inputmiles"  >
                                    <option>Select Miles (optional)</option>
                                    <option>15 miles</option>
                                    <option>30 miles</option>
                                    <option>45 miles</option>
                                    <option>60 miles</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" id="inputpass" placeholder="Enter Passcode (optional)">
                            </div>
                        </div>
                        <div class="col-md-10 mx-auto">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6 adv-srch text-left">
                                        <a onclick="showForm()" style="cursor: pointer">Advanced Search</a>
                                    </div>
                                    <div class="col-md-6 text-right btns-as">
                                        <a href="#" class="site-btn">Search</a>
                                    </div>
                                </div>
                            </div>
                        </div>
<div class="col-md-12 mx-auto adv-search-container">
    <form id="formElement" style="display: none;">

        <div class="container pb-3 pt-5">
            <div class="row border-check">
                <div class="col-md-8 mx-auto pt-3">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Contract type</label>
                        </div>
                        <div class="col-md-8">
                            <select id="inputState" class="form-control">
                                <option selected>Permanent</option>
                                <option>Temporary</option>
                                <option>Either</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Type of role</label>
                        </div>
                        <div class="col-md-8">
                            <select id="inputState" class="form-control">
                                <option selected>Live-in</option>
                                <option>Live-out</option>
                                <option>Either</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Preffered language</label>
                        </div>
                        <div class="col-md-8">
                            <select id="inputState" class="form-control">
                                <option selected>English</option>
                                <option>Russian</option>
                                <option>Spanish</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Driver</label>
                        </div>
                        <div class="col-md-8">
                            <select id="inputState" class="form-control">
                                <option selected>Yes</option>
                                <option>No</option>
                                <option>Flexible</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Swimmer</label>
                        </div>
                        <div class="col-md-8">
                            <select id="inputState" class="form-control">
                                <option selected>Yes</option>
                                <option>No</option>
                                <option>Flexible</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Travel with family</label>
                        </div>
                        <div class="col-md-8">
                            <select id="inputState" class="form-control">
                                <option selected>Yes</option>
                                <option>No</option>
                                <option>Flexible</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Available from</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                               <input type="date" name="bday" max="3000-12-31" 
                               min="1000-01-01" class="form-control">
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="btns-as pt-4 pb-2">
            <a href="#" class="site-btn m-4">Filter</a>
            <a href="#" onclick="closeForm()" class="site-btn-1 m-4">Cancel</a>
        </div>
    </form>
</div>
</div>
</div>
<?php
    $this->load->view('parent/profiles');
?>
