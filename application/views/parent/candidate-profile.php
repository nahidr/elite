<style>
    .Settings h3 {
        text-transform: uppercase;
        letter-spacing: 1.2px;
        color: #3c3c3c;
    }
    .Settings{
        background-color: #fff;
    }
    .quality-list li{
        padding: 5px;
        list-style: none;
        font-size: 15px;
    }
    .condition-list li{
        list-style: none;
        padding: 5px;
        font-size: 15px;
    }
    .condition-list p{
        font-size: 15px;
    }
    .bg-pink{
        background: #f3e2dd;
    }
    .modal-body {
      position:relative;
      padding:0px;
  }
  .close {
      position:absolute;
      right:-30px;
      top:0;
      z-index:999;
      font-size:2rem;
      font-weight: normal;
      color:#fff;
      opacity:1;
  }
  .modal-dialog {
      max-width: 800px;
      margin: 50px auto;
  }
  .condition ul {
      list-style-type: none; /* no default bullets */
  }

  .condition li { 
      font-family: Arial;
      font-size: 18px;
  }

  .condition li:before { /* the custom styled bullets */
      background-color: #5c0100;
      border-radius: 50%;
      content: "";
      display: inline-block;
      margin-right: 10px;
      margin-bottom: 2px;
      height: 7px;
      width: 7px;
  } 
  .backtoprofiles a{
    color: #5c0100;
    font-weight: 700;
    }
    h2{
        color: #5c0100;
    }
    .pull-right{
        text-align: right;
    }
</style>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content">
                    <div class="row">
                        <div class="col-md-12 backtoprofiles">
                            <a href="<?= base_url("view-profiles") ?>" class="pull-right"><h4><b>Back to profiles</b></h4></a>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row Settings border">
                        <div class="col-md-12 pl-0">
                            <div class="container p-0">
                                <div class="row border-bottom">
                                    <div class="col-md-3 text-center border-right">
                                        <div class="settings-page-header">
                                            <div class="col-md-12 pt-5 sidebar-profile">
                                                <img src="<?= base_url() ?>assets/assets/images/profile.png">
                                                <h2 class="pt-2"><b><?= $can_full_name ?></b></h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 p-0">
                                        <div class="qualities p-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5 class="pull-right"><a href="<?= base_url("favourite/".$user_id_fk) ?>"><img src="<?= base_url() ?>assets/assets/images/heart.png" style="<?= ($favourite == 0)? "filter: brightness(50%);" : "" ?>"></a></h5>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: -40px">
                                                <div class="col-md-6 quality-list">
                                                    <!-- <li><span><img src="<?= base_url() ?>assets/assets/images/check.png">&emsp;NVQ Level 2 in Health & Social Care</span></li> -->
                                                    <li><span><img src="<?= base_url() ?>assets/assets/images/check.png">&emsp;DBS checked</span></li>
                                                    <li><span><img src="<?= base_url() ?>assets/assets/images/check.png">&emsp;Professional reference checked</span></li>
                                                    <li><span><img src="<?= base_url() ?>assets/assets/images/check.png">&emsp;Professional indemnity insurance verified</span></li>
                                                    
                                                </div>
                                                <div class="col-md-6 quality-list">
                                                	<?php
                                                	   $car = "";
                                                	   $options = json_decode($can_options, true);
                                                	   if($options[0] == 1){
                                                	       if($options[1] == 1){
                                                	           $car = "Has Drivers Licence & Car";
                                                	       }else{
                                                	           $car = "Has Drivers Licence but no Car";
                                                	       }
                                                	   }else{
                                                	       if($options[1] == 1){
                                                	           $car = "No Drivers Licence but has Car";
                                                	       }else{
                                                	           $car = "No Drivers Licence & no Car";
                                                	       }
                                                	   }
                                                	   $pet = ($options[2] == 1)? "Love Pets!" : "No Pets Please!";
                                                	   $smoker = ($options[3] == 1)? "Smoker" : "Non Smoker";
                                                	   $english = ($options[4] == 1)? "Speaks English (".$options[5].")" : "No English";
                                                	?>
                                                    <li><span><img src="<?= base_url() ?>assets/assets/images/speak.png">&emsp;<?= $english ?></span></li>
                                                    <li><span><img src="<?= base_url() ?>assets/assets/images/pets.png">&emsp;<?= $pet ?></span></li>
                                                    <li><span><img src="<?= base_url() ?>assets/assets/images/smoke.png">&emsp;<?= $smoker ?></span></li>
                                                    <li><span><img src="<?= base_url() ?>assets/assets/images/car.png">&emsp;<?= $car ?></span></li>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 pr-0 pt-0 border-right">
                                        <div class="settings-page-header">
                                            <div class="col-md-12 pt-5 pb-5 quality-list">
                                                <li><span><img src="<?= base_url() ?>assets/assets/images/map.png">&emsp;<?= $can_state.", ".$can_country ?></span></li>
                                                <li><span><img src="<?= base_url() ?>assets/assets/images/star.png">&emsp;<?= $can_experience ?> Year(s) of Experience</span></li>
                                            </div>
                                            <div class="col-md-12 pt-3 pb-3 quality-list">
                                            	<?php
                                            	   $type = json_decode($can_availability, true);
                                            	   foreach ($type as $avail){
                                            	       if($avail == 1){
                                            	           echo '<li><span><img src="'.base_url().'assets/assets/images/day.png">&emsp;Day Care</span></li>';
                                            	       }else if($avail == 2){
                                            	           echo '<li><span><img src="'.base_url().'assets/assets/images/night.png">&emsp;Night Care</span></li>';
                                            	       }else if($avail == 3){
                                            	           echo '<li><span><img src="'.base_url().'assets/assets/images/home.png">&emsp;Live-in-care</span></li>';
                                            	       }
                                            	   }
                                            	?>
                                                
                                                
                                                
                                            </div>
                                            <div class="col-md-9 pt-5 text-center pb-5 mx-auto">
                                                <a href="<?= base_url("parent-inbox") ?>" class="site-btn mt-2 mb-2">Contact Me</a>
                                                <?php
                                                $url = base_url()."uploads/candidate/".$can_video;
                                                ?>
                                                <a href="#" class="site-btn-1 mt-2 mb-2" class="btn btn-primary video-btn" data-toggle="modal" data-src="<?= $url ?>" data-target="#myModal">Watch Intro</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 p-0">
                                        <div class="border-bottom">
                                            <div class="condition pt-4 pl-4 pr-4">
                                                <h3 class="m-0"><b>Condition Experience</b></h3>
                                                <div class="row pt-2 condition-list">
                                                	<?php 
                                                	   $all = [1,2,3,4,5,6,7,8,9];
                                                	   $experience = json_decode($can_conditions,true);
                                                	   for($i = 0; $i < 9; $i++){
                                                	       if(in_array($all[$i], $experience)){
                                                	           if($all[$i] == 1){
                                                	               $show = "Alzheimer's disease";
                                                	           }else if($all[$i] == 2){
                                                	               $show = "Anxiety";
                                                	           }else if($all[$i] == 3){
                                                	               $show = "Athirist";
                                                	           }else if($all[$i] == 4){
                                                	               $show = "Confusion";
                                                	           }else if($all[$i] == 5){
                                                	               $show = "Stroke";
                                                	           }else if($all[$i] == 6){
                                                	               $show = "Dementia";
                                                	           }else if($all[$i] == 7){
                                                	               $show = "Depression";
                                                	           }else if($all[$i] == 8){
                                                	               $show = "Diabetes";
                                                	           }else if($all[$i] == 9){
                                                	               $show = "Incontinence";
                                                	           }
                                                	   ?>
                                                	   <div class="col-md-4">
                                                            <li><?= $show ?></li>
                                                        </div>
                                                	   <?php
                                                	       }
                                                	   }
                                                	?>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="condition pt-4 pl-4 pr-4">
                                            <h3 class="m-0"><b>About</b></h3>
                                            <div class="row condition-list">
                                                <div class="col-md-12">
                                                    <p><?= $can_about ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="condition pt-4 pl-4 pr-4 pb-4">
                                            <h3 class="m-0"><b>Interests</b></h3>
                                            <div class="row condition-list">
                                                <div class="col-md-12">
                                                	<?php
                                                	$interests = json_decode($can_interests);
                                                	$text = "";
                                                	for($i = 1; $i < 4; $i++){
                                                	    if(in_array($i, $interests)){
                                                	        if($i == 1){
                                                	            if($text == ""){
                                                	                $text = "Painting";
                                                	            }else{
                                                	                $text = $text.", Painting";
                                                	            }
                                                	        }else if($i == 2){
                                                	            if($text == ""){
                                                	                $text = "Singing";
                                                	            }else{
                                                	                $text = $text.", Singing";
                                                	            }
                                                	        }else if($i == 3){
                                                	            if($text == ""){
                                                	                $text = "Reading";
                                                	            }else{
                                                	                $text = $text.", Reading";
                                                	            }
                                                	        }
                                                	    }
                                                	}
                                                	?>
                                                    <p><?= $text ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">


                      <div class="modal-body">

                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>        
                      <!-- 16:9 aspect ratio -->
                      <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item" src="<?= $url ?>" frameborder="0" allowfullscreen></iframe>
                      </div>


                  </div>

              </div>
          </div>
      </div>