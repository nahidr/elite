<style>
    @font-face { font-family: Sweet Easy; src: url('<?= base_url() ?>assets/assets/fonts/Sweet Easy personal Use.ttf');}
    @font-face { font-family: Segoe UI;  src: url('<?= base_url() ?>assets/assets/fonts/segoeui.ttf');}
    form{
      display:none;
  }

  .btns-as .site-btn,.btns-as .site-btn-1 {
    padding: 15px 55px;
}

#formButton{
  border-radius: 28px;
  font-family: Arial;
  color: #ffffff;
  font-size: 20px;
  background: #3498db;
  padding: 10px 20px 10px 20px;
  text-decoration: none;
  margin:20px;

}
.profile-name h3{
    margin: 0px;
}
#formButton:hover{
  background: #3cb0fd;
  text-decoration: none;
}

#submit{
  border-radius: 28px;
  font-family: Arial;
  color: #ffffff;
  font-size: 15px;
  background: #3498db;
  padding: 10px 20px 10px 20px;
  text-decoration: none;
}

#submit:hover{
  background: #3cb0fd;
  text-decoration: none;
}

.border-check {
  display: inline-block;
  position: relative;
}

.border-check:after {
  position: absolute;
  content: '';
  border-top: 1px dashed #5c0100;
  width: 80%;
  transform: translateX(-50%);
  top: -25px;
  left: 50%;
}

</style>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container dashboard-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-header pl-3 mt-4">
                                <h2 class="pageheader-title">Favourited Profiles</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <?php
                            $this->load->view('parent/profiles');
                        ?>
                    </div>
                </div>
            </div>