<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <title>Elite Private Staff</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="assets/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/libs/css/style.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/vendor/charts/c3charts/c3.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
    <style>
    @font-face { font-family: Sweet Easy; src: url('<?= base_url() ?>assets/assets/fonts/Sweet Easy personal Use.ttf');}
    @font-face { font-family: Segoe UI;  src: url('<?= base_url() ?>assets/assets/fonts/segoeui.ttf');}
    </style>
    <script type="text/javascript">
    var BASE_URL = "<?php echo base_url(); ?>";
    var registration_step = "<?= isset($status)? $status : 1 ?>";
    //     console.log(registration_step);
    </script>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg fixed-top">
                <a class="navbar-brand" id="marg-0-auto" href="<?= base_url("parent-dash") ?>"><img src="<?= base_url() ?>assets/images/logo.png" width="250px"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav nav-menu">
                        <li class="nav-link">
                            <a href="<?= base_url() ?>">Home</a>
                        </li>
                        <li class="nav-link">
                            <a href="<?= base_url("contact") ?>">Contact us</a>
                        </li>
                        <li class="nav-link">
                            <a href="<?= base_url("about") ?>">About us</a> 
                        </li>
                        <li class="nav-link">
                            <a href="<?= base_url("gdpr") ?>">GDPR</a> 
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php $pic = $this->session->userdata['user']['user_picture']; ?>
                            <img src="<?= base_url() ?>uploads/parent/<?= $pic ?>" alt="" class="user-avatar-md rounded-circle">
                            <span class="fa fa-caret-down pl-1 fa-2x"></span></a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2" style="top: 90%; right: 10%; background: #f3e2dd; border-top-right-radius: 13px; border-bottom-left-radius: 13px;">
                                <a class="dropdown-item" href="<?= base_url("parent-settings") ?>">Edit Profile</a>
                                <hr class="m-0">
                                <a class="dropdown-item" href="<?= base_url("logout") ?>">Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div> 
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                <div class="container-fluid">
                                    <div class="row text-center pt-2 pb-2">
                                        <div class="col-md-12 sidebar-profile">
                                            <span>Welcome to your account</span>
                                            <h4><?= $this->session->userdata['user']['user_first_name']." ".$this->session->userdata['user']['user_last_name'] ?></h4>
                                            <?php $pic = $this->session->userdata['user']['user_picture']; ?>
                                            <img class="rounded-circle" src="<?= base_url() ?>uploads/parent/<?= $pic ?>">
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                            	<div style="text-align: center; margin-bottom:50px">
                            		<button class="site-btn" id="trigger_file_selector" style="font-weight:normal"><u>Upload Picture</u></button>
                            	</div>
                            	<input style="display: none;" type="file" class="custom-file-input" id="inputGroupFile01" name="userfile" aria-describedby="inputGroupFileAddon01">
                                <input type="hidden" name="user_type_global" id="user_type_global" value="2">
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link <?= ($this->session->userdata['page_name'] == "Parent Dashboard")? "active" : "" ?>" href="<?= base_url("parent-dashboard") ?>" aria-expanded="false"><i class="fa fa-fw fa-home"></i>Dashboard</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link <?= ($this->session->userdata['page_name'] == "View Profiles")? "active" : "" ?>" href="<?= base_url("view-profiles") ?>" aria-expanded="false"><i class="fa fa-fw fa-search"></i>Search Profiles</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link <?= ($this->session->userdata['page_name'] == "Favourited")? "active" : "" ?>" href="<?= base_url("parent-favourited") ?>" aria-expanded="false"><i class="fa fa-fw fa-heart"></i>Favourited</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link <?= ($this->session->userdata['page_name'] == "My Jobs")? "active" : "" ?>" href="<?= base_url("my-jobs") ?>" aria-expanded="false"><i class="fa fa-fw fa-briefcase"></i>My Jobs</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link <?= ($this->session->userdata['page_name'] == "Parent Inbox")? "active" : "" ?>" href="<?= base_url("parent-inbox") ?>" aria-expanded="false"><i class="fa fa-fw fa-envelope"></i>Inbox</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link <?= ($this->session->userdata['page_name'] == "Parent Settings")? "active" : "" ?>" href="<?= base_url("parent-settings") ?>" aria-expanded="false"><i class="fa fa-fw fa-cog"></i>Settings</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->