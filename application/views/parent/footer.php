            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="mx-auto col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            © Elite Private Staff 2019. All rights reserved.
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <script type="text/javascript">
    function showForm() {
        document.getElementById('formElement').style.display = 'block';
    }
    function closeForm() {
        document.getElementById('formElement').style.display = 'none';
    }
</script>
<script>
    function countryChange() {
        var countryState = [
        [
        'US', [
        ['', 'State/Province'],
        ['AL', 'Alabama'],
        ['AK', 'Alaska'],
        ['AZ', 'Arizona'],
        ['AR', 'Arkansas'],
        ], ],
        [
        'CA', [
        ['', 'State/Province'],
        ['AB', 'Alberta'],
        ['BC', 'British Columbia'],
        ['MB', 'Manitoba'],
        ['NB', 'New Brunswick'],
        ]]
        ];

        var countryElement = document.getElementById('countryId');
        var stateElement = document.getElementById('stateId');
        var stateLabelElement = document.getElementById('stateLabel');
        var miles = document.getElementById('inputmiles');
        var pass = document.getElementById('inputpass');

        if (countryElement && stateElement) {
            var listOfState = [
            ['XX', 'None']
            ];
            var currentCountry = countryElement.options[countryElement.selectedIndex].value;
            for (var i = 0; i < countryState.length; i++) {
                if (currentCountry == countryState[i][0]) {
                    listOfState = countryState[i][1];
                }
            }
            if (listOfState.length < 2) 
            {
                stateElement.style.display = 'none';
                stateLabelElement.style.display = 'none';
            }
            else 
            {
                stateElement.style.display = 'inline';
                stateLabelElement.style.display = 'inline';
            }
            var selectedState;
            for (var i = 0; i < stateElement.length; i++) {
                if (stateElement.options[i].selected === true) {
                    selectedState = stateElement.options[i].value;
                }     
            }
            stateElement.options.length = 0;
            for (var i = 0; i < listOfState.length; i++) {
                stateElement.options[i] = new Option(listOfState[i][1], listOfState[i][0]);
                if (listOfState[i][0] == selectedState) {
                    stateElement.options[i].selected = true;
                }    
            }
            if (stateElement.length > 0) 
            {
                miles.style.display = 'inline';
                pass.style.display = 'inline';
            }

        }
    }
</script>
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="<?= base_url() ?>assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="<?= base_url() ?>assets/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="<?= base_url() ?>assets/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="<?= base_url() ?>assets/assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="<?= base_url() ?>assets/assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="<?= base_url() ?>assets/assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="<?= base_url() ?>assets/assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="<?= base_url() ?>assets/assets/vendor/charts/morris-bundle/morris.js"></script>
    <?php if($this->session->userdata['page_name'] != "Advertize Vacancy"){ ?>
    <!-- chart c3 js -->
    <script src="<?= base_url() ?>assets/assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="<?= base_url() ?>assets/assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="<?= base_url() ?>assets/assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="<?= base_url() ?>assets/assets/libs/js/dashboard-ecommerce.js"></script>
    <?php } else { ?>
    <!-- chart c3 js -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
    <script>window.jQuery || document.write('<script src="<?= base_url() ?>assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/js/msform.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?= base_url() ?>assets/js/ie10-viewport-bug-workaround.js"></script>
    <script src="<?= base_url() ?>assets/assets/libs/js/dashboard-ecommerce.js"></script>
    <?php } ?>
    <script>
$(document).ready(function() {
    var $videoSrc;  
    $('.video-btn').click(function() {
        $videoSrc = $(this).data( "src" );
    }); 
    $('#myModal').on('shown.bs.modal', function (e) {
        $("#video").attr('src',$videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" ); })
    $('#myModal').on('hide.bs.modal', function (e) {
        $("#video").attr('src',$videoSrc); }) });
</script>
<script>
    $(function() {
        $('input[name="type"]').on('click', function() {
            if ($(this).val() == 'Experienced') {
                $('#textboxes').show();
                $('#textboxes1').hide();
            }
            else if($(this).val() == 'Fresher'){
                $('#textboxes1').show();
                $('#textboxes').hide();
            }
            else {
                $('#textboxes').hide();
                $('#textboxes1').hide();
            }
        });
    });
</script>
	<script type="text/javascript">
    	$("#trigger_file_selector").on('click',function(){
        	$("#inputGroupFile01").click();
        });
        $("input[type=file]").on('change',function(){
//             alert(this.files[0].name);
			var name = this.files[0].name;
            var form_data = new FormData();
        	var ext = name.split('.').pop().toLowerCase();
        	if(jQuery.inArray(ext, ['png','jpg','jpeg']) == -1) 
        	{
        		alert("Invalid Image File");
        	}else{
        		var oFReader = new FileReader();
            	oFReader.readAsDataURL(this.files[0]);
            	var f = this.files[0];
            	var fsize = f.size||f.fileSize;
            	var type = $("#user_type_global").val();
            	if(fsize > 2000000)
            	{
            		alert("Image File Size is very big");
            	}
            	else
            	{
            		form_data.append("userfile", this.files[0]);
            		console.log(form_data);
            		$.ajax({
            			url:BASE_URL+"update-profile-picture/"+type,
            			method:"POST",
            			data: form_data,
            			contentType: false,
            			cache: false,
            			processData: false,
            			beforeSend:function(){
//            				$('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
            			},   
            			success:function(data)
            			{
//            				$('#uploaded_image').html(data);
            				console.log(data);
            				if(data!=0){
            					window.location.reload(true);
            				}
            			}
            		});
            	}
        	}
        });
	</script>
</body>
 
</html>