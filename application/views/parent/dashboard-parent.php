<div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content">
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="page-header pl-3 pt-3 pb-3">
                                <h2 class="pageheader-title">Dashboard</h2>
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <div class="page-header pl-3 pt-3 pb-3" style="text-align:right">
                                <a href="<?= base_url("advertise-vacancy") ?>" class="site-btn">Advertise your vacancy</a>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-5">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="hiring pl-3 pt-3">
                                        <h3>The hiring process</h3>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="hiring-box pt-3 pb-3">
                                       <div class="p-3 text-center">
                                        <ul>
                                            <li>Search & Shortlist profiles of interest</li>
                                            <li>To contact potential candidates advertise your vacancy using our quick <span style="color: #ffcec2">template</span>.</li>
                                            <li>$79.99 Payment Required.</li>
                                            <li>Your vacancy will be live for 6 weeks.</li>
                                            <li>Start contacting candidates using our message board or direct mail.</li>
                                        </ul>
                                           
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="hiring pl-3 pt-3">
                                        <h3>Activity feed</h3>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="hiring-box pt-3 pb-2">
                                    <?php if(!empty($activities)){?>
                                       <div class="pt-3 pl-5 pr-5 pb-3">
                                        <div class="text-right pb-2">
                                                <a href="<?= base_url("mark-read") ?>">Mark all as read</a>
                                            </div>
                                            <?php
                                            if($interested){
                                                foreach ($interested as $int){
                                                    echo "<p><a style='padding: 0px;' href='".base_url("view-candidate/".$int['candidate_user_id_fk'])."'>Someone</a> just showed interest in your <a style='padding: 0px;' href='".base_url("my-jobs")."'>job post</a>.</p>";
                                                }
                                            }
                                            ?>
                                            <!-- <p>Someone just showed interest in your job post.</p> --> 
                                       </div>
                                    <?php }else{?>
                                        <div class="pt-3 pl-5 pr-5 pb-3">
                                        <div class="text-right pb-2"> 
                                            </div>
                                             No activities at the moment.
                                       </div>
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 pb-5 pt-3">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="hiring pl-3 pt-3">
                                        <h3>Inbox</h3>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <?php if(!empty($messages)){?>
                                    <div class="inbox-box ">
                                        <div class="text-right pt-3">
                                            <a href="<?= base_url("parent-inbox") ?>">View Inbox</a>
                                        </div>
                                        <?php foreach ($messages as $msg):?>
                                            <div class="inbox-sec active viewconvo" id='<?=$msg['message_id']?>'> 
                                                    <h3><?=$msg['user_first_name']?>  <?=$msg['user_last_name']?></h3>
                                                    <p><?=$msg['message_text']?></p> 
                                            </div>
                                        <?php endforeach;?> 
                                    </div>
                                    <?php }else{?>
                                        <div class="inbox-box ">
                                            <div class="text-right pt-3"> 
                                            </div> 
                                            <div class="inbox-sec"> 
                                                <p>No messages.</p> 
                                        </div>
                                    <?php } ?> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
