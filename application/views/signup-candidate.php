

<div class="container">
	<div class="row">
		<div class="col-12 col-md-9 mx-auto mt-5 mb-5">
			<div class="login text-center pt-5 pb-5">
				<div class="radio-toolbar pt-3 pb-3">
					<div class="row login-signup-btn">
						<div class="col-md-6 pb-3">
							<a href="<?= base_url('parent-signup') ?>">Parent</a>
						</div>
						<div class="col-md-6 pb-3">
							<a href="" class="active">Candidates</a>
						</div>
					</div>
				</div>
				<div class="pt-5 pb-5">
					<h2 class=""
						style="color: #5c0100; font-family: Sweet Easy; font-size: 24px; line-height: 45px;"
						data-aos="fade">Create a FREE account to browse jobs</h2>
				</div>

				<div class="">
					<div class="container">
						<div class="row">
							<div class="col-md-10 mx-auto">
								<?php echo form_open('register'); ?>
								<div class="row" id="placeholder-input">
									<?php $this->load->view('messages'); ?>
									<input type="hidden" name="user_type" value="1">
									<div class="col-lg-12 pt-2">
										<h6 class="text-left">Name</h6>
										<div class="row">
											<div class="col-lg-6">
												<input name="user_first_name" type="text"
													placeholder="First Name*">
											</div>
											<div class="col-lg-6">
												<input name="user_last_name" type="text"
													placeholder="Last Name*">
											</div>
										</div>
									</div>
									<div class="col-lg-12 pt-2">
										<h6 class="text-left">Email Address</h6>
										<input name="user_email" type="email" placeholder="Email*">
									</div>
									<div class="col-lg-12 pt-2">
										<h6 class="text-left">
											Password <i>(min 6 letters)</i>
										</h6>
										<input name="user_password" type="password"
											placeholder="Password*">
									</div>
									<div class="col-lg-12 pt-2">
										<h6 class="text-left">Confirm Password</h6>
										<input name="user_password_confirm" type="password"
											placeholder="Confirm Password*">
									</div>
									<div class="col-lg-12 signup-note text-left">
										<label>By creating an account you agree to the website <span>terms
												& conditions</span> and our <span>privacy notes</span>.
										</label>
									</div>
									<div class="col-lg-12 pt-2 pb-2">
										<div class="Search-btn text-center pt-5">
											<button class="site-btn" type="submit">Create an account</button>
										</div>
									</div>
									<div class="col-lg-12 pt-2 pb-2">
										<div class="have-account pt-2">
											<a href="<?= base_url('candidate-login') ?>"> Have an
												Account? Login</a>
										</div>
									</div>
								</div>
								<?php echo form_close(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

