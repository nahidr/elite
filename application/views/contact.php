    <div class="contact-section  pt-5 text-center">
      <div class="container">
        <div class="col-md-12">
          <h2 class="m-0 aos-init aos-animate" style="color: #5c0100; font-family: Sweet Easy;font-size: 24px;line-height: 55px;letter-spacing: 1.5px;" data-aos="fade"><span><b>Contact Us</b></span></h2>
          <h4 style="color: #e5b4a5;font-size: 24px;letter-spacing: 2px;line-height: 55px;" data-aos="fade"><span>Let us know we can help you</span></h4>
        </div>
        <div class="col-md-12 pt-2 pb-2">
          <div class="row pt-3">
            <div class="col-md-6">
              <form action="" method="">
              <div class="row" id="placeholder-input">
                <div class="col-md-12 pb-3 text-left">
                  <label>Full Name</label>
                  <input type="text" name="">
                </div>
                <div class="col-md-12 pb-3 text-left">
                  <label>Email Address</label>
                  <input type="text" name="">
                </div>
                <div class="col-md-12 pb-3 text-left">
                  <label>Subject</label>
                  <input type="text" name="">
                </div>
                <div class="col-md-12 pb-3 text-left">
                  <label>Message</label>
                  <textarea style="height: 160px"></textarea>
                </div>
                <div class="col-lg-12 pt-2 pb-2">
                  <div class="Search-btn text-center">
                    <a href="#" class="site-btn">Send</a>
                  </div>
                </div>
              </div>
              </form>
            </div>
            <div class="col-md-5 mx-auto text-left">
              <div class="pt-5">
                <div class="row">
                  <div class="col-md-9">
                    <ul>
                      <li style="list-style: none;">
                        <div class="row pt-3 pb-3">
                          <div class="col-md-3 pt-3">
                            <span><img src="assets/images/sale.png"></span>
                          </div>
                          <div class="col-md-9" >
                            <div class="row">
                              <div class="col-md-12">
                                <h5><b>Sales</b></h5>
                              </div>
                              <div class="col-md-12">
                                <p class="m-0">07787 343 003</p>
                                <p class="m-0">sles@elitestaff.com</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      
                        <div class="row pt-3 pb-3">
                          <div class="col-md-3 pt-3">
                            <span><img src="assets/images/help.png"></span>
                          </div>
                          <div class="col-md-9" >
                            <div class="row">
                              <div class="col-md-12">
                                <h5><b>Help & Support</b></h5>
                              </div>
                              <div class="col-md-12">
                                <p class="m-0">07787 343 003</p>
                                <p class="m-0">sles@elitestaff.com</p>
                              </div>
                            </div>
                          </div>
                        </div>
      
                        <div class="row pt-3 pb-3">
                          <div class="col-md-3 pt-3">
                            <span><img src="assets/images/media.png"></span>
                          </div>
                          <div class="col-md-9" >
                            <div class="row">
                              <div class="col-md-12">
                                <h5><b>Media & Press</b></h5>
                              </div>
                              <div class="col-md-12">
                                <p class="m-0">07787 343 003</p>
                                <p class="m-0">sales@elitestaff.com</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 p-0 mt-5 text-center">
          <h2 style="color: #5c0100; font-family: Sweet Easy;font-size: 24px;line-height: 55px;letter-spacing: 2px;font-weight: lighter;" data-aos="fade">
            <span>
              <b>Find Us</b>
            </span>
          </h2>
          <div id="googleMap" class="mt-3"></div>
          <script>
            function myMap() {
              var mapProp= {
                center:new google.maps.LatLng(51.508742,-0.120850),
                zoom:5,
              };
              var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
            }
          </script>
          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpeqUgLxQJqSSQKpZ4c84C1cYm3p58MSA&callback=myMap"></script>
        </div>
      </div>
    </div>
    