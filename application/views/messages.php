<?php if($this->session->flashdata('error_msg')){ ?>
	<div class="alert alert-danger" role=" alert"style="width: 100%;">
    	<?php echo $this->session->flashdata('error_msg'); ?>
	</div>
<?php }
if($this->session->flashdata('success_msg')){ ?>
    <div class="alert alert-success" role=" alert"style="width: 100%;">
    	<?php echo $this->session->flashdata('success_msg'); ?>
	</div>
<?php }
if(validation_errors()){?>
    <div class="alert alert-danger" role=" alert"style="width: 100%;">
    	<?php echo validation_errors(); ?>
	</div>
<?php } else if($this->session->flashdata('Email_issue')){ ?>
    <div class="alert alert-danger" role=" alert"style="width: 100%;">
    	<?php echo $this->session->flashdata('Email_issue'); ?>
	</div>
<?php } ?>