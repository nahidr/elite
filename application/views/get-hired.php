<div class="section pt-5 text-center">
      <div class="container">
        <div class="col-md-12 aboutus">
          <h2 class="m-0" style="color: #5c0100; font-family: Sweet Easy;font-size: 25px;line-height: 55px;" data-aos="fade"><span>Online Introduction Platform for<br> Employers and Private Household staff</span></h2>
          <p class="pt-3">Welcome Talent. We bring to you attractive employment opportunities direct from Employers in the UK & Europe.
            A platform for top talent to introduce themselves, show interest in jobs & wait for the Employer to apply to them.
          No agencies. Direct Hire.</p>
        </div>
        <div class="site-section pt-5 pb-2">
          <div class="container">
            <div class="row no-gutters align-items-stretch h-100">
              <div class="col-md-10 mx-auto min-h">
                <div class="row">
                  <div class="col-lg-6 ">
                    <div class="row no-gutters req-box align-items-stretch  half-sm">
                      <div class="col-md-12 text">
                        <div class="p-4 text-left">
                         <h2 class="text-center heading" style="color: #5c0100;font-size: 22px;line-height: 55px;text-transform: uppercase;" data-aos="fade"><span>Key requirements to register</span></h2>
                         <ul>
                          <li>
                            <span class="pl-2">Qualified and or trained (certificate required)</span>
                          </li>
                          <li>
                            <span class="pl-2">3 years minimum solid experience working in professional households.</span>
                          </li>
                          <li>
                            <span class="pl-2">Enhanced Police checked.</span>
                          </li>
                          <li>
                            <span class="pl-2">Paediatric First Aid trained if working with children (certificate required).</span>
                          </li>
                          <li>
                            <span class="pl-2">Checkable references.</span>
                          </li>
                          <li>
                            <span class="pl-2">Identification - travel document (copy required).</span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-lg-6 ">
                  <div class="row no-gutters req-box align-items-stretch half-sm">
                    <div class="col-md-12 min-h text order-md-1 order-lg-2">
                      <div class="p-4 text-left">
                        <h2 class="text-center heading" style="color: #5c0100;font-size: 22px;line-height: 55px;text-transform: uppercase;" data-aos="fade"><span>Security & Safety</span></h2>
                        <ul>
                          <li style="font-family: Segoe UI;">
                            <span class="pl-2">
                              Security and safety features
                            </span>
                          </li>
                          <li>
                            <span class="pl-2">SSL - Encryption</span>
                          </li>
                          <li>
                            <span class="pl-2">Upload documents are not shared.</span>
                          </li>
                          <li>
                            <span class="pl-2">Your profile includes icons to show
                            what documents have been vetted.</span>
                          </li>
                          <li>
                            <span class="pl-2">No personal details disclosed.</span>
                          </li>
                          <li>
                            <span class="pl-2">Successful profiles receive ID number.</span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="team-section pb-5">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h1 class="numer-leftside">1</h1>
          <h3 class="laptop-text-h pl-5 pt-5 aos-init aos-animate" style="color: #5c0100; font-family: Sweet Easy;font-size: 24px;line-height: 55px;" data-aos="fade">Create a profile</h3>
          <ul class="laptop-text-ul pt-3">
            <li>
              Complete the CV template
            </li>
            <li>
              Quick & easy
            </li>
            <li>
              Provide an insight into your skills & experience. 
            </li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
          <h1 class="numer-rightside">2</h1>
          <h3 class="laptop-text-h pl-5 pt-5 aos-init aos-animate" style="color: #5c0100; font-family: Sweet Easy;font-size: 24px;line-height: 55px;" data-aos="fade">Upload documents</h3>
          <ul class="laptop-text-ul pt-3">
            <li>
              qualification/training certificates including Paediatric first aid if child carer. 
            </li>
            <li>
              Enhanced Police check.
            </li>
            <li>
              Travel document (ID).
            </li>
            <li>
              Give the employer reassurance
            </li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <h1 class="numer-leftside">3</h1>
          <h3 class="laptop-text-h pl-5 pt-5 aos-init aos-animate" style="color: #5c0100; font-family: Sweet Easy;font-size: 24px;line-height: 55px;" data-aos="fade">Sixty seconds video-recording</h3>
          <ul class="laptop-text-ul pt-3">
            <li>
              Easy to follow instructions.
            </li>
            <li>
              Speed-up the hiring process. 
            </li>
            <li>
              Formally introduce yourself. 
            </li>
            <li>
              Create engagement. 
            </li>
          </ul>
        </div>
        <div class="col-md-6"></div>
      </div>
      <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
          <h1 class="numer-rightside">4</h1>
          <h3 class="laptop-text-h pl-5 pt-5 aos-init aos-animate" style="color: #5c0100; font-family: Sweet Easy;font-size: 24px;line-height: 55px;" data-aos="fade">Browse jobs & let the Employer know you are interested</h3>
          <ul class="laptop-text-ul pt-3">
            <li>
              Find your ideal job.
            </li>
            <li>
              Use platform private messaging to communicate.
            </li>
            <li>
              Get hired
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>