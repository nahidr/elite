
    
    <div class="site-block-half d-block d-lg-flex site-block-video mb-5" data-aos="fade">
      <div class="container-fluid">
        <div class="row text-center">
          <div class="col-md-12 p-0">
            <div class="image bg-image order-2" style="background-image: url(assets/images/video.png);height: 470px;width: 100%;">
              <a href="https://vimeo.com/channels/staffpicks/93951774"></a>
              <h2 class="pt-5" style="font-family: Sweet Easy;color: #fff;">The Hiring Process</h2>
              <div class="play-video pt-5">
                <a href="">
                  <span class="fa fa-play-circle fa-5x pt-5" style="color: #fff;"></span>
                  <h6 style="color: #fff;">Watch Video</h6>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="section aboutus text-center">
      <div class="container">
        <div class="col-md-12">
          <h2 class="m-0 aos-init aos-animate" style="color: #5c0100; font-family: Sweet Easy;font-size: 24px;line-height: 55px;" data-aos="fade"><span>We are a team of experienced recruitment consultants<br> working in the background uetting candidate.</span></h2>
        </div>
        <div class="col-md-12 mx-auto pt-5 pb-5">
          <div class="row pt-4">
            <hr> 
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="process">
                <h2><b>1</b></h2>
                <h4>Online</h4>
                <h4>Application</h4>
              </div>
            </div>
            
            <div class="col-md-4">
              <div class="process">
                <h2><b>2</b></h2>
                <h4>Background</h4>
                <h4>Checks</h4>
              </div>
            </div>
            
            <div class="col-md-4">
              <div class="process">
                <h2><b>3</b></h2>
                <h4>Candidate</h4>
                <h4>Approval</h4>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid banner">
      <div class="row text-center">
        <div class="col-md-6">
          <h4>Start looking for the perfect candidate now</h4>
        </div>
        <div class="col-md-6">
          <a href="#" class="banner-btn m-2">Contact me</a>
        </div>
      </div>
    </div>
    