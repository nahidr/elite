
    <div class="about-section">
      <div class="container pt-5 pb-5">
        <div class="row pb-5">
          <div class="col-md-10 mx-auto">
            <h2 class="text-center m-0 aos-init aos-animate pt-2 pb-5" style="color: #5c0100; font-family: Sweet Easy;font-size: 24px;line-height: 55px;letter-spacing: 2px;" data-aos="fade"><span><b>General Data Protection Regulation</b></span></h2>
            <p class="text-left">It is a long established fact that a reader will be distracted by the readable content of a page when 
              looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many 
            desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their inf</p>
          </div>
        </div>
        <div class="row pt-5">
          <div class="col-md-12 mx-auto">
            <div class="row mb-5">
              <div class="col-md-12">
                <div class="row text-center">
                  <div class="col-md-4 pt-5">
                    <img src="assets/images/icons/secure.png">
                    <h3 class="p-2"><b>Cookies</b></h3>
                    <p class="p-2">facilis est et expedita distinctio. Nam
                      libero tempore, cum soluta nobis est 
                      eligendi optio cumque nihil impedit 
                    quo minus idquod repellendus. </p>
                  </div>

                  <div class="col-md-4 pt-5">
                    <img src="assets/images/icons/secure.png">
                    <h3 class="p-2"><b>Data Protection</b></h3>
                    <p class="p-2">facilis est et expedita distinctio. Nam
                      libero tempore, cum soluta nobis est 
                      eligendi optio cumque nihil impedit 
                    quo minus idquod repellendus. </p>
                  </div>

                  <div class="col-md-4 pt-5">
                    <img src="assets/images/icons/secure.png">
                    <h3 class="p-2"><b>Unsubscribe</b></h3>
                    <p class="p-2">facilis est et expedita distinctio. Nam
                      libero tempore, cum soluta nobis est 
                      eligendi optio cumque nihil impedit 
                    quo minus idquod repellendus. </p>
                  </div>

                  <div class="col-md-4 pt-5">
                    <img src="assets/images/icons/secure.png">
                    <h3 class="p-2"><b>Breach Notification</b></h3>
                    <p class="p-2">facilis est et expedita distinctio. Nam
                      libero tempore, cum soluta nobis est 
                      eligendi optio cumque nihil impedit 
                    quo minus idquod repellendus. </p>
                  </div>

                  <div class="col-md-4 pt-5">
                    <img src="assets/images/icons/secure.png">
                    <h3 class="p-2"><b>Consent</b></h3>
                    <p class="p-2">facilis est et expedita distinctio. Nam
                      libero tempore, cum soluta nobis est 
                      eligendi optio cumque nihil impedit 
                    quo minus idquod repellendus. </p>
                  </div>

                  <div class="col-md-4 pt-5">
                    <img src="assets/images/icons/secure.png">
                    <h3 class="p-2"><b>Delete Data</b></h3>
                    <p class="p-2">facilis est et expedita distinctio. Nam
                      libero tempore, cum soluta nobis est 
                      eligendi optio cumque nihil impedit 
                    quo minus idquod repellendus. </p>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    