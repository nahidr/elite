<style>
    input[type=text], input[type=password], input[type=email], select{
        background-color: #f4f4f1;
        width: 100%;
        padding: 7px;
        border: 1px solid #ccc;
        border-radius: 0px;
        box-sizing: border-box;
        margin-bottom: 16px;
        resize: vertical;
    }
    textarea {
        outline: 0;
        width: 100%;
        padding: 7px;
        border: 1px solid #ccc;
        border-radius: 20px;
        box-sizing: border-box;
        margin-bottom: 16px;
        resize: none;
    }
    .site-btn {
        padding: 12px 20px !important;
        min-width: 100px;
        color: #fff !important;
    }
    .pull-right{
        float: right !important;
    }
    .deactivate-btn{
        font-family: Segoe UI;
        position: relative;
        display: inline-block;
        padding: 12px 0px;
        font-size: 14px;
        font-weight: 600;
        line-height: 16px;
        letter-spacing: 2px;
        border-radius: 20px;
        min-width: 200px;
        text-align: center;
        background: #5c0100;
        cursor: pointer;
        border: none;
        color: #fff;
    }
    .deactivate-modal-btn{
        font-weight: 700;
        cursor: pointer;
        border: none;
        background: transparent;
        color: #5c0100;
        font-size: large;
    }
    .deactivate-modal-btn:hover{
        color: #ffcec2;
    }
    .cancel-btn{
        font-family: Segoe UI;
        position: relative;
        display: inline-block;
        padding: 12px 0px;
        font-size: 14px;
        font-weight: 600;
        line-height: 16px;
        letter-spacing: 2px;
        border-radius: 20px;
        border: 2px solid #5c0100 !important;
        min-width: 200px;
        text-align: center;
        background: #fff;
        cursor: pointer;
        color: #5c0100;
    }
    .far{
        color: #5c0100;
    }
    @media (min-width: 576px){
        .modal-dialog {
            max-width: 600px;
            margin: 1.75rem auto;
    }}
</style>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-header pl-3 pt-4 pb-3">
                                <h2 class="pageheader-title">Account Settings</h2>
                            </div>
                        </div>
                    </div>
					<?php $this->load->view('messages'); ?>
                    <div class="row mb-5 text-center">
                        <div class="col-lg-12 p-2">
                            <div class="hiring-box pt-3 pb-3">
                                <div class="container pt-2">
                                    <div class="row">
                                    	<div class="col-md-2"></div>
                                        <div class="col-md-8 pl-4">
                                        	<?php echo form_open('change-user-info'); ?>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <h4 class="text-left m-0 mb-1"><b>First Name</b></h4>
                                                    <input type="text" name="user_first_name" value="<?= $this->session->userdata['user']['user_first_name'] ?>">
                                                </div>
                                                <div class="col-lg-6">
                                                    <h4 class="text-left m-0 mb-1"><b>Last Name</b></h4>
                                                    <input type="text" name="user_last_name" value="<?= $this->session->userdata['user']['user_last_name'] ?>">
                                                </div>
                                                <div class="col-lg-12">
                                                    <h4 class="text-left m-0 mb-1"><b>Email Address</b></h4>
                                                    <input type="text" name="user_email" value="<?= $this->session->userdata['user']['user_email'] ?>">
                                                </div>
                                                <div class="col-md-12 pt-2 pb-2">
                                                	<button class="site-btn m-2" type="submit">Save Changes</button>
                                                </div>
                                            </div>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 p-2">
                            <div class="hiring-box pt-3 pb-3">
                                <div class="container pt-2">
                                	<?php echo form_open_multipart('change-user-password'); ?>
                                    <div class="row">
                                    	<div class="col-md-3"></div>
                                        <div class="col-md-6 pl-4">
                                            <div class="row">
                                            	<?php echo form_open('change-user-info'); ?>
                                                <div class="col-lg-12">
                                                    <h4 class="text-left m-0 mb-1"><b>Current Password</b></h4>
                                                    <input type="password" name="user_password">
                                                </div>
                                                <div class="col-lg-12">
                                                    <h4 class="text-left m-0 mb-1"><b>New Password</b></h4>
                                                    <input type="password" name="new_password">
                                                </div>
                                                <div class="col-lg-12">
                                                    <h4 class="text-left m-0 mb-1"><b>Confirm New Password</b></h4>
                                                    <input type="password" name="confirm_new_password">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 pt-2 pb-2">
                                            <div class="row">
                                            	<div class="col-md-4 text-center mt-3 mb-3">
                                                    <a href="<?= base_url("logout") ?>" class="m-2 p-0">Logout</a>
                                                </div>
                                                <div class="col-md-4 text-center">
                                                	<button class="site-btn m-2" type="submit">Save Changes</button>
                                                </div>
                                                <div class="col-md-4 text-center mt-3 mb-3">
                                                    <button type="button" class="deactivate-modal-btn" data-toggle="modal" data-target="#myModal">Deactivate my account</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <i class="far fa-frown fa-6x"></i>
                                    <h3 class="pt-2" style="color: #333;"><b>We're sad to see you go</b></h3>
                                </div>
                                <div class="col-md-12 pt-3">
                                    <p class="m-0">This action can't be undone. Please make sure you want to close your account and permanently delete all your data.</p>
                                    <p class="m-0">We will not be able to recover any of your data, should you need to access your records later.</p>
                                    <br>
                                    <p class="m-0 mb-1">Your feedback matters, please tell us why you're closing your account.</p>
                                    <textarea rows="3"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <?php echo form_open('deactivate-user'); ?>
                                    <div class="col-md-12 text-center">
                                        <button type="button" class="btn btn-default cancel-btn m-2" data-dismiss="modal">Close</button>
                                        <button class="site-btn m-2" type="submit">Deactivate Account</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                      
                  </div>
              </div>
          </div>
      </div>