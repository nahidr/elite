
    
<style>
  input[type=text], select, textarea {
    background-color: #f4f4f1;
    /*box-shadow: inset 0 0 8px -1px #888;*/
    border-radius: 4px;
  }
  .label{
    color: #868685;
  }
  h3.laptop-text-h{
    color: #000;
    font-weight: 700
  }
  p.laptop-text{
    font-size: 18px;
    line-height: 40px;
  }
  .site-btn{
    border-radius: 20px;
    letter-spacing: 1px;
    padding: 12px 25px;
    font-size: 20px;
  }
  table thead{
    background-color: #EDAEB4;
  }
  table thead tr th{
    font-size: 24px;
    color: #fff;
  }
  ul.laptop-text-ul{
    list-style: none;
    text-align: left;
  }
  ul.laptop-text-ul li:before{
    content: '✓';
    color: #5c0100;
    font-weight: bolder;
  }
  ul.laptop-text-ul li{
    padding-bottom: 5px;
    font-size: 18px;
  }
  table tbody tr th.first-col{
    vertical-align: middle;
    font-size: 24px;
    overflow: hidden;
    white-space: nowrap;
    text-transform: uppercase;
  }
  table tbody tr td{
    overflow: hidden;
    white-space: nowrap;
  }
  .table-responsive {
    overflow-x: auto;
  }
  .table-bordered td, .table-bordered th {
    border: 3px solid #EDAEB4;
  }
  .circle {
    border-radius: 50%;
    width: 50px;
    height: 50px;
    background-color: rgba(170,64,63,0.8);
    display: inline-block;
    position: relative;
    }
    
    .circle.big {
        width: 70%;
        height: auto;
        padding: 185px;
        background-color: #aa403f;
    }
    
    .one {
        /*position: absolute;*/
        z-index: 1;
        width: 150px;
        height: 150px;
        left: 260px;
        top: -25px;
        border: 3px solid #fff;
    }
    
    .two {
        top: -25px;
        right: 260px;
        /*position: absolute;*/
        z-index: 1;
        width: 150px;
        height: 150px;
        border: 3px solid #fff;
    }
    
    .three {
        /*position: absolute;*/
        z-index: 1;
        width: 150px;
        height: 150px;
        left: 260px;
        bottom: -30px;
        border: 3px solid #fff;
    }
    
    
    .four {
        /*position: absolute;*/
        z-index: 1;
        width: 150px;
        height: 150px;
        right: 260px;
        bottom: -30px;
        border: 3px solid #fff;
    }
    .circle-text{
        padding-top: 55px;
        font-size: 18px;
        font-weight: 900;
        color: #000;
    }
    @media (max-width: 768px){
      .circle.big {
        width: 50px;
        height: 50px;
        padding: 133px;
        background-color: #aa403f;
    }
      .one {
        /*position: absolute;*/
        z-index: 1;
        width: 150px;
        height: 150px;
        left: 0px;
        top: -25px;
        border: 3px solid #fff;
    }
    
    .two {
        top: -25px;
        right: 0px;
        /*position: absolute;*/
        z-index: 1;
        width: 150px;
        height: 150px;
        border: 3px solid #fff;
    }
    
    .three {
        /*position: absolute;*/
        z-index: 1;
        width: 150px;
        height: 150px;
        left: 0px;
        bottom: -30px;
        border: 3px solid #fff;
    }
    
    
    .four {
        /*position: absolute;*/
        z-index: 1;
        width: 150px;
        height: 150px;
        right: 0px;
        bottom: -30px;
        border: 3px solid #fff;
    }
}
</style>

    <div class="team-section">
      <div class="container-fluid pt-5 pb-5" style="max-width: 1250px;">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="m-0 aos-init aos-animate" style="color: #5c0100; font-family: Sweet Easy;font-size: 30px;line-height: 55px;letter-spacing: 2px;" data-aos="fade"><span><b>Performance Management</b></span></h2>
          </div>
        </div>
        <div class="row pt-5 pb-5">
          <div class="col-md-3 p-0">
            <div class="row text-center">
              <div class="col-md-12">
                <div class="circle one"><p class="circle-text">ASSESS</p></div>
                <p>Lorem ipsum dolor sit amet, consectetur
                 adipiscing elit, sed do eiusmod tempor 
                 ncididunt ut labore et dolore magna aliqua. 
                 Ut enim ad minim veniam, quis nostrud
               exercitation ullamco laboris </p>
              </div>
            </div>
            <div class="row text-center">
              <div class="col-md-12">
                <div class="circle three"><p class="circle-text">IMPROVE</p></div>
                <p class="pt-5">Lorem ipsum dolor sit amet, consectetur
                 adipiscing elit, sed do eiusmod tempor 
                 ncididunt ut labore et dolore magna aliqua. 
                 Ut enim ad minim veniam, quis nostrud
               exercitation ullamco laboris </p>
              </div>
            </div>
          </div>
          <div class="col-md-6 p-0 pb-5 text-center">
            <div id="big-circle" class="circle big text-center">
              <h3 style="color: #fff;margin-top: -45px;margin-left: -65px;font-weight: 800;">EMPLOYEE PERFORMANCE MANAGEMENT</h3>
            </div>
          </div>
          <div class="col-md-3 p-0">
            <div class="row text-center">
              <div class="col-md-12">
                <div class="circle two"><p class="circle-text">TRAIN</p></div>
                <p>Lorem ipsum dolor sit amet, consectetur
                 adipiscing elit, sed do eiusmod tempor 
                 ncididunt ut labore et dolore magna aliqua. 
                 Ut enim ad minim veniam, quis nostrud
               exercitation ullamco laboris </p>
              </div>
            </div>
            <div class="row text-center">
              <div class="col-md-12">
                <div class="circle four"><p class="circle-text">REWARD</p></div>
                <p class="pt-5">Lorem ipsum dolor sit amet, consectetur
                 adipiscing elit, sed do eiusmod tempor 
                 ncididunt ut labore et dolore magna aliqua. 
                 Ut enim ad minim veniam, quis nostrud
               exercitation ullamco laboris </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row pt-5 pb-5">
          <div class="col-md-12 table-responsive">
            <table class="table table-bordered text-center">
            <thead>
              <tr>
                <th scope="col">Actions</th>
                <th scope="col">Details</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th class="first-col" scope="row">Assess</th>
                <td>
                  <ul class="laptop-text-ul">
                    <li>
                      Sed ut percipiciatis unde omnis iste natus
                    </li>
                    <li>
                      Sed ut percipiciatis unde omnis iste natus
                    </li>
                    <li>
                      Sed ut percipiciatis unde omnis iste natus
                    </li>
                  </ul>
                </td>
              </tr>
              <tr>
                <th class="first-col" scope="row">Train</th>
                <td>
                  <ul class="laptop-text-ul">
                    <li>
                      Sed ut percipiciatis unde omnis iste natus
                    </li>
                    <li>
                      Sed ut percipiciatis unde omnis iste natus
                    </li>
                    <li>
                      Sed ut percipiciatis unde omnis iste natus
                    </li>
                  </ul>
                </td>
              </tr>
              <tr>
                <th class="first-col" scope="row">Reward</th>
                <td>
                  <ul class="laptop-text-ul">
                    <li>
                      Sed ut percipiciatis unde omnis iste natus
                    </li>
                    <li>
                      Sed ut percipiciatis unde omnis iste natus
                    </li>
                    <li>
                      Sed ut percipiciatis unde omnis iste natus
                    </li>
                  </ul>
                </td>
              </tr>
              <tr>
                <th class="first-col" scope="row">Improve</th>
                <td>
                  <ul class="laptop-text-ul">
                    <li>
                      Sed ut percipiciatis unde omnis iste natus
                    </li>
                    <li>
                      Sed ut percipiciatis unde omnis iste natus
                    </li>
                    <li>
                      Sed ut percipiciatis unde omnis iste natus
                    </li>
                  </ul>
                </td>
              </tr>
            </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>
    