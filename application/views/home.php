
    <div>
      <div class="site-blocks-cover1 overlay aos-init aos-animate" style="background-image: url(&quot;assets/images/banner.png&quot;); background-size: cover; background-repeat: no-repeat; background-position: 50% 103.906px;" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row row-slider align-items-center justify-content-center text-center">
            <div class="col-md-2 text-left">
              <h5>Nanny</h5>
              <h5>Nanny/PA</h5>
              <h5>Governess/Tutor/Teacher</h5>
              <h5>Maternity Nurse</h5>
            </div>
            <div class="col-md-8">
              <h2 class="mb-3">We Make Finding The Perfect<br> Candidate Easy!</h2>
              <h3>Online platform for Parents &amp; Proffesional Staff.</h3>
              <h3>No agency fees - Save time - Hire Direct</h3>
            </div>
            <div class="col-md-2 text-left">
              <h5>Nanny/Housekeeper</h5>
              <h5>Housekeeper</h5>
              <h5>Chauffer</h5>
              <h5>Butle</h5>
            </div>
          </div>
        </div>
      </div>  
    </div>
    <div class="section pt-5 text-center">
      <div class="container">
        <div class="col-md-12">
          <h2 class="m-0" style="color: #5c0100; font-family: Sweet Easy;font-size: 24px;line-height: 55px;" data-aos="fade"><span>We are a team of experienced recruitment consultants<br> working in the background uetting candidate.</span></h2>
          <h4 class="heading" style="color: #e5b4a5;font-size: 24px;letter-spacing: 2px;line-height: 55px;" data-aos="fade"><span>Quality & Safety is our top priority</span></h4>
        </div>
        <div class="site-section pt-5 pb-2">
          <div class="container">
            <div class="row no-gutters align-items-stretch h-100">
              <div class="col-md-10 mx-auto">
                <div class="row">
                  <div class="col-lg-6 ">
                    <div class="row no-gutters req-box align-items-stretch  half-sm">
                      <div class="col-md-12 text">
                        <div class="p-4 text-left">
                         <h2 class="text-center heading" style="color: #5c0100;font-size: 24px;line-height: 55px;text-transform: uppercase;" data-aos="fade"><span>Candidates Requirements</span></h2>
                         <ul>
                          <li>
                            <span class="pl-2">Qualified/trained with 2 years minimum experience (no gaps in employment).</span>
                          </li>
                          <li>
                            <span class="pl-2">Enhanced Police &amp; ID checked Paediatric First aid Trained</span>
                          </li>
                          <li>
                            <span class="pl-2">Reference checked (we carry out one reference check from recent employer).</span>
                          </li>
                          <li>
                            <span class="pl-2">A 60 second introduction recording.</span>
                          </li>
                          <li>
                            <span class="pl-2">All documents are kept on our system.</span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-lg-6 ">
                  <div class="row no-gutters req-box align-items-stretch half-sm">
                    <div class="col-md-12  text order-md-1 order-lg-2">
                      <div class="p-4 text-left">
                        <h2 class="text-center heading" style="color: #5c0100;font-size: 24px;line-height: 55px;text-transform: uppercase;" data-aos="fade"><span>Employers Requirements</span></h2>
                        <ul>
                          <li style="font-family: Segoe UI;">
                            <span class="pl-2">
                            Create a free account and using the filter feature, find available talent that are a close match.
                            </span>
                          </li>
                          <li>
                            <span class="pl-2">Found matching profiles ...</span>
                          </li>
                          <li>
                            <span class="pl-2">Publish your vacancy using our quick template.</span>
                          </li>
                          <li>
                            <span class="pl-2">Purchase credits and start contacting candidates using message board or direct email.</span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="site-block-half d-block d-lg-flex site-block-video mb-5" data-aos="fade">
    <div class="container-fluid">
      <div class="row text-center">
        <div class="col-md-12 p-0">
          <div class="image bg-image order-2" style="background-image: url(<?= base_url() ?>assets/images/video.png);height: 470px;width: 100%;">
            <a href="https://vimeo.com/channels/staffpicks/93951774"></a>
            <h2 class="pt-5" style="font-family: Sweet Easy;color: #fff;">Employment Contract & Payroll</h2>
            <div class="play-video pt-5">
              <a href="">
                <span class="fa fa-play-circle fa-5x pt-5" style="color: #fff;"></span>
                <h6 style="color: #fff;">Watch Video</h6>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="section aboutus text-center">
    <div class="container">
      <div class="col-md-12">
        <h2 class="heading" style="color: #5c0100; font-family: Sweet Easy;font-size: 24px;line-height: 55px;" data-aos="fade"><span><b>About Us</b></span></h2>
      </div>
      <div class="col-md-11 mx-auto pt-5 pb-5">
        <p>It is a long established fact that a reader will bd tracyu ted b eadab ntent of a page when looking at its layuto 
          the point of using took psum is that it has a more thenennormal make ipsum to made easy .long established 
          fact that a eader will bd strac hen eadable tontent of a page when looking can do takent happy moment to do 
          make mle and easy. a long established fact that a reader will bd tracyu tedb eadab ntent opage wen 
          loking at its layuto the point offusing took psum is that it has a more then ennormal mako made 
          easylong esta lised fact that a reader will bd strac then eadable tontent of a page when looking
        can do akent happymoment to do make simple and easy.  </p>
      </div>
    </div>
  </div>

  <div class="contact-section pt-5 text-center">
    <div class="container">
      <div class="col-md-12">
        <h2 class="heading" style="color: #5c0100; font-family: Sweet Easy;font-size: 24px;line-height: 55px;" data-aos="fade">
          <span>
            <b>Contact Us</b>
          </span>
        </h2>
      </div>
      <div class="col-md-12 pt-5 pb-5">
        <div class="row">
          <div class="col-md-5 text-left">
            <h4>Don't hesitate to drop us a line.</h4>
            <p>It is a long established fact that a reader will be the<br> hole world took all then we do the best work for tmake lished fact that reader</p>
            <div class="row">
              <div class="col-md-9">
                <ul>
                  <li style="list-style: none;">
                    <div class="row pt-2 pb-2">
                      <div class="col-md-2">
                        <span class="fa fa-map-marker fa-2x"></span>
                      </div>
                      <div class="col-md-10" >
                        <p class="m-0">4946 Marmora Road</p>
                        <p class="m-0">London UK</p>
                      </div>
                    </div>
                    <div class="row pt-2 pb-2">
                      <div class="col-md-2">
                        <span class="fa fa-phone fa-2x"></span>
                      </div>
                      <div class="col-md-10" >
                        <p class="m-0"> +92 3746 383738</p>
                        <p class="m-0"> +92 3746 383738</p>
                      </div>
                    </div>
                    <div class="row pt-2 pb-2">
                      <div class="col-md-2">
                        <span class="fa fa-envelope-o fa-2x"></span>
                      </div>
                      <div class="col-md-10" >
                        <p class="m-0"> abc@gmail.com</p>
                        <p class="m-0"> abc@gmail.com</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>

          </div>
          <div class="col-md-7">
            <div class="row" id="placeholder-input">
              <div class="col-md-6">
                <input type="text" name="" placeholder="First Name*">
              </div>
              <div class="col-md-6">
                <input type="text" name="" placeholder="Last Name*">
              </div>
              <div class="col-md-6">
                <input type="text" name="" placeholder="Email*">
              </div>
              <div class="col-md-6">
                <input type="text" name="" placeholder="Phone*">
              </div>
              <div class="col-md-12">
                <textarea placeholder="Message*" style="height: 160px"></textarea>
              </div>
              <div class="col-lg-12 pt-2 pb-2">
                <div class="Search-btn text-center">
                  <a href="#" class="site-btn">Send</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
