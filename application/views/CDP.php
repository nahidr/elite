<div class="about-section">
      <div class="container pt-5 pb-5">
        <div class="row pb-5">
          <div class="col-md-10 mx-auto">
            <h2 class="text-center m-0 aos-init aos-animate pt-2 pb-5" style="color: #5c0100; font-family: Sweet Easy;font-size: 24px;line-height: 55px;letter-spacing: 2px;" data-aos="fade"><span><b>CDP</b></span></h2>
            <p class="text-left">It is a long established fact that a reader will be distracted by the readable content of a page when 
              looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many 
            desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their inf</p>
          </div>
        </div>
      </div>
    </div>

    <div class="team-section">
      <div class="container pt-5 pb-5">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="m-0 aos-init aos-animate" style="color: #5c0100; font-family: Sweet Easy;font-size: 30px;line-height: 55px;letter-spacing: 2px;" data-aos="fade"><span><b>Training Courses</b></span></h2>
          </div>
          <div class="col-md-3 pt-5 text-center">
            <a href="#" class="courses-btn">Paediatric First Aid</a>
          </div>
          <div class="col-md-3 pt-5 text-center">
            <a href="#" class="courses-btn">Emergency First Aid</a>
          </div>
          <div class="col-md-3 pt-5 text-center">
            <a href="#" class="courses-btn">Emergency First Aid</a>
          </div>
          <div class="col-md-3 pt-5 text-center">
            <a href="#" class="courses-btn">Emergency First Aid</a>
          </div>

          <div class="col-md-3 pt-5 text-center">
            <a href="#" class="courses-btn">Paediatric First Aid</a>
          </div>
          <div class="col-md-3 pt-5 text-center">
            <a href="#" class="courses-btn">Emergency First Aid</a>
          </div>
          <div class="col-md-3 pt-5 text-center">
            <a href="#" class="courses-btn">Emergency First Aid</a>
          </div>
          <div class="col-md-3 pt-5 text-center">
            <a href="#" class="courses-btn">Emergency First Aid</a>
          </div>
        </div>
      </div>
    </div>