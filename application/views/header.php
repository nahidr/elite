<!DOCTYPE html>
<html lang="en">
<head>
  <title>Elite Private Staff</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href='https://fonts.googleapis.com/css?family=Baloo Bhaijaan' rel='stylesheet'>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900|Roboto+Mono:300,400,500"> 
  <link rel="stylesheet" href="<?= base_url() ?>assets/fonts/icomoon/style.css">

  <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/magnific-popup.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/jquery-ui.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.theme.default.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-datepicker.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/fonts/flaticon/font/flaticon.css">
  
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/aos.css">

  <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
  <style>
  @font-face { font-family: Sweet Easy; src: url('<?= base_url() ?>assets/assets/fonts/Sweet Easy personal Use.ttf');}
  @font-face { font-family: Segoe UI;  src: url('<?= base_url() ?>assets/assets/fonts/segoeui.ttf');}
  input[type=text], input[type=email], input[type=password], select, textarea {
    background-color: #f4f4f1;
    /*box-shadow: inset 0 0 8px -1px #888;*/
    border-radius: 4px;
  }
  .label{
    color: #868685;
  }
  .process{
    padding-top: 20px;
    width: 150px;
    margin: 0 auto;
    height: 150px;
    background: #fff;
  }
  .process h2, .process h4{
    color: #5c0100;
  }
  .banner-btn{
    position: relative;
    display: inline-block;
    padding: 10px 20px;
    font-size: 16px;
    font-weight: 600;
    line-height: 16px;
    letter-spacing: 2px;
    border-radius: 20px;
    min-width: 100px;
    text-align: center;
    background: #fff;
    cursor: pointer;
    border: none;
    color: #5c0100;
  }
  .banner h4{
    font-size: 20px;
    padding-top: 15px;
    color: #fff;
  }
  .banner{
    padding: 20px;
    background: #5c0100;
  }
  body{
    /*background-color: #eaeae8;*/
  }
  hr.style-eight {
    overflow: visible; /* For IE */
    padding: 0;
    border: none;
    border-top: 5px solid #fff;
    color: #333;
    text-align: center;
    width: 70%;
    margin-bottom: 100px;
    }
    hr.style-eight:after {
        content: "O";
        display: inline-block;
        position: relative;
        top: -0.7em;
        font-size: 1.5em;
        padding: 0 0.25em;
        background: white;
    }
    .site-btn{
    border-radius: 20px;
    letter-spacing: 1px;
    padding: 12px 25px;
    font-size: 20px;
  }
  .min-h{min-height: 380px;}
</style>
</head>
<body>

  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->

    <div class="site-navbar-wrap bg-white">
      <div class="site-navbar-top ">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-3 banner-pic">
              <div class="d-flex ml-auto">
                <a href="#" class="d-flex align-items-center ml-auto mr-4">
                  <!-- <span class="icon-phone mr-2"></span> -->
                  <span class="d-none d-md-inline-block"><img src="<?= base_url() ?>assets/images/top-bar-pic1.png" class="image-responsive"></span>
                </a>
                <a href="#" class="d-flex align-items-center">
                  <!-- <span class="icon-envelope mr-2"></span> -->
                  <span class="d-none d-md-inline-block"><img src="<?= base_url() ?>assets/images/top-bar-pic2.png" class="image-responsive"></span>
                </a>
              </div>
            </div>
            <div class="col-6" style="padding: 5px 15px;">
              <a href="<?= base_url() ?>"><img src="<?= base_url() ?>assets/images/logo.png" class="pb-2 pt-2 image-responsive"></a>
            </div>
            <div class="col-2 mx-auto text-right text-center">
          	<?php if (isset($this->session->userdata['user']['user_id'])) { ?>
          		<!-- <div class="d-flex ml-auto login-btn" style="width: 95px;"> -->
          		<div class="d-flex ml-auto login-btn">
          			<?php
          			if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 1) {
          			?>
              			<a href="<?= base_url('candidate-registration') ?>" class="d-flex align-items-center ml-4">
                          <!-- <span class="icon-phone mr-2"></span> -->
                          <span class="d-none d-md-inline-block">Dash</span>
                        </a>
                    <?php
          			}else if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 2) {
          			?>
              			<a href="<?= base_url('parent-dash') ?>" class="d-flex align-items-center ml-4">
                          <!-- <span class="icon-phone mr-2"></span> -->
                          <span class="d-none d-md-inline-block">Dash</span>
                        </a>
          			<?php
          			}
          			?>
          			<a href="<?= base_url('logout') ?>" class="d-flex align-items-center ml-4">
                      <!-- <span class="icon-phone mr-2"></span> -->
                      <span class="d-none d-md-inline-block">Logout </span>
                    </a>
                </div>
          	<?php } else { ?>
          		<div class="d-flex ml-auto login-btn">
                    <a href="<?= base_url('parent-login') ?>" class="d-flex align-items-center ml-4">
                      <!-- <span class="icon-phone mr-2"></span> -->
                      <span class="d-none d-md-inline-block">Login</span>
                    </a>
                    
                    <a href="<?= base_url('parent-signup') ?>" class="d-flex align-items-center ml-4">
                      <!-- <span class="icon-envelope mr-2"></span> -->
                      <span class="d-none d-md-inline-block">Sign Up</span>
                    </a>
                </div>
            <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <div class="site-navbar">
        <div class="container pt-1">
          <div class="row align-items-center menu">

            <div class="col-10 mx-auto">
              <nav class="site-navigation" role="navigation">
                <div class="container">
                  <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3 pull-right"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

                  <ul class="site-menu js-clone-nav d-none d-lg-block">
                    <li class="<?= ($this->session->userdata['page_name'] == "Home")? "active" : "" ?>"><a href="<?= base_url() ?>">Home</a></li>
                    <?php
                        $check = "";
                        $check1 = ($this->session->userdata['page_name'] == "Hiring Process")? true : false;
                        $check2 = ($this->session->userdata['page_name'] == "Employer Toolkit")? true : false;
                        $check3 = ($this->session->userdata['page_name'] == "Household Staff")? true : false;
                        $check4 = ($this->session->userdata['page_name'] == "Performance Management")? true : false;
                        if($check1 || $check2 || $check3 || $check4){
                            $check = "active";
                        }else{
                            $check = "";
                        }
                    ?>
                    <li class="dropdown <?= $check ?>"><a class="dropdown-toggle" data-toggle="dropdown" href="#">For Employers<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li class="<?= ($this->session->userdata['page_name'] == "Hiring Process")? "active" : "" ?>"><a href="<?= base_url("hiring-process") ?>">Hiring Process</a></li>
                        <li class="<?= ($this->session->userdata['page_name'] == "Employer Toolkit")? "active" : "" ?>"><a href="<?= base_url("employer-toolkit") ?>">Employer Toolkit</a></li>
                        <li class="<?= ($this->session->userdata['page_name'] == "Household Staff")? "active" : "" ?>"><a href="<?= base_url("household-staff") ?>">Household Staff</a></li>
                        <!-- <li class="<?= ($this->session->userdata['page_name'] == "Performance Management")? "active" : "" ?>"><a href="<?= base_url("performance-management") ?>">Performance Management</a></li> -->
                      </ul>
                    </li>
                    <?php
                        $check = "";
                        $check1 = ($this->session->userdata['page_name'] == "CDP")? true : false;
                        $check2 = ($this->session->userdata['page_name'] == "Get Hired")? true : false;
                        if($check1 || $check2){
                            $check = "active";
                        }else{
                            $check = "";
                        }
                    ?>
                    <li class="dropdown <?= $check ?>"><a class="dropdown-toggle" data-toggle="dropdown" href="#">For Candidates<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li class="<?= ($this->session->userdata['page_name'] == "CDP")? "active" : "" ?>"><a href="<?= base_url("cdp") ?>">CDP</a></li>
                        <li class="<?= ($this->session->userdata['page_name'] == "Get Hired")? "active" : "" ?>"><a href="<?= base_url("get-hired") ?>">Get Hired</a></li>
                      </ul>
                    </li>
                    <li class="<?= ($this->session->userdata['page_name'] == "Contact")? "active" : "" ?>"><a href="<?= base_url("contact") ?>">Contact Us</a></li>
                    <li class="<?= ($this->session->userdata['page_name'] == "About")? "active" : "" ?>"><a href="<?= base_url("about") ?>">About Us</a></li>
                    <li class="<?= ($this->session->userdata['page_name'] == "GDPR")? "active" : "" ?>"><a href="<?= base_url("gdpr") ?>">GDPR</a></li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>