<!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container dashboard-content">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    
                    <div class="row">
                        <div class="col-md-11 mx-auto welcome-msg">
                            <p>Welcome Talent. We bring to you professional families seeking top talent. <br>
                                You have completed your registration process.<br> One of our consultants will verify your profile and documents. If successful, you will receive an email to guide you on paying the &pound;19.99 administration registration fee for your account to go live.<br> Please keep checking your email account for our email.</p>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="hiring pl-3 pt-3">
                                            <h3>Inbox</h3>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 pt-3 col-12">
                                        <div class="inbox-box ">
                                        	<div class="text-right pt-3">
                                                <a href="<?= base_url("can-inbox") ?>">View Inbox</a>
                                            </div>
                                            <div class="inbox-sec active">
                                                <h3>Lilly Howell</h3>
                                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam</p>
                                            </div>
                                            <div class="inbox-sec">
                                                <h3>Lilly Howell</h3>
                                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam</p>
                                            </div>
                                            <div class="inbox-sec">
                                                <h3>Lilly Howell</h3>
                                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6 pt-5 pb-2">
                                        <div class="container">
                                            <div class="row p-2 bg-white">
                                                <div class="col-sm-2 p-0">
                                                    <img src="<?= base_url() ?>assets/assets/images/jobs.png">
                                                </div>
                                                <div class="col-sm-7 pl-2">
                                                    <h5 class="m-0">Favourited</h5>
                                                </div>
                                                <div class="col-sm-3 text-right pl-2">
                                                    <h5 class="m-0">6</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pt-5 pb-2">
                                        <div class="container p-0">
                                            <div class="row p-2 bg-white">
                                                <div class="col-sm-2 p-0">
                                                    <img src="<?= base_url() ?>assets/assets/images/eye.png" width="36px">
                                                </div>
                                                <div class="col-sm-7 p-0 pl-2">
                                                    <h5 class="m-0">Viewed your profile</h5>
                                                </div>
                                                <div class="col-sm-3 text-right pl-2">
                                                    <h5 class="m-0">51</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="hiring pl-3 pt-3">
                                            <h3>Activity feed</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="hiring-box pt-3 pb-2">
                                           <div class="pt-3 pl-5 pr-5 pb-3" style="padding: 0px;">
                                            <div class="text-right pb-2" style="padding: 0px;">
                                                <a href="<?= base_url("mark-read") ?>">Mark all as read</a>
                                            </div>
                                            <?php
                                            if($interested){
                                                foreach ($interested as $int){
                                                    echo "<p>You showed interest in a <a style='padding: 0px;' href='".base_url("view-jobs")."'>job post</a>.</p>";
                                                }
                                            }
                                            ?>
<!--                                             <p>Someone just showed interest in your job post.</p> -->
<!--                                             <p>Someone just showed interest in your job post.</p> -->
<!--                                             <p>Someone just showed interest in your job post.</p> -->
<!--                                             <p>Someone just showed interest in your job post.</p> -->
<!--                                             <p>Someone just showed interest in your job post.</p> -->
<!--                                             <p>Someone just showed interest in your job post.</p> -->
<!--                                             <p>Someone just showed interest in your job post.</p> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>