<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <title>Elite Private Staff</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="../assets/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/assets/libs/css/style.css">
    <link rel="stylesheet" href="../assets/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="../assets/assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="../assets/assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="../assets/assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../assets/assets/vendor/charts/c3charts/c3.css">
    <link rel="stylesheet" href="../assets/assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
    <style>
    .form-control {
        height: 45px;
    }
    .Search input[type=text],.Search select,.Search textarea {
        height: 45px !important;
        border-radius: 0px;
        border: 1px solid #ced4da;
        width: 100%;
    }
    .modal-body {
      position:relative;
      padding:0px;
  }
  .close {
      position:absolute;
      right:-30px;
      top:0;
      z-index:999;
      font-size:2rem;
      font-weight: normal;
      color:#fff;
      opacity:1;
  }
  .modal-dialog {
      max-width: 800px;
      margin: 50px auto;
  }
  .job-specs h2, .job-specs h3{
    color: #5c0100;
    font-weight: 700;
  }
  .job-specs ul li{
    list-style: none;
}
@font-face { font-family: Sweet Easy; src: url('../assets/assets/fonts/Sweet Easy personal Use.ttf');}
@font-face { font-family: Segoe UI;  src: url('../assets/assets/fonts/segoeui.ttf');}
@font-face { font-family: Raleway;  src: url('../assets/assets/fonts/Raleway-Regular.ttf');}
</style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg fixed-top">
                <a class="navbar-brand" id="marg-0-auto" href="dashboard.html"><img src="../assets/images/logo.png" width="250px"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav nav-menu">
                        <li class="nav-link">
                            <a href="../home.html">Home</a>
                        </li>
                        <li class="nav-link">
                            <a href="../contact.html">Contact us</a>
                        </li>
                        <li class="nav-link">
                            <a href="../about.html">About us</a> 
                        </li>
                        <li class="nav-link">
                            <a href="../gdpr.html">GDPR</a> 
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/assets/images/profile.png" alt="" class="user-avatar-md rounded-circle"><span class="fa fa-caret-down pl-1 fa-2x"></span></a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2" style="top: 90%; right: 10%; background: #f3e2dd; border-top-right-radius: 13px; border-bottom-left-radius: 13px;">
                                <a class="dropdown-item" href="#">Edit Profile</a>
                                <hr class="m-0">
                                <a class="dropdown-item" href="../login-candidate.html">Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                <div class="container-fluid">
                                    <div class="row text-center pt-2 pb-2">
                                        <div class="col-md-12 sidebar-profile">
                                            <span>Welcome to your account</span>
                                            <h4>Sharan Gill</h4>
                                            <img src="../assets/assets/images/profile.png">
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
<a class="nav-link" href="dashboard.html" aria-expanded="false"><i class="fa fa-fw fa-home"></i>Dashboard</a>
</li>
<li class="nav-item ">
<a class="nav-link active" href="searchjobs.html" aria-expanded="false"><i class="fa fa-fw fa-search"></i>Search Jobs</a>
</li>
<li class="nav-item ">
<a class="nav-link" href="likedjobs.html" aria-expanded="false"><i class="fa fa-fw fa-heart"></i>Liked Jobs</a>
</li>
<li class="nav-item ">
<a class="nav-link" href="inbox.html" aria-expanded="false"><i class="fa fa-fw fa-envelope"></i>Inbox</a>
</li>
<li class="nav-item ">
<a class="nav-link" href="settings.html" aria-expanded="false"><i class="fa fa-fw fa-cog"></i>Settings</a>
</li>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-header pl-2 pt-3">
                                <h2 class="pageheader-title">Jobs Feeds</h2>
                            </div>
                        </div>
                    </div>
                    
                    <div class="container">
                        <div class="row Search mb-5">
                         <div class="col-md-10 text-center mx-auto">
                           <div class="row">
                             <div class="col-lg-7 p-0">
                               <input type="text" class="form-control input-sm" placeholder="Web Designer">
                           </div>
                           <div class="col-lg-3 p-0">
                               <select class="form-control" id="exampleFormControlSelect1">
                                 <option>Cateogories</option>
                             </select>
                         </div>
                         <div class="col-lg-2 p-0">
                           <button href="" style="padding: 10px 60px;border:1px solid #e0bb75; background-color: #fe8d91;border-radius: 0px;"><span class="fa fa-search" style="color:white; font-size: 22px;"></span></button>
                       </div>
                   </div>
               </div>
           </div>
           <div class="row jobs-row mt-2 mb-4">
            <div class="col-md-12 p-0">
                <h3>
                    <a href="viewjob.html">Full time Nanny</a>
                    <span class="fa fa-heart" style="font-size: 18px;float: right;"></span>
                </h3>
            </div>
            <div class="col-md-10 p-0">
                <div class="jobs">
                    <div class="title">
                        <div class="container">
                            <div class="row">

                            </div>
                        </div>
                    </div>
                    <div class="type">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="type-1">Job Type</label>
                                    <h5 class="type-2">Full Time Job</h5>
                                </div>
                                <div class="col-md-3">
                                    <label class="type-1">Job Shift</label>
                                    <h5 class="type-2">Morning</h5>
                                </div>
                                <div class="col-md-3">
                                    <label class="type-1">Experience</label>
                                    <h5 class="type-2">3 Years</h5>
                                </div>
                                <div class="col-md-3">
                                    <label class="type-1">Salary</label>
                                    <h5 class="type-2">$700 - $850</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="job-detail pb-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row jobs-row mt-2 mb-4">
            <div class="col-md-12 p-0">
                <h3>
                    <a href="viewjob.html">Full time Nanny</a>
                    <span class="fa fa-heart active" style="font-size: 18px;float: right;"></span>
                </h3>
            </div>
            <div class="col-md-10 p-0">
                <div class="jobs">
                    <div class="title">
                        <div class="container">
                            <div class="row">

                            </div>
                        </div>
                    </div>
                    <div class="type">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="type-1">Job Type</label>
                                    <h5 class="type-2">Full Time Job</h5>
                                </div>
                                <div class="col-md-3">
                                    <label class="type-1">Job Shift</label>
                                    <h5 class="type-2">Morning</h5>
                                </div>
                                <div class="col-md-3">
                                    <label class="type-1">Experience</label>
                                    <h5 class="type-2">3 Years</h5>
                                </div>
                                <div class="col-md-3">
                                    <label class="type-1">Salary</label>
                                    <h5 class="type-2">$700 - $850</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="job-detail pb-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row jobs-row mt-2 mb-4">
            <div class="col-md-12 p-0">
                <h3>
                    <a href="viewjob.html">Full time Nanny</a>
                    <span class="fa fa-heart" style="font-size: 18px;float: right;"></span>
                </h3>
            </div>
            <div class="col-md-10 p-0">
                <div class="jobs">
                    <div class="title">
                        <div class="container">
                            <div class="row">

                            </div>
                        </div>
                    </div>
                    <div class="type">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="type-1">Job Type</label>
                                    <h5 class="type-2">Full Time Job</h5>
                                </div>
                                <div class="col-md-3">
                                    <label class="type-1">Job Shift</label>
                                    <h5 class="type-2">Morning</h5>
                                </div>
                                <div class="col-md-3">
                                    <label class="type-1">Experience</label>
                                    <h5 class="type-2">3 Years</h5>
                                </div>
                                <div class="col-md-3">
                                    <label class="type-1">Salary</label>
                                    <h5 class="type-2">$700 - $850</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="job-detail pb-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5 mb-5">
            <div class="col-md-12 pt-5 pb-5 text-center">
                <a href="#" class="site-btn">Load More</a>
            </div>
        </div>
    </div>
</div>
</div>
<!--<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">-->
<!--  <div class="modal-dialog" role="document">-->
<!--    <div class="modal-content">-->


<!--      <div class="modal-body">-->

<!--         <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--          <span aria-hidden="true">&times;</span>-->
<!--      </button>        -->
<!--      <div class="container">-->
<!--        <div class="row pt-5 pb-5">-->
<!--            <div class="col-md-8 mx-auto">-->
<!--                <div class="row text-center job-specs">-->
<!--                    <div class="col-md-12">-->
<!--                        <h2>Full Time Nanny/stay in</h2>-->
<!--                        <label class="mb-0">London, UK</label>-->
<!--                        <p><label>Posted 6 days ago&emsp;<span class="fa fa-eye"> 23 Views</span></label></p>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="row job-specs">-->
<!--                    <div class="col-md-12">-->
<!--                        <h3>Job Specifications</h3>-->
<!--                        <ul>-->
<!--                            <li>-->
<!--                                <div class="row">-->
<!--                                    <div class="col-md-6">-->
<!--                                        <h4><b>Job Shift:</b></h4>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-6">-->
<!--                                        <h4>Morning Shift</h4>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <div class="row">-->
<!--                                    <div class="col-md-6">-->
<!--                                        <h4><b>Job Type:</b></h4>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-6">-->
<!--                                        <h4>3 Months</h4>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <div class="row">-->
<!--                                    <div class="col-md-6">-->
<!--                                        <h4><b>Experience:</b></h4>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-6">-->
<!--                                        <h4>2 Years</h4>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <div class="row">-->
<!--                                    <div class="col-md-6">-->
<!--                                        <h4><b>Apply Before:</b></h4>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-6">-->
<!--                                        <h4>Dec 29, 2018</h4>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="row job-specs mb-2">-->
<!--                    <h3>Job Description</h3>-->
<!--                    <ul>-->
<!--                        <li>-->
<!--                         <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here.</p>   -->
<!--                     </li>-->
<!--                 </ul>-->
<!--             </div>-->
<!--             <div class="row mb-2">-->
<!--                 <div class="col-md-12 text-center">-->
<!--                     <a href="" class="site-btn">Show Interest</a>-->
<!--                 </div>-->
<!--             </div>-->
<!--         </div>-->
<!--     </div>-->
<!-- </div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!-- ============================================================== -->
<!-- footer -->
<!-- ============================================================== -->
<div class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="mx-auto col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                © Elite Private Staff 2019. All rights reserved.
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- end footer -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper  -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->
<!-- jquery 3.3.1 -->
<script>
    $(document).ready(function() {
        var $videoSrc;  
        $('.video-btn').click(function() {
            $videoSrc = $(this).data( "src" );
        });
        console.log($videoSrc);
        $('#myModal').on('shown.bs.modal', function (e) {
            $("#video").attr('src',$videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" ); })
    });
</script>
<script src="../assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
<!-- bootstap bundle js -->
<script src="../assets/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
<!-- slimscroll js -->
<script src="../assets/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
<!-- main js -->
<script src="../assets/assets/libs/js/main-js.js"></script>
<!-- chart chartist js -->
<script src="../assets/assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
<!-- sparkline js -->
<script src="../assets/assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
<!-- morris js -->
<script src="../assets/assets/vendor/charts/morris-bundle/raphael.min.js"></script>
<script src="../assets/assets/vendor/charts/morris-bundle/morris.js"></script>
<!-- chart c3 js -->
<script src="../assets/assets/vendor/charts/c3charts/c3.min.js"></script>
<script src="../assets/assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
<script src="../assets/assets/vendor/charts/c3charts/C3chartjs.js"></script>
<script src="../assets/assets/libs/js/dashboard-ecommerce.js"></script>
</body>

</html>