<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <title>Elite Private Staff</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="<?= base_url() ?>assets/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/libs/css/style.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/multiforms.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/vendor/charts/c3charts/c3.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
    <style>
    @font-face { font-family: Sweet Easy; src: url('<?= base_url() ?>assets/fonts/Sweet Easy personal Use.ttf');}
    @font-face { font-family: Segoe UI;  src: url('<?= base_url() ?>assets/fonts/segoeui.ttf');}
    @font-face { font-family: Raleway;  src: url('<?= base_url() ?>assets/fonts/Raleway-Regular.ttf');}
    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 700;
        font-size: 18px;
        color: #666;
    }
    .welcome-msg p{
        color: #333;
        font-weight: 600;
    } 
    .personal-details h2{
        color: #333;
        font-weight: 700;
        font-size: 25px;
        letter-spacing: 1.2px;
    }
    .check-btn input { display:none; }
    .check-btn input:checked ~ label {     
        color: #fff;
        background: #5c0100; }
        .check-btn label{
            background: #e5b4a5;
            min-width: 100px;
            border-radius: 15px;
            padding: 7px 25px;
            margin-bottom: 10px;
        }
        .check-btn span{
            padding: 10px 10px;
        }
        input.rButton {
         margin-bottom: 0px !important; 
         display: inline-block;
         width: 17px !important;
         height: 17px !important;
         vertical-align: middle;
     }
     .convText {
        font-family:'Segoe UI';
        font-size:15px;
        vertical-align: middle;
    }
    .uploadfile .form {
        border: 1px dashed #3d405c;
        height: 175px;
    }
    .uploadfile .form input {
        margin: 0;
        padding: 0;
        width: 100%;
        height: 100%;
        outline: none;
        opacity: 0;
    }
    .img-txt{
        margin-top: -160px;
    }
    .uploadfile .form p {
        margin-top: 15px;
        text-align: center;
        color: #3d405c;
    }
    a.browse-btn{
        border-radius: 0px;
        color: #fff;
        background-image: linear-gradient(to right, #f6dcd3 , #000, #000, #f6dcd3);
        padding: 10px 25px;
        font-weight: 100;
        border: none;
    }
    #progressbar li:before {
        margin: 0 70px 10px 70px;
    }
    input[type=text], select, textarea {
        background-color: #f4f4f1;
        /*box-shadow: inset 0 0 8px -1px #888;*/
        border-radius: 4px;
      }
      .label{
        color: #868685;
      }
</style>
<script type="text/javascript">
    var BASE_URL = "<?php echo base_url(); ?>";
    var registration_step = "<?= isset($status)? $status : 1 ?>";
//     console.log(registration_step);
</script>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg fixed-top">
                <a class="navbar-brand" id="marg-0-auto" href="<?= base_url("candidate-registration") ?>"><img src="<?= base_url() ?>assets/images/logo.png" width="250px"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav nav-menu">
                        <li class="nav-link active">
                            <a href="<?= base_url() ?>">Home</a>
                        </li>
                        <li class="nav-link">
                            <a href="<?= base_url("contact") ?>">Contact us</a>
                        </li>
                        <li class="nav-link">
                            <a href="<?= base_url("about") ?>">About us</a> 
                        </li>
                        <li class="nav-link">
                            <a href="<?= base_url("gdpr") ?>">GDPR</a> 
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?= base_url() ?>assets/assets/images/profile.png" alt="" class="user-avatar-md rounded-circle"><span class="fa fa-caret-down pl-1 fa-2x"></span></a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2" style="top: 90%; right: 10%; background: #f3e2dd; border-top-right-radius: 13px; border-bottom-left-radius: 13px;">
                                <a class="dropdown-item" href="<?= base_url("can-settings") ?>">Edit Profile</a>
                                <hr class="m-0">
                                <a class="dropdown-item" href="<?= base_url("logout") ?>">Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="<?= base_url("candidate-registration") ?>">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                <div class="container-fluid">
                                    <div class="row text-center pt-2 pb-2">
                                        <div class="col-md-12 sidebar-profile">
                                            <span>Welcome to your account</span>
                                            <h4><?= $this->session->userdata['user']['user_first_name']." ".$this->session->userdata['user']['user_last_name'] ?></h4>
                                            <?php $pic = $this->session->userdata['user']['user_picture']; ?>
                                            <img class="rounded-circle" src="<?= base_url() ?>uploads/candidate/<?= $pic ?>">
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                            	<div style="text-align: center;">
                            		<button class="site-btn" style="background-color: #FCBCA1" id="trigger_file_selector" type="button"><i class="fa fa-camera" aria-hidden="true"></i> Change Profile Picture</button>
                            	</div>
                            	<input style="display: none;" type="file" class="custom-file-input" id="inputGroupFile01" name="userfile" aria-describedby="inputGroupFileAddon01">
                                <input type="hidden" name="user_type_global" id="user_type_global" value="1">
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?= ($this->session->userdata['page_name'] == "Candidate Dashboard")? "active" : "" ?>" href="<?= base_url("candidate-registration") ?>" aria-expanded="false"><i class="fa fa-fw fa-home"></i>Dashboard</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?= ($this->session->userdata['page_name'] == "View Jobs")? "active" : "" ?>" href="<?= base_url("view-jobs") ?>" aria-expanded="false"><i class="fa fa-fw fa-search"></i>Browse Jobs</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?= ($this->session->userdata['page_name'] == "Shortlisted Jobs")? "active" : "" ?>" href="<?= base_url("shortlisted-jobs") ?>" aria-expanded="false"><i class="fa fa-fw fa-heart"></i>Shorlisted Jobs</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?= ($this->session->userdata['page_name'] == "Candidate Inbox")? "active" : "" ?>" href="<?= base_url("can-inbox") ?>" aria-expanded="false"><i class="fa fa-fw fa-envelope"></i>Inbox</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?= ($this->session->userdata['page_name'] == "Candidate Settings")? "active" : "" ?>" href="<?= base_url("can-settings") ?>" aria-expanded="false"><i class="fa fa-fw fa-cog"></i>Settings</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->