<!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="mx-auto col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        &copy; Elite Private Staff 2018. All rights reserved.
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end wrapper  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper  -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->


<?php if($this->session->userdata['page_name'] == "Candidate registration"){ ?>
    <!-- jquery 3.3.1 -->
    <script src="<?= base_url() ?>assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="<?= base_url() ?>assets/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="<?= base_url() ?>assets/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="<?= base_url() ?>assets/assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="<?= base_url() ?>assets/assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="<?= base_url() ?>assets/assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="<?= base_url() ?>assets/assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="<?= base_url() ?>assets/assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
    <script>window.jQuery || document.write('<script src="<?= base_url() ?>assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/js/msform.js"></script>
<?php } else { ?>
	<script src="<?= base_url() ?>assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="<?= base_url() ?>assets/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="<?= base_url() ?>assets/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="<?= base_url() ?>assets/assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="<?= base_url() ?>assets/assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="<?= base_url() ?>assets/assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="<?= base_url() ?>assets/assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="<?= base_url() ?>assets/assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="<?= base_url() ?>assets/assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="<?= base_url() ?>assets/assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="<?= base_url() ?>assets/assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="<?= base_url() ?>assets/assets/libs/js/dashboard-ecommerce.js"></script>
    <script type="text/javascript">
    	$("#trigger_file_selector").on('click',function(){
        	$("#inputGroupFile01").click();
        });
        $("input[type=file]").on('change',function(){
//             alert(this.files[0].name);
			var name = this.files[0].name;
            var form_data = new FormData();
        	var ext = name.split('.').pop().toLowerCase();
        	if(jQuery.inArray(ext, ['png','jpg','jpeg']) == -1) 
        	{
        		alert("Invalid Image File");
        	}else{
        		var oFReader = new FileReader();
            	oFReader.readAsDataURL(this.files[0]);
            	var f = this.files[0];
            	var fsize = f.size||f.fileSize;
            	var type = $("#user_type_global").val();
            	if(fsize > 2000000)
            	{
            		alert("Image File Size is very big");
            	}
            	else
            	{
            		form_data.append("userfile", this.files[0]);
            		console.log(form_data);
            		$.ajax({
            			url:BASE_URL+"update-profile-picture/"+type,
            			method:"POST",
            			data: form_data,
            			contentType: false,
            			cache: false,
            			processData: false,
            			beforeSend:function(){
//            				$('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
            			},   
            			success:function(data)
            			{
//            				$('#uploaded_image').html(data);
            				console.log(data);
            				if(data!=0){
            					window.location.reload(true);
            				}
            			}
            		});
            	}
        	}
        });
	</script>
<?php } ?>

</body>

</html>