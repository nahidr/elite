<?php
if(is_array($jobs)){
    foreach ($jobs as $job){
        $type = $job['parent_job_type'];
        $job_type = "";
        if($type == 1){
            $job_type = "Day Care";
        }else if($type == 2){
            $job_type = "Night Care";
        }else if($type == 3){
            $job_type = "Full-Time";
        }else if($type == 4){
            $job_type = "Stay-In (Weekend Only)";
        }else{
            $job_type = $type;
        }
        $avail = $job['parent_availability'];
        $availability = "";
        if($avail == 1){
            $availability = "As Soon As Possible";
        }else if($avail == 2){
            $availability = "In Two Weeks";
        }else if($avail == 3){
            $availability = "From The Start Of Coming Month";
        }else{
            $availability = $avail;
        }
        $pos = $job['parent_position'];
        $position = "";
        if($pos == 1){
            $position = "Nanny";
        }else if($pos == 2){
            $position = "Personal Assistant";
        }else if($pos == 3){
            $position = "Governess/Tutor/Teacher";
        }else if($pos == 4){
            $position = "Maternity Nurse";
        }else if($pos == 5){
            $position = "Housekeeper";
        }else if($pos == 6){
            $position = "Chauffer";
        }else if($pos == 7){
            $position = "Butler";
        }else{
            $position = $pos;
        }
        $cou = $job['parent_country'];
        $country = "";
        if($cou == 1){
            $country = "England";
        }else if($cou == 2){
            $country = "Scotland";
        }else if($cou == 3){
            $country = "Ireland";
        }else if($cou == 4){
            $country = "Wales";
        }else if($cou == 5){
            $country = "Isle of Man";
        }else{
            $country = $cou;
        }
        ?>
           <div class="row jobs-row mt-2 mb-4">
            <div class="col-md-12 p-0">
                <h3>
                    <a href="" data-toggle="modal" data-target="#<?= $job['tag'] ?>"><?= $job['parent_job_title'] ?></a>
                    <a href="<?= base_url("shortlist/".$job['tag']) ?>"><span class="fa fa-heart" style="font-size: 18px;float: right;<?= ($job['shortlisted'] == 0)? "" : "color:red;" ?>"></span></a>
                </h3>
            </div>
            <div class="col-md-10 p-0">
                <div class="jobs">
                    <div class="title">
                        <div class="container">
                            <div class="row">

                            </div>
                        </div>
                    </div>
                    <div class="type">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="type-1">Job Type</label>
                                    <h5 class="type-2"><?= $job_type ?></h5>
                                </div>
                                <div class="col-md-3">
                                    <label class="type-1">Availability</label>
                                    <h5 class="type-2"><?= $availability ?></h5>
                                </div>
                                <div class="col-md-3">
                                    <label class="type-1">Experience</label>
                                    <h5 class="type-2"><?= $job['parent_experience'] ?> Years</h5>
                                </div>
                                <div class="col-md-3">
                                    <label class="type-1">Salary</label>
                                    <h5 class="type-2">&pound;<?= $job['parent_min_salary'] ?> - &pound;<?= $job['parent_max_salary'] ?></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="job-detail pb-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <p><?= $job['parent_job_description'] ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="<?= $job['tag'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
        
        
              <div class="modal-body">
        
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>        
              <div class="container">
                <div class="row pt-5 pb-5">
                    <div class="col-md-8 mx-auto">
                        <div class="row text-center job-specs">
                            <div class="col-md-12">
                                <h2><?= $job['parent_job_title'] ?></h2>
                                <label class="mb-0"><?= $job['parent_postcode'].", ".$country ?></label>
                                <?php
                                $earlier = new DateTime($job['created']);
                                $later = new DateTime();
                                $diff = $later->diff($earlier)->format("%a");
                                if($diff == 0){
                                    $time = "Today";
                                }else if($diff == 1){
                                    $time = "Yesterday";
                                }else{
                                    $time = $diff . " day(s) ago";
                                }
                                ?>
                                <p><label><?= $time ?>&emsp;<span class="fa fa-eye"> 23 Views</span></label></p>
                            </div>
                        </div>
                        <div class="row job-specs">
                            <div class="col-md-12">
                                <h3>Job Specifications</h3>
                                <ul>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4><b>Availability:</b></h4>
                                            </div>
                                            <div class="col-md-6">
                                                <h4><?= $availability ?></h4>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4><b>Job Type:</b></h4>
                                            </div>
                                            <div class="col-md-6">
                                                <h4><?= $job_type ?></h4>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4><b>Experience:</b></h4>
                                            </div>
                                            <div class="col-md-6">
                                                <h4><?= $job['parent_experience'] ?> Years</h4>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4><b>Position</b></h4>
                                            </div>
                                            <div class="col-md-6">
                                                <h4><?= $position ?></h4>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row job-specs mb-2">
                            <h3>Job Description</h3>
                            <ul>
                                <li>
                                 <p><?= $job['parent_job_description'] ?></p>   
                             </li>
                         </ul>
                     </div>
                     <div class="row job-specs mb-2">
                            <h3>Job Requirements</h3>
                            <ul>
                                <li>
                                 <p><?= $job['parent_job_requirements'] ?></p>   
                             </li>
                         </ul>
                     </div>
                     <div class="row job-specs mb-2">
                            <h3>Benefits</h3>
                            <ul>
                                <li>
                                 <p><?= $job['parent_benefits'] ?></p>   
                             </li>
                         </ul>
                     </div>
                     <div class="row mb-2">
                         <div class="col-md-12 text-center">
                             <a href="<?= base_url('show-interest/'.$job['tag']) ?>" class="site-btn">Show Interest</a>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
        </div>
        </div>
	</div>
</div>
        <?php
                }
            }
        ?>