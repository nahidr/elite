
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <script type="text/javascript">
        var step = "<?= isset($step)? $step : 1 ?>";
        </script>
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container p-3">
                    <div class="row">
                        <div class="col-md-11 mx-auto welcome-msg">
                            <p>Welcome Talent. We bring to you professional families seeking top talent. <br>
                                Needs to give clear guidance on how the process works. Apply direct. Simple 4 steps Submit.<br> One of our consultants will verify your profile and documents. If successful, you will receive an email requesting/how to pay the &pound;19.99 administration registration fee for your account to go live. </p>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid dashboard-content">
                        <div class="row">
                            <div class="col-md-12 hiring-box mx-auto">
                                <div id="msform">
                                    <!-- progressbar -->
                                    <ul id="progressbar">
                                        <li id="li1" class="active">Registration</li>
                                        <li id="li2" >Requirements</li>
                                        <li id="li3" >Upload Documents</li>
                                        <li id="li4" >Upload Video</li>
                                    </ul>
                                    
                                    <!-- fieldsets -->
                                    <fieldset id="f1">
                                        <div class="container" style='color:#242424'>
                                        	<input type="hidden" name="user_id_fk">
                                            <div class="row">
                                                <div class="col-md-12 personal-details text-center">
                                                    <h2>Personal Details</h2>
                                                </div>
                                                <div class="col-md-6 text-left pb-3">
                                                    <h3 class="m-0 mb-1">Full Name</h3>
                                                    <input type="text" name="fullname"/>
                                                </div>
                                                <div class="col-md-6 text-left pb-3">
                                                    <h3 class="m-0 mb-1">Phone Numer</h3>
                                                    <input type="text" name="phonenumber"/>
                                                </div>
                                                <div class="col-md-12 text-left pb-3">
                                                    <h3 class="m-0 mb-1">Address</h3>
                                                    <input type="text" name="address"/>
                                                </div>
                                                <div class="col-md-4 text-left pb-3">
                                                    <h3 class="m-0 mb-1">State/County</h3>
                                                    <input type="text" name="state"/>
                                                </div>

                                                <div class="col-md-4 text-left pb-3">
                                                    <h3 class="m-0 mb-1">Postcode</h3>
                                                    <input type="text" name="postcode"/>
                                                </div>
                                                <div class="col-md-4 text-left pb-3">
                                                    <h3 class="m-0 mb-1">Country</h3>
                                                    <select name="country">
                                                        <option>Uk</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6 text-left pb-3">
                                                    <h3 class="m-0 mb-1">Education (Degree)</h3>
                                                    <select name="education">
                                                        <option>Masters</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-6 text-left pb-3">
                                                    <h3 class="m-0 mb-1">Years of Experience</h3>
                                                    <select name="experience">
                                                        <option>Many</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-12 text-left pb-3">
                                                    <h3 class="m-0 mb-1">Tell us more about</h3>
                                                    <textarea name="about" rows="7"></textarea>
                                                </div>
                                                <div class="col-md-12 text-left pb-3">
                                                    <div class="row text-left">
                                                        <div class="col-md-12 pt-2 pb-2">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <h3 class="m-0 mb-1" style="margin-top: 2px !important;">Do you have a driving Licence?</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <input type="radio" class="rButton" name="opt1" value="1"/><span class="convText"> Yes</span>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="radio" class="rButton"  name="opt1" value="0"/><span class="convText"> No</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 pt-2 pb-2">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <h3 class="m-0 mb-1" style="margin-top: 2px !important;">Do you have a car?</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <input type="radio" class="rButton" name="opt2" value="1"/><span class="convText"> Yes</span>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="radio" class="rButton"  name="opt2" value="0"/><span class="convText"> No</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 pt-2 pb-2">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <h3 class="m-0 mb-1" style="margin-top: 2px !important;">Do you like pets?</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <input type="radio" class="rButton" name="opt3" value="1"/><span class="convText"> Yes</span>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="radio" class="rButton"  name="opt3" value="0"/><span class="convText"> No</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 pt-2 pb-2">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <h3 class="m-0 mb-1" style="margin-top: 2px !important;">Do you smoke?</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <input type="radio" class="rButton" name="opt4" value="1"/><span class="convText"> Yes</span>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="radio" class="rButton"  name="opt4" value="0"/><span class="convText"> No</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 pt-2 pb-2">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <h3 class="m-0 mb-1" style="margin-top: 2px !important;">Do you speak english?</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <input type="radio" class="rButton" name="opt5" value="1"/><span class="convText"> Yes</span>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="radio" class="rButton"  name="opt5" value="0"/><span class="convText"> No</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <select name="opt6">
                                                                        <option>Native</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row check-btn">
                                                <div class="col-md-12 personal-details text-center">
                                                    <h2>Availability</h2>
                                                </div>
                                                <div class="col-md-12 pt-3 pb-5 text-center">
                                                 <span><input type="checkbox" id="c1" name="availability[]" value="1"><label for="c1">Day Care</label></span>
                                                 <span><input type="checkbox" id="c2" name="availability[]" value="2"><label for="c2">Night Care</label></span>
                                                 <span><input type="checkbox" id="c3" name="availability[]" value="3"><label for="c3">Live-in</label></span>
                                             </div>
                                         </div>
                                         <div class="row pt-5 check-btn">
                                            <div class="col-md-12 personal-details text-center">
                                                <h2>Conditions Experience</h2>
                                            </div>
                                            <div class="col-md-12 pt-3 pb-5 text-center">
                                             <span><input type="checkbox" id="c4" name="conditions[]" value="1"><label for="c4">Anxiety</label></span>
                                             <span><input type="checkbox" id="c5" name="conditions[]" value="2"><label for="c5">Arthiris</label></span>
                                             <span><input type="checkbox" id="c6" name="conditions[]" value="3"><label for="c6">Alzheimer</label></span>
                                             <span><input type="checkbox" id="c7" name="conditions[]" value="4"><label for="c7">Confusion</label></span>
                                             <span><input type="checkbox" id="c8" name="conditions[]" value="5"><label for="c8">Dementia</label></span>
                                             <span><input type="checkbox" id="c9" name="conditions[]" value="6"><label for="c9">Depression</label></span>
                                             <span><input type="checkbox" id="c10" name="conditions[]" value="7"><label for="c10">Diabetes</label></span>
                                             <span><input type="checkbox" id="c11" name="conditions[]" value="8"><label for="c11">Incontinence</label></span>
                                             <span><input type="checkbox" id="c12" name="conditions[]" value="9"><label for="c12">Stroke</label></span>
                                         </div>
                                     </div>
                                     <div class="row pt-5 check-btn">
                                        <div class="col-md-12 personal-details text-center">
                                            <h2>Interests/Hobbies</h2>
                                        </div>
                                        <div class="col-md-12 pt-3 pb-5 text-center">
                                         <span><input type="checkbox" id="c13" name="interests[]" value="1"><label for="c13">Painting</label></span>
                                         <span><input type="checkbox" id="c14" name="interests[]" value="2"><label for="c14">Singing</label></span>
                                         <span><input type="checkbox" id="c15" name="interests[]" value="3"><label for="c15">Reading</label></span>
                                     </div>
                                 </div>
                             </div>
                             <input type="button" id="can-next1" name="next" class="mt-5 next action-button" value="Next"/>
                         </fieldset>
                         <fieldset id="f2">
                            <div class="container" style='color:#242424'>
                                <div class="row pb-5">
                                    <div class="col-md-12 personal-details text-center">
                                        <h2>Job Requirements</h2>
                                    </div>
                                    <div class="col-md-6 text-left pb-3">
                                        <h3 class="m-0 mb-1">Type of Contract</h3>
                                        <select name="contract">
                                            <option>1 Year</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 text-left pb-3">
                                        <h3 class="m-0 mb-1">Type of role</h3>
                                        <select name="role">
                                            <option>Fully Involved</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 text-left pb-3">
                                        <h3 class="m-0 mb-1">Preferred Country</h3>
                                        <select name="preferred_country">
                                            <option>Uk</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 text-left pb-3">
                                        <h3 class="m-0 mb-1">Preferred Language</h3>
                                        <select name="preferred_language">
                                            <option>English</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4 text-left pb-3">
                                        <h3 class="m-0 mb-1">Travel with family</h3>
                                        <select name="travel_with_family">
                                            <option>Yes</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 text-left pb-3">
                                        <h3 class="m-0 mb-1">Driver</h3>
                                        <select name="driver">
                                            <option>Yes</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 text-left pb-3">
                                        <h3 class="m-0 mb-1">Swimmer</h3>
                                        <select name="swimmer">
                                            <option>Yes</option>
                                        </select>
                                    </div>

                                    <div class="col-md-6 text-left pb-3">
                                        <h3 class="m-0 mb-1">Available from</h3>
                                        <input type="date" name="available_from" placeholder="">
                                    </div>
                                </div>
                            </div>
<!--                             <input type="button" name="previous" class="previous action-button-previous" value="Previous"/> -->
                            <input type="button" name="next" id="can-next2" class="next action-button" value="Next"/>
                        </fieldset>
                        <fieldset style="margin: 0;width: 100%;" id="f3">
                            <div class="container" style='color:#242424'>
                                <div class="row pb-5">
                                    <div class="col-md-12 personal-details text-center">
                                        <h2>Upload your Documents</h2>
                                    </div>
                                    <div class="col-md-6 pt-5 uploadfile text-left">
                                        <h3 class="m-0 mb-1"><b>CV</b></h3>
                                        <div class="form text-center">
                                            <input type="file" multiple="" id="file_cv" name="file_cv">
                                            <div class="img-txt">
                                                <img src="<?= base_url() ?>assets/images/icons/icons8-upload-64.png">
                                                <p>Drop a file here or &emsp;<a href="#" class="site-btn" style='letter-spacing:0px;color:white'>Select File</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pt-5 uploadfile text-left">
                                        <h3 class="m-0 mb-1"><b>ID</b></h3>
                                        <div class="form text-center">
                                            <input type="file" multiple="" id="file_id" name="file_id">
                                            <div class="img-txt">
                                                <img src="<?= base_url() ?>assets/images/icons/icons8-upload-64.png">
                                                <p>Drop a file here or &emsp;<a href="#" class="site-btn" style='letter-spacing:0px;color:white'>Select File</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pt-5 uploadfile text-left">
                                        <h3 class="m-0 mb-1"><b>Qualification/Training</b></h3>
                                        <div class="form text-center">
                                            <input type="file" multiple="" id="file_training" name="file_training">
                                            <div class="img-txt">
                                                <img src="<?= base_url() ?>assets/images/icons/icons8-upload-64.png">
                                                <p>Drop a file here or &emsp;<a href="#" class="site-btn" style='letter-spacing:0px;color:white'>Select File</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pt-5 uploadfile text-left">
                                        <h3 class="m-0 mb-1"><b>Paediatric first aid</b></h3>
                                        <div class="form text-center">
                                            <input type="file" multiple="" id="file_first_aid" name="file_first_aid">
                                            <div class="img-txt">
                                                <img src="<?= base_url() ?>assets/images/icons/icons8-upload-64.png">
                                                <p>Drop a file here or &emsp;<a href="#" class="site-btn" style='letter-spacing:0px;color:white'>Select File</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 pt-5 uploadfile text-left">
                                        <h3 class="m-0 mb-1"><b>Reference</b></h3>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="container"  style="border: 1px dashed #3d405c;">
                                                    <div class="row pt-2">
                                                        <div class="col-md-3 pt-2">
                                                            <h3 class="m-0 mb-1">Name</h3>
                                                        </div>
                                                        <div class="col-md-9 ">
                                                            <input type="text" name="ref1_name"/>
                                                        </div>
                                                        <div class="col-md-3 pt-2">
                                                            <h3 class="m-0 mb-1">Title</h3>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" name="ref1_title"/>
                                                        </div>
                                                        <div class="col-md-3 pt-2">
                                                            <h3 class="m-0 mb-1">Company</h3>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" name="ref1_company"/>
                                                        </div>
                                                        <div class="col-md-3 pt-2">
                                                            <h3 class="m-0 mb-1">Phone</h3>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" name="ref1_phone"/>
                                                        </div>
                                                        <div class="col-md-3 pt-2">
                                                            <h3 class="m-0 mb-1">Email</h3>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" name="ref1_email"/>
                                                        </div>
<!--                                                         <div class="col-md-12 pt-3 pb-3 text-center">
                                                            <a href="#" class="site-btn" style='letter-spacing:0px;color:white'>Save</a>
                                                         	</div> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="container"  style="border: 1px dashed #3d405c;">
                                                    <div class="row pt-2">
                                                        <div class="col-md-3 pt-2">
                                                            <h3 class="m-0 mb-1">Name</h3>
                                                        </div>
                                                        <div class="col-md-9 ">
                                                            <input type="text" name="ref2_name"/>
                                                        </div>
                                                        <div class="col-md-3 pt-2">
                                                            <h3 class="m-0 mb-1">Title</h3>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" name="ref2_title"/>
                                                        </div>
                                                        <div class="col-md-3 pt-2">
                                                            <h3 class="m-0 mb-1">Company</h3>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" name="ref2_company"/>
                                                        </div>
                                                        <div class="col-md-3 pt-2">
                                                            <h3 class="m-0 mb-1">Phone</h3>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" name="ref2_phone"/>
                                                        </div>
                                                        <div class="col-md-3 pt-2">
                                                            <h3 class="m-0 mb-1">Email</h3>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" name="ref2_email"/>
                                                        </div>
<!--                                                         <div class="col-md-12 pt-3 pb-3 text-center">
                                                            <a href="#" class="site-btn" style='letter-spacing:0px;color:white'>Save</a>
           		                                             </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
<!--                             <input type="button" name="previous" class="previous action-button-previous" value="Previous"/> -->
                            <input type="button" name="next" id="can-next3" class="next action-button" value="Next"/>
                        </fieldset>
                        <fieldset id="f4">
                        	<?php echo form_open_multipart('save-video'); ?>
                            <div class="container" style='color:#242424'>
                                <div class="row pb-5">
                                    <div class="col-md-12 personal-details text-center">
                                        <h2>Upload your Introduction Video</h2>
                                    </div>
                                    <div class="col-md-12 pt-5 mx-auto uploadfile">
                                        <div class="form">
                                            <input type="file" multiple="" name="file_video">
                                            <div class="img-txt">
                                                <img src="<?= base_url() ?>assets/images/icons/icons8-documentary-100.png">
                                                <p>Drop Video File</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
<!--                             <input type="button" name="previous" class="previous action-button-previous" value="Previous"/> -->
<!--                             <a href="dashboard.html" class="next action-button" >Submit</a> -->
							<button type="submit" class="action-button">Submit</button>
							<?php echo form_close(); ?>
                        </fieldset>
                    </div>
                    <!-- link to designify.me code snippets -->
                    <!-- /.link to designify.me code snippets -->
                </div>
            </div>
            <!-- /.MultiStep Form -->
        </div>

        