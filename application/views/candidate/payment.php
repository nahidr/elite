    <!doctype html>
    <html lang="en">

    <head>
        <!-- Required meta tags -->
        <title>Elite Private Staff</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="../assets/assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="../assets/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="../assets/assets/libs/css/style.css">
        <link rel="stylesheet" href="../assets/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <link rel="stylesheet" href="../assets/assets/vendor/charts/chartist-bundle/chartist.css">
        <link rel="stylesheet" href="../assets/assets/vendor/charts/morris-bundle/morris.css">
        <link rel="stylesheet" href="../assets/assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="../assets/assets/vendor/charts/c3charts/c3.css">
        <link rel="stylesheet" href="../assets/assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
        input[type=text], select{
            width: 100%;
            padding: 7px;
            border: 1px solid #ccc;
            border-radius: 0px;
            box-sizing: border-box;
            margin-bottom: 16px;
            resize: vertical;
        }
        textarea {
            outline: 0;
            width: 100%;
            padding: 7px;
            border: 1px solid #ccc;
            border-radius: 20px;
            box-sizing: border-box;
            margin-bottom: 16px;
            resize: none;
        }
        .site-btn {
            padding: 12px 20px !important;
            min-width: 100px;
            color: #fff !important;
        }
        .pull-right{
            float: right !important;
        }
        .deactivate-btn{
            font-family: Segoe UI;
            position: relative;
            display: inline-block;
            padding: 12px 0px;
            font-size: 14px;
            font-weight: 600;
            line-height: 16px;
            letter-spacing: 2px;
            border-radius: 20px;
            min-width: 200px;
            text-align: center;
            background: #5c0100;
            cursor: pointer;
            border: none;
            color: #fff;
        }
        input.radio {
            padding: 8px;
            border: 1px solid #ccc;
            border-radius: 0px;
            margin-bottom: 10px;
            width: 20px;
            display: inline-block;
            box-sizing: border-box;
            color: #2C3E50;
            font-size: 15px;
        }
        h3.total{
            color: #666;
            font-weight: 600;
        }
        h3.price{
            font-weight: 600;
            color: #e5b4a5;
        }
        .border-bottom {
            border-bottom: 2px solid #e6e6f2 !important;
        }

        @font-face { font-family: Sweet Easy; src: url('../assets/assets/fonts/Sweet Easy personal Use.ttf');}
        @font-face { font-family: Segoe UI;  src: url('../assets/assets/fonts/segoeui.ttf');}
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg fixed-top">
                <a class="navbar-brand" id="marg-0-auto" href="dashboard.html"><img src="../assets/images/logo.png" width="250px"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav nav-menu">
                        <li class="nav-link">
                            <a href="">Home</a>
                        </li>
                        <li class="nav-link">
                            <a href="">Contact us</a>
                        </li>
                        <li class="nav-link">
                            <a href="">About us</a> 
                        </li>
                        <li class="nav-link">
                            <a href="">GDPR</a> 
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/assets/images/profile.png" alt="" class="user-avatar-md rounded-circle"><span class="fa fa-caret-down pl-1 fa-2x"></span></a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2" style="top: 90%; right: 10%; background: #f3e2dd; border-top-right-radius: 13px; border-bottom-left-radius: 13px;">
                                <a class="dropdown-item" href="#">Edit Profile</a>
                                <hr class="m-0">
                                <a class="dropdown-item" href="#">Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                <div class="container-fluid">
                                    <div class="row text-center pt-2 pb-2">
                                        <div class="col-md-12 sidebar-profile">
                                            <span>Welcome to your account</span>
                                            <h4>Sharan Gill</h4>
                                            <img src="../assets/images/profile.png">
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="dashboard.html" aria-expanded="false"><i class="fa fa-fw fa-home"></i>Dashboard</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="viewjobs.html" aria-expanded="false"><i class="fa fa-fw fa-search"></i>Browse Jobs</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="shorlisted.html" aria-expanded="false"><i class="fa fa-fw fa-heart"></i>Shorlisted Jobs</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="inbox.html" aria-expanded="false"><i class="fa fa-fw fa-envelope"></i>Inbox</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="#" aria-expanded="false"><i class="fa fa-fw fa-cog"></i>Settings</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-md-12 mt-3">
                            <div class="page-header pl-3 pt-3">
                                <!-- <h2 class="pageheader-title">Dashboard</h2> -->
                            </div>
                        </div>
                        <div class="col-lg-8 mx-auto p-2">
                            <div class="hiring-box pt-3 pb-3">
                                <div class="container">
                                  <div class="row">
                                      <div class="col-md-8 mx-auto mt-4 border-bottom">
                                          <div class="row">
                                              <div class="col-md-6 text-center">
                                                  <h3 class="total">TOTAL</h3>
                                              </div>
                                              <div class="col-md-6 text-center">
                                                  <h3 class="price">$ 300.00</h3>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="row pt-3 pb-3">
                                      <div class="col-md-6 mx-auto">
                                          <div class="row">
                                              <div class="col-md-6 text-center">
                                                  <label><input type="radio" id="radio1" name="type" class="radio" value="Fresher"> Bank Card</label>
                                              </div>
                                              <div class="col-md-6 text-center">
                                                  <label><input type="radio" name="type" class="radio" value="Experienced"> PayPal</label>
                                              </div>
                                          </div>
                                      </div>
                                  </div>


                                  <div id="textboxes" style="display: none">

                                  </div>
                                  <div id="textboxes1" style="display: none">
                                    <div class="row pt-3">
                                        <div class="col-md-10 mx-auto text-center">
                                            <div class="row">
                                                <div class="col-md-6 text-left pb-3">
                                                    <h3 class="m-0 mb-1">Name of Cardholder</h3>
                                                    <input type="text" name="fname"/>
                                                </div>
                                                <div class="col-md-6 text-left pb-3">
                                                    <h3 class="m-0 mb-1">Card Number</h3>
                                                    <input type="text" name="fname"/>
                                                </div>
                                                <div class="col-md-6 text-left pb-3">
                                                    <h3 class="m-0 mb-1">Expiry Date</h3>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input type="text" name="fname"/>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" name="fname"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-left pb-3">
                                                    <h3 class="m-0 mb-1">CVV</h3>
                                                    <input type="text" name="fname"/>
                                                </div>
                                                <div class="col-md-12 pt-5 pb-5">
                                                    <a href="#" class="site-btn">Process Payment</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="mx-auto col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    © Elite Private Staff 2018. All rights reserved.
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper  -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->
<!-- jquery 3.3.1 -->
<script src="../assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
<!-- bootstap bundle js -->
<script src="../assets/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
<!-- slimscroll js -->
<script src="../assets/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
<!-- main js -->
<script src="../assets/assets/libs/js/main-js.js"></script>
<!-- chart chartist js -->
<script src="../assets/assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
<!-- sparkline js -->
<script src="../assets/assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
<!-- morris js -->
<script src="../assets/assets/vendor/charts/morris-bundle/raphael.min.js"></script>
<script src="../assets/assets/vendor/charts/morris-bundle/morris.js"></script>
<!-- chart c3 js -->
<script src="../assets/assets/vendor/charts/c3charts/c3.min.js"></script>
<script src="../assets/assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
<script src="../assets/assets/vendor/charts/c3charts/C3chartjs.js"></script>
<script src="../assets/assets/libs/js/dashboard-ecommerce.js"></script>
<script>
    $(function() {
        $('input[name="type"]').on('click', function() {
            if ($(this).val() == 'Experienced') {
                $('#textboxes').show();
                $('#textboxes1').hide();
            }
            else if($(this).val() == 'Fresher'){
                $('#textboxes1').show();
                $('#textboxes').hide();
            }
            else {
                $('#textboxes').hide();
                $('#textboxes1').hide();
            }
        });
    });
</script>
</body>

</html>