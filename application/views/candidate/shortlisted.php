<style>
    .form-control {
        height: 45px;
    }
    .Search input[type=text],.Search select,.Search textarea {
        height: 45px !important;
        border-radius: 0px;
        border: 1px solid #ced4da;
        width: 100%;
    }
    .Search :focus{
        background-color: white;
    }
    .modal-body {
      position:relative;
      padding:0px;
  }
  .close {
      position:absolute;
      right:-30px;
      top:0;
      z-index:999;
      font-size:2rem;
      font-weight: normal;
      color:#fff;
      opacity:1;
  }
  .modal-dialog {
      max-width: 800px;
      margin: 50px auto;
  }
  .job-specs h2, .job-specs h3{
    color: #5c0100;
    font-weight: 700;
  }
  .job-specs ul li{
    list-style: none;
    }
</style>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="container">
                        <?php
                        $this->load->view("candidate/jobs");
                        ?>
                        <div class="row mt-5 mb-5">
                            <div class="col-md-12 pt-5 pb-5 text-center">
                                <a href="#" class="site-btn">Load More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>