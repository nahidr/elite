<style>
    .form-control {
        height: 45px;
    }
    .Search input[type=text],.Search select,.Search textarea {
        height: 45px !important;
        border-radius: 0px;
        border: 1px solid #ced4da;
        width: 100%;
    }
    .Search :focus{
        background-color: white;
    }
    .modal-body {
      position:relative;
      padding:0px;
  }
  .close {
      position:absolute;
      right:-30px;
      top:0;
      z-index:999;
      font-size:2rem;
      font-weight: normal;
      color:#fff;
      opacity:1;
  }
  .modal-dialog {
      max-width: 800px;
      margin: 50px auto;
  }
  .job-specs h2, .job-specs h3{
    color: #5c0100;
    font-weight: 700;
  }
  .job-specs ul li{
    list-style: none;
    }
</style>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-header pl-2 pt-3">
                                <h2 class="pageheader-title">Jobs Feeds</h2>
                            </div>
                        </div>
                    </div>
                    <?php
                    $keyword = "";
                    $category = "";
                    if(array_key_exists("key", $this->session->userdata())){
                        $old = $this->session->userdata("key");
                        $keyword = $old['keyword'];
                        $category = $old['category'];
                    }
                    ?>
                    <div class="container">
                        <div class="row Search mb-5">
                         <div class="col-md-10 text-center mx-auto">
                         <?php echo form_open('search-jobs'); ?>
                           <div class="row Search">
                             <div class="col-lg-7 p-0">
                             	<input type="text" class="form-control input-sm" placeholder="Keyword" name="keyword" value="<?= $keyword ?>">
                           </div>
                           <div class="col-lg-3 p-0">
                               	<select class="" id="job-cat" name="category"><!-- .form-contorl -->
                                   <option value="" <?= ($category == "")? "selected" : "" ?>>Any</option>
                                   <option value="1" <?= ($category == "1")? "selected" : "" ?>>Nanny</option>
                                   <option value="2" <?= ($category == "2")? "selected" : "" ?>>Personal Assistant</option>
                                   <option value="3" <?= ($category == "3")? "selected" : "" ?>>Governess/Tutor/Teacher</option>
                                   <option value="4" <?= ($category == "4")? "selected" : "" ?>>Maternity Nurse</option>
                                   <option value="5" <?= ($category == "5")? "selected" : "" ?>>Housekeeper</option>
                                   <option value="6" <?= ($category == "6")? "selected" : "" ?>>Chauffer</option>
                                   <option value="7" <?= ($category == "7")? "selected" : "" ?>>Butler</option>
                                   <option value="" <?= ($category == "")? "selected" : "" ?>>Any</option>
                             	</select>
                         </div>
                         <div class="col-lg-2 p-0">
                           <button style="padding: 10px 60px;border:1px solid #e0bb75; background-color: #5C0100;border-radius: 0px;"><span class="fa fa-search" style="color:white; font-size: 22px;"></span></button><!-- #fe8d91|#5C0100 -->
                       </div>
                   </div>
                   <?php echo form_close(); ?>
               </div>
           </div>
           <?php
            $this->load->view("candidate/jobs");
            ?>
        <div class="row mt-5 mb-5">
            <div class="col-md-12 pt-5 pb-5 text-center">
                <a href="#" class="site-btn">Load More</a>
            </div>
        </div>
    </div>
</div>
</div>