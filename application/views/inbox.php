
    <!-- ============================================================== -->
    <!-- wrapper  -->
    <!-- ============================================================== -->
    <div class="dashboard-wrapper">
      <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content">
          <!-- ============================================================== -->
          <!-- pageheader  -->
          <!-- ============================================================== -->
          <div class="row">
            <div class="col-md-6">
              <div class="page-header pl-3 pt-3 pb-3">
                <h2 class="pageheader-title">Chat</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5 text-left">
              <div class="srch_bar">
                <div class="stylish-input-group">
                  <input type="text" class="search-bar"  placeholder="Search" >
                  <span class="input-group-addon">
                    <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                  </span> </div>
                </div>
              </div>
              <div class="col-md-7 text-left pl-3 chat-head">
                <h3>Lilly Howell <span class="fa fa-circle"></span></h3>
                <hr>
              </div>
            </div>
            <div class="messaging">
              <div class="inbox_msg">
                <div class="container">
                  <div class="row">
                    <div class="col-md-5 col-lg-5">
                      <div class="inbox_people">
                        <div class="inbox_chat">
                        
                        
                        <?php if(!empty($messages)){
                          foreach($messages as $msg):?>
                          <div class="chat_list <?=($msg['is_read']=='0')? 'active_chat' : ''?> ">
                            <div class="chat_people">
                              <div class="chat_img"> <img src="<?= base_url() ?>assets/assets/images/mail6.png" alt="sunil"> </div>
                              <div class="chat_ib">
                                <h5><?=$msg['user_first_name']?> <?=$msg['user_last_name']?></h5>
                                <p><?=$msg['message_text']?></p>
                              </div>
                            </div>
                          </div> 

                          <?php endforeach; }?>
                          
                        </div>
                      </div>
                    </div>
                    <div class="col-md-7 col-lg-7">
                      <div class="mesgs">
                        <div class="msg_history">

                          <div class="outgoing_msg">
                            <div class="incoming_msg_img" style="float: right;"> <img src="<?= base_url() ?>assets/assets/images/search-profile.png" style="border-radius: 50%;" alt="sunil"> </div>
                            <div class="sent_msg text-right">
                              <p>Hi, how are you today?</p>
                              <span class="time_date"> 11:01 AM    |    June 9</span> </div>
                            </div>

                                <div class="incoming_msg">
                                  <div class="incoming_msg_img"> <img src="<?= base_url() ?>assets/assets/images/search-profile.png" style="border-radius: 50%;" alt="sunil"> </div>
                                  <div class="received_msg">
                                    <div class="received_withd_msg">
                                      <p>I'm good. Thank you</p>
                                      <span class="time_date"> 11:01 AM    |    Yesterday</span></div>
                                    </div>
                                  </div>
                                </div>

                                <div class="type_msg">
                                  <div class="input_msg_write">
                                    <input type="text" class="write_msg" placeholder="Type a message" />
                                    <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
