	<footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 text-center">
            <div class="pt-5 pb-3">
              <img src="<?= base_url() ?>assets/images//logo.png" class="pb-2 image-responsive">
            </div>
          </div>
          <div class="col-lg-6">
            <div class="row">
              <div class="col-6 col-md-6 col-lg-6 mb-5 mb-lg-0">
                <h3 class="footer-heading mb-4">
                  <span style="display: inline-block;border-bottom: 1px solid #2f2f2f;padding-bottom: 2px;"><b>COMPANY</b></span></h3>
                  <ul class="list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Who we are</a></li>
                    <li><a href="#">Work with us</a></li>
                    <li><a href="#">Find a nanny</a></li>
                  </ul>
                </div>
                <div class="col-6 col-md-6 col-lg-6 mb-5 mb-lg-0">
                  <h3 class="footer-heading mb-4">
                    <span style="display: inline-block;border-bottom: 1px solid #2f2f2f;padding-bottom: 2px;"><b>CONTACT US</b></span></h3>
                    <ul class="list-unstyled">
                      <li><a href="#"><span class="fa fa-phone" style="color: #5c0100"></span>&emsp;02054876284</a></li>
                      <li><a href="#"><span class="fa fa-envelope" style="color: #5c0100"></span>&emsp;info@eliteprivatestaff.co.uk</a></li>
                      <li><a href="#"><span class="fa fa-map-marker" style="color: #5c0100"></span>&emsp;45, Oxford St xJJ4DOl, UK</a></li>
                    </ul>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <hr style="border-color: #666666;">
          <div class="container">
            <div class="row text-center">
              <div class="col-md-12">
                <div class="text-left">
                  <p style="color: #666666;"><b>
                  &copy; Elive Private Staff 2019. All rights reserved 2018.</b>
                  <label class="pull-right text-center bottom">
                    <a href="#" class="pr-3">Terms & Conditions</a>
                    <a href="#" class="pr-3">Privacy Policy</a>
                  </label>
                </p>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>

    <script src="<?= base_url() ?>assets/js/jquery-3.3.1.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery-migrate-3.0.1.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery-ui.js"></script>
    <script src="<?= base_url() ?>assets/js/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/js/owl.carousel.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery.stellar.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery.countdown.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script src="<?= base_url() ?>assets/js/aos.js"></script>

    <script src="<?= base_url() ?>assets/js/main.js"></script>

  </body>
  </html>