
    <div class="about-section">
      <div class="container pt-5 pb-5">
        <div class="row pb-5">
          <div class="col-md-10 mx-auto">
            <h2 class="text-center m-0 aos-init aos-animate pt-2 pb-5" style="color: #5c0100; font-family: Sweet Easy;font-size: 24px;line-height: 55px;letter-spacing: 2px;" data-aos="fade"><span><b>How We Do Business</b></span></h2>
            <p class="text-left">It is a long established fact that a reader will be distracted by the readable content of a page when 
              looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many 
            desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their inf</p>
          </div>
        </div>
        <div class="row pt-5">
          <div class="col-md-10 mx-auto">
            <h2 class="text-center" style="color: #000;font-size: 24px;font-family: Segoe UI;letter-spacing: 2px;" data-aos="fade"><span><b>Some of the vales we live by as a company</b></span></h2>
            <div class="row mb-5">
              <div class="col-md-12">
                <div class="row text-center pt-5 pb-5">
                  <div class="col-md-3 top-75">
                    <img src="assets/images/trans.png">
                    <h5 class="pt-2 pb-2">Transparency</h5>
                  </div>
                  <div class="col-md-3">
                    <img src="assets/images/accountibility.png">
                    <h5 class="pt-2 pb-2">Accountibility</h5>
                  </div>

                  <div class="col-md-3 top-75">
                    <img src="assets/images/quality.png">
                    <h5 class="pt-2 pb-2">Quality</h5>
                  </div>
                  <div class="col-md-3">
                    <img src="assets/images/simplicity.png">
                    <h5 class="pt-2 pb-2">Simplicity</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="team-section">
      <div class="container pt-5 pb-5">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="m-0 aos-init aos-animate" style="color: #5c0100; font-family: Sweet Easy;font-size: 24px;line-height: 55px;letter-spacing: 2px;" data-aos="fade"><span><b>Meet Our Team</b></span></h2>
          </div>
          <div class="col-md-4 pt-5 text-center">
            <img src="assets/images/team1.png" class="team-image-responsive">
            <div class="pt-5 pb-3">
              <h5>EARL STONE</h5>
              <label>MARKETER</label>
            </div>
          </div>
          <div class="col-md-4 pt-5 text-center">
            <img src="assets/images/team2.png" class="team-image-responsive">
            <div class="pt-5 pb-3">
              <h5>NORA READ</h5>
              <label>MARKETER</label>
            </div>
          </div>
          <div class="col-md-4 pt-5 text-center">
            <img src="assets/images/team3.png" class="team-image-responsive">
            <div class="pt-5 pb-3">
              <h5>JESSICA BROWN</h5>
              <label>MARKETER</label>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="site-section bg-light">
      <div class="container">
        <div class="col-12 mb-3 text-center">
          <h2 class="m-0 aos-init aos-animate" style="color: #5c0100; font-family: Sweet Easy;font-size: 24px;line-height: 55px;letter-spacing: 2px;" data-aos="fade"><span><b>Customers Say</b></span></h2>
        </div>
        <div class="block-13 nonloop-block-13 owl-carousel" data-aos="fade">

          <div class="p-4">
            <div class="block-47 d-flex">

              <blockquote class="block-47-quote">
                <p style="color:  #000;font-family: 'brandon grotesque';font-style: normal;font-size: 16px;font-weight: bold;">&ldquo;Magnam iure fugit recusandae nobis a amet, officiis laboriosam repudiandae? Quis nostrum numquam ducimus quo ab laboriosam qui expedita, cupiditate ex, sed dignissimos facere provident dolores, eius distinctio quas aliquid.&rdquo;</p>
                <cite class="block-47-quote-author">&mdash; John doe, CEO <a href="#" style="color: #322e75">XYZ Inc.</a></cite>
              </blockquote>
            </div>
          </div>

          <div class="p-4">
            <div class="block-47 d-flex">

              <blockquote class="block-47-quote">
                <p style="color:  #000;font-family: 'brandon grotesque';font-style: normal;font-size: 16px;font-weight: bold;">&ldquo;Magnam iure fugit recusandae nobis a amet, officiis laboriosam repudiandae? Quis nostrum numquam ducimus quo ab laboriosam qui expedita, cupiditate ex, sed dignissimos facere provident dolores, eius distinctio quas aliquid.&rdquo;</p>
                <cite class="block-47-quote-author">&mdash; John doe, CEO <a href="#" style="color: #322e75">XYZ Inc.</a></cite>
              </blockquote>
            </div>
          </div>

          <div class="p-4">
            <div class="block-47 d-flex">

              <blockquote class="block-47-quote">
                <p style="color:  #000;font-family: 'brandon grotesque';font-style: normal;font-size: 16px;font-weight: bold;">&ldquo;Magnam iure fugit recusandae nobis a amet, officiis laboriosam repudiandae? Quis nostrum numquam ducimus quo ab laboriosam qui expedita, cupiditate ex, sed dignissimos facere provident dolores, eius distinctio quas aliquid.&rdquo;</p>
                <cite class="block-47-quote-author">&mdash; John doe, CEO <a href="#" style="color: #322e75">XYZ Inc.</a></cite>
              </blockquote>
            </div>
          </div>

          <div class="p-4">
            <div class="block-47 d-flex">

              <blockquote class="block-47-quote">
                <p style="color:  #000;font-family: 'brandon grotesque';font-style: normal;font-size: 16px;font-weight: bold;">&ldquo;Magnam iure fugit recusandae nobis a amet, officiis laboriosam repudiandae? Quis nostrum numquam ducimus quo ab laboriosam qui expedita, cupiditate ex, sed dignissimos facere provident dolores, eius distinctio quas aliquid.&rdquo;</p>
                <cite class="block-47-quote-author">&mdash; John doe, CEO <a href="#" style="color: #322e75">XYZ Inc.</a></cite>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </div>
    