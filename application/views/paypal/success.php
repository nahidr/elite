<?php
$this->load->view('header');
?>
<section class="thanks categories-page">
		<div class="container">
			<div class="row text-center">
				<div class="col-md-6 mx-auto">
					<div class="thanks">
						<h2>Thankyou!</h2>
						<label style="padding: 20px;">Your Transaction was Successful. Details:</label>
                        <p>Item Number : <span><?php echo $item_number; ?></span></p>
                        <p>TXN ID : <span><?php echo $txn_id; ?></span></p>
                        <p>Amount Paid : <span>&pound <?php echo $payment_amt.' '.$currency_code; ?></span></p>
                        <p>Payment Status : <span><?php echo $status; ?></span></p>
                        <?php 
                        if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 1) {
                            $go = "candidate-dash";
                        }else if (isset($this->session->userdata['user']['user_type']) && $this->session->userdata['user']['user_type'] == 2) {
                            $go = "parent-dash";
                        }else if(!isset($this->session->userdata['user']['user_type'])){
                            redirect(base_url("logout"));
                        }
                        ?>
						<button class="site-btn" style="margin-bottom: 50px;"><a href="<?php echo base_url($go); ?>" style="">Back To Dashboard</a></button>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- page end -->
<?php
$this->load->view('footer');
?>