

    <div class="team-section">
      <div class="container pt-5">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="m-0 aos-init aos-animate" style="color: #5c0100; font-family: Sweet Easy;font-size: 30px;line-height: 55px;letter-spacing: 2px;" data-aos="fade"><span><b>Employers Toolkit</b></span></h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-5 text-center">
            <div>
              <h1 class="numer-leftside">1</h1>
              <img src="assets/images/laptop.png" class="image-responsive">
            </div>
          </div>
          <div class="col-md-7 pt-5">
            <h3 class="laptop-text-h pt-5">Job offer template</h3>
            <p class="laptop-text pt-3 pb-5">Take a few minutes to tell us about what you’re looking for, and let us match 
            you up with the best candidates for you.</p>
            <a href="#" class="site-btn mt-1">Find your nanny today</a>
          </div>
        </div>
      </div>
    </div>
    <div class="about-section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h1 class="numer-leftside" style="color: #fff;">2</h1>
            <h3 class="laptop-text-h text-center pt-5">Pre-employment checklist</h3>
            <ul class="laptop-text-ul pt-3">
              <li>
                criminal records check including spent convictions
              </li>
              <li>
                qualifications - CV
              </li>
              <li>
                driving and car status
              </li>
              <li>
                qualifications - CV
              </li>
              <li>
                driving and car status
              </li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="col-md-8">
           <h1 class="numer-rightside" style="color: #fff;">3</h1>
            <h3 class="laptop-text-h text-center pt-5">Employment Contract</h3>
            <p class="laptop-text pt-3 pb-5">By accepting our terms and conditions you agree to the terms of use of the
             Elite Private Staff nline platform which includes agreeing that you will contract
             directly with each client as a self-employed carer. You may wish to enter into
           a written self-employed contract with your customer, but that is at your discretion.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="team-section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h1 class="numer-leftside">4</h1>
            <h3 class="laptop-text-h text-center pt-5">Payroll - HMRC checklist</h3>
            <ul class="laptop-text-ul pt-3">
              <li>
                criminal records check including spent convictions
              </li>
              <li>
                qualifications - CV
              </li>
              <li>
                driving and car status
              </li>
              <li>
                qualifications - CV
              </li>
              <li>
                driving and car status
              </li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="col-md-8">
            <h1 class="numer-rightside">5</h1>
            <h3 class="laptop-text-h text-center pt-5">Disciplinary and grievances</h3>
            <p class="laptop-text pt-3 pb-5">By accepting our terms and conditions you agree to the terms of use of the
             Elite Private Staff nline platform which includes agreeing that you will contract
             directly with each client as a self-employed carer. You may wish to enter into
           a written self-employed contract with your customer, but that is at your discretion.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8">
            <h1 class="numer-leftside">6</h1>
            <h3 class="laptop-text-h text-center pt-5">Performance Management</h3>
            <p class="laptop-text pt-3 pb-5">By accepting our terms and conditions you agree to the terms of use of the
             Elite Private Staff nline platform which includes agreeing that you will contract
             directly with each client as a self-employed carer. You may wish to enter into
           a written self-employed contract with your customer, but that is at your discretion.</p>
          </div>
          <div class="col-md-4"></div>
        </div>
      </div>
    </div>
    