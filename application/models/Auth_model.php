<?php

Class Auth_model extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        ob_clean();
    }
    
    // Read data using username and password
    public function checkUser($email, $password) {
         $this->db->select('*');
        $this->db->from('tbl_user'); 
        $this->db->where("user_email",$email);
        $this->db->where("user_password",md5($password));
        $this->db->limit(1);
        $query = $this->db->get();
        
        if ($query->num_rows() == 1) {
            $data = $query->row(); 
            if($data->user_status == 0){
                $results = array(  "login" => "2"  );
            }else{
                $results = array(
                    "login" => "1",
                    "user_id" => $data->user_id,
                    "user_first_name" => $data->user_first_name,
                    "user_last_name" => $data->user_last_name,
                    "user_name" => $data->user_name,
                    "user_email" => $data->user_email,
                    "user_password" => $data->user_password,
                    "user_type" => $data->user_type,
                    "user_status" => $data->user_status,
                    'user_picture' => $data->user_picture
                );
            }
        } else {
            $results = array(
                "login" => "0"
            );
        }
        return $results;
    }
     
    public function get_user_data($id) { 
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->where("user_id",$id);
        $this->db->limit(1);
        $query = $this->db->get();
        
        if ($query->num_rows() == 1) {
            $res = $query->result_array();
            return $res[0];
        } else {
            return false;
        }
    }

    
    public function update_user($data) {
        $this->db->where('user_id',$data['user_id']);
        $result = $this->db->update('tbl_user',$data);
        return $result;
    }
    
    public function change_email($pin){
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->where("temp_pin",$pin);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
            $result = $res[0];
            $result['user_email'] = $result['temp_email'];
            $result['temp_email'] = "";
            $result['temp_pin'] = "";
            $data = $result;
            $result = $this->update_user($data);
            return $result;
        }else{
            return false;
        }
    }
    
    public function delete_user($user_id="") {
        if($user_id == ""){
            $user_id = $this->session->userdata['user']['user_id'];
            $this->db->where('user_id',$user_id);
            $result = $this->db->delete('tbl_user');
        }else{
            $data = array(
                'user_status' => '0'
            );
            $this->db->where('user_id',$user_id);
            $result = $this->db->update('tbl_user',$data);
        } 
        return $result;
    }  

    public function generatePIN($digits = 5){
        $i = 0;
        $pin = "";
        while($i < $digits){
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
    }
    
    public function register($data) {  
        $pin = $this->generatePIN();
        $data['temp_pin'] = $pin; 

        $this->db->insert('tbl_user', $data);
        if ($this->db->affected_rows() > 0) { 
            $id= $this->db->insert_id();
            $email = $data['user_email'];
            $name = $data['user_first_name']." ".$data['user_last_name'];  

            //$this->sendValidationEmail($pin, $email, $name);
            return true; 
        } else {
            return false;
        }
    }

    private function sendValidationEmail($pin,$email,$name){
        $this->load->library('email');
        $code = md5($email);
        $this->email->set_mailtype('html');
        $this->email->from('info@elite.co.uk');
        $this->email->to($email);
        $this->email->subject('Please activate your account');
        $message ='<!Doctype html> <html><head></head><body>';
        $message .='<p>Dear '.$name.'</p>';
        $message .='<p>Thank you for registering with Elite Private Staff!<br>Please <strong><a href="'.base_url('validateEmail').'/'.$pin.'/'.$role.'/'.$code.'">click here</a></strong> to activate your account.</p>';
        $message .='<p>Once again thank you for choosing Elite.</p><hr>';
        $message .='<p>Regards</p>';
        $message .='</body></html>';
        $this->email->message($message);
        if($this->email->send()){
            return true;
        }else{
            return false;
        }
    }

    public function validateEmail($pin,$code){
        $this->db->select('*');
        $this->db->from('tbl_user'); 
        $this->db->where("temp_pin",$pin); 
        $query = $this->db->get(); 
        
        if($query->num_rows()===1){
            $data = $query->row();
			if(md5($data->user_email)===$code){
                $result=$this->activateAccount($data->user_email);
                 
                if($result){
                    $this->db->select('*');
                    $this->db->from('tbl_user'); 
                    $this->db->where('user_email', $data->user_email);
                    $query = $this->db->get();
                    return $query->row();
                }
			}else
			echo "Error";
		}
    }
    
    private function activateAccount($email){
        $this->db->set('user_status', 1);
        $this->db->where('user_email', $email);
        $result = $this->db->update('tbl_user');
        return $result;        
    }

    public function sendEmailChangeEmail($pin,$newemail,$email){
        $this->load->library('email');
        $code = md5($email);
        $this->email->set_mailtype('html');
        $this->email->from('info@elite.co.uk');
        $this->email->to($email);
        $this->email->subject('Email Account Change');
        $message ='<!Doctype html> <html><head></head><body>';
        $message .='<p>Dear '.$email.'</p>';
        $message .='<p>An attempt to change the email associated with your account was made.<br>If you initidated this request, please <strong><a href="'.base_url('change-email').'/'.$pin.'">click here</a></strong> to confirm changing it to '.$newemail.'.</p>';
        $message .='<p>If you did not initiate this request, change your account password.</p><hr>';
        $message .='<p>Regards</p>';
        $message .='</body></html>';
        $this->email->message($message);
        if($this->email->send()){
            return true;
        }else{
            return false;
        }
    }
    
    private function sendPasswordResetEmail($pin,$email){
        $this->load->library('email');
        $code = md5($email);
        $this->email->set_mailtype('html');
        $this->email->from('info@proparcade.co.uk');
        $this->email->to($email);
        $this->email->subject('Password Reset Request');
        $message ='<!Doctype html> <html><head></head><body>';
        $message .='<p>Dear '.$email.'</p>';
        $message .='<p>We have received a Password Reset Request for this email.<br>Did not initiate this request? No need to worry, just ignore this email.<br>If you did initiate this request, please <strong><a href="'.base_url('reset-password').'/'.$pin.'/'.$code.'">click here</a></strong> to Reset your Password.</p>';
        $message .='<p>Once again thank you for choosing Proparcade.</p><hr>';
        $message .='<p>Regards</p>';
        $message .='</body></html>';
        $this->email->message($message);
        if($this->email->send()){
            return true;
        }else{
            return false;
        }
    }
    
    public function sendemail($email,$from,$name,$to="help@proparcade.co.uk"){
        $this->load->library('email');
        $this->email->set_mailtype('html');
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject('User Help Message');
        $message ='<!Doctype html> <html><head></head><body>';
        $message .='<p>From '.$name.'</p>';
        $message .='<p> '.$email.'</p><hr>';
        $message .='<p>Regards '.$name.'</p>';
        $message .='</body></html>';
        $this->email->message($message);
        if($this->email->send()){
            return true;
        }else{
            return false;
        }
    }
    
    public function reset_password($pin,$code){
        $this->db->select('*');
        $this->db->from('tbl_user'); 
        $this->db->where("temp_pin",$pin);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $data = $query->row();
            $email = $data->user_email;
            if(md5($email) == $code){
                return true;
            }
        } else {
            return false;
        }
    }
    
    public function recover_email($email){
        $pin = $this->generatePIN();
        $data['temp_pin'] = $pin;
        $this->db->where('user_email',$email);
        $this->db->update('tbl_user',$data);
        $result = $this->sendPasswordResetEmail($pin, $email);
        return $result;
    }
    
    public function set_new_password($password,$pin,$email) {
        $this->db->where('temp_pin',$pin);
        $this->db->where('user_email',$email);
        $data = array(
            'user_password' => $password
        );
        $result = $this->db->update('tbl_user',$data);
        return $result;
    }
    
    public function get_interested($id,$type){
        $this->db->select('*');
        $this->db->from('tbl_can_interested');
        if($type == 1){
            $this->db->where("candidate_user_id_fk",$id);
        }else{
            $this->db->where("parent_user_id_fk",$id);
        }
        $this->db->where("interested_status",0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result;
        }else{
            return false;
        }
    }
    
    public function mark_read($id,$type){
        $data['interested_status'] = 1;
        if($type == 1){
            $this->db->where("candidate_user_id_fk", $id);
        }else{
            $this->db->where("parent_user_id_fk", $id);
        }
        $this->db->update('tbl_can_interested', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else{
            return false;
        }
    }


    public function getActivities($user){
        $this->db->where('user_fk', $user);
        $query = $this->db->get('tbl_activityfeed');
        return $query->result_array();
    }

    public function getTopMessages($user){
        $this->db->select('tbl_messages.*, tbl_user.user_first_name, tbl_user.user_last_name');
        // $this->db->select('*');
        $this->db->from('tbl_messages'); 
        $this->db->where('sent_to', $user);
        $this->db->join('tbl_user', 'user_id = sent_by');
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->result_array();
    }
// $config['protocol']    = 'smtp';
// $config['smtp_host']    = 'ssl://smtp.gmail.com';
// $config['smtp_port']    = '465';
// $config['smtp_timeout'] = '7';
// $config['smtp_user']    = 'moazzam@weblinerz.co.uk';
// $config['smtp_pass']    = 'smoe05aS';
// $config['charset']    = 'utf-8';
// $config['newline']    = "\r\n";
// $config['mailtype'] = 'html'; // or text
// $config['validation'] = TRUE; // bool whether to validate email or not
}
?>