<?php
Class Admin_model extends CI_Model {
    
    public function __construct(){
        parent::__construct(); 
    }

    public function get_user_by_type($type=3) {
        $count = "SELECT COUNT(*) FROM listings WHERE posted_by = user_id";
        
        $this->db->select('tbl_user.*,('.$count.') as listings');
        $this->db->from('tbl_user');
        $this->db->where("tbl_user.user_type",$type);
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
            return $res;
        } else {
            return false;
        }
    } 

    public function change_user_status($id,$value="0"){ //block by default
        $data = array(
        "user_status" => $value
        );
        $this->db->where('user_id',$id);
        $result = $this->db->update('tbl_user',$data);
        return $result;
    }

    public function change_user_type($id,$value="0"){ //block by default
        $data = array(
        "user_type" => $value
        );
        $this->db->where('user_id',$id);
        $result = $this->db->update('tbl_user',$data);
        return $result;
    }

}