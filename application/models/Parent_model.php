<?php

Class Parent_model extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        ob_clean();
    }
    
    public function add_basic_info($data) { 
        $this->db->insert('tbl_parent_basic_info', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else{
            return false;
        } 
    }
    
    public function add_job_requirements($data) { 
        $this->db->insert('tbl_parent_job_requirement', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else{
            return false;
        } 
    }
    
    public function add_payment_details($data) { 
        $this->db->insert('tbl_parent_payment', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else{
            return false;
        } 
    }
    
    public function get_basic_info($id, $tag) { 
        $this->db->select('*');
        $this->db->from('tbl_parent_basic_info');
        $this->db->where("user_id_fk",$id);
        $this->db->where("tag",$tag);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result[0];
        }else{
            return false;
        } 
    }
    
    public function get_job_requirements($id, $tag) { 
        $this->db->select('*');
        $this->db->from('tbl_parent_job_requirement');
        $this->db->where("user_id_fk",$id);
        $this->db->where("tag",$tag);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result[0];
        }else{
            return false;
        } 
    }
    
    public function get_payment_details($id, $tag) { 
        $this->db->select('*');
        $this->db->from('tbl_parent_payment');
        $this->db->where("user_id_fk",$id);
        $this->db->where("tag",$tag);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result[0];
        }else{
            return false;
        } 
    }
    
    public function get_all_profiles($list = "") { 
        $this->db->select('tbl_user.temp_pin,tbl_user.user_created,tbl_user.user_last_logged_in, tbl_user.user_picture ,tbl_can_personal_details.can_state as currentlocation,tbl_can_personal_details.can_options as options,tbl_can_personal_details.can_country as nationality,tbl_user.user_id as id,tbl_can_personal_details.can_full_name as name');
        $this->db->from('tbl_can_personal_details');
        $this->db->join("tbl_user","tbl_can_personal_details.user_id_fk = tbl_user.user_id");
//         $this->db->where("user_id_fk",$id);
        $this->db->where("tbl_user.user_live",1);
        if($list != ""){
            $this->db->where("tbl_user.user_id IN ($list) ");
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result;
        }else{
            return false;
        } 
    }
    
    public function get_favourite_profiles($id) { 
        $this->db->select('*');
        $this->db->from('tbl_parent_favourite');
        $this->db->where("parent_user_id_fk",$id);
        $query = $this->db->get();
        $ids = array();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $res){
                array_push($ids, $res['candidate_user_id_fk']);
            }
            $list = rtrim(implode(',', $ids), ',');
            if($list != ""){
                $result = $this->get_all_profiles($list);
                return $result;
            }else{
                return $ids;
            }
        }else{
            return $ids;
        } 
    }
    
    public function get_my_jobs($id) { 
        $this->db->select('bi.*,jr.*');
        $this->db->where("bi.user_id_fk",$id);
        $this->db->where("bi.live_status",1);
        $this->db->from('tbl_parent_basic_info bi');
        $this->db->join("tbl_parent_job_requirement jr","bi.user_id_fk = jr.user_id_fk AND bi.tag = jr.tag");
        
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            if($result){
                return $result;
            }else{
                return false;
            }
        }else{
            return false;
        }  
    }
    
    public function change_job_live_status($tag,$value){  
        $data['live_status'] = $value;
        $this->db->where("tag", $tag);
        $this->db->update('tbl_parent_basic_info', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else{
            return false;
        } 
    }
    
    public function get_profile_info($id) { 
        $this->db->select('tbl_can_personal_details.*,tbl_can_documents.can_video');
        $this->db->from('tbl_can_personal_details');
        $this->db->where("tbl_can_personal_details.user_id_fk",$id);
        $this->db->join("tbl_can_documents","tbl_can_documents.user_id_fk = tbl_can_personal_details.user_id_fk");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result[0];
        }else{
            return false;
        } 
    }
    
    public function registration_status($data){
        $status = 1;
        $this->db->select('*');
        $this->db->from('tbl_parent_basic_info');
        $this->db->where("user_id_fk",$data['user_id_fk']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $status = 2;
            $this->db->select('*');
            $this->db->from('tbl_parent_job_requirement');
            $this->db->where("user_id_fk",$data['user_id_fk']);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $status = 3;
                $this->db->select('*');
                $this->db->from('tbl_parent_payment');
                $this->db->where("user_id_fk",$data['user_id_fk']);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    $status = 4;
                }else{
                    $status = 3;
                }
            }else{
                $status = 2;
            }
        }else{
            $status = 1;
        }
        return $status;
    }
    
    public function change_user_live_status($userid, $status){
        $data['user_live'] = $status;
        $this->db->where("user_id", $userid);
        $this->db->update('tbl_user', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else{
            return false;
        }
    }
    
    public function send_registration_email($data){
        $userid = $data['user_id'];
        $email = $data['user_email'];
        
        $this->load->library('email');
        $this->email->set_mailtype('html');
        $this->email->from('info@elite.co.uk');
        $this->email->to($email);
        $this->email->subject('Please activate your account');
        $message ='<!Doctype html> <html><head></head><body>';
        $message .='<p>Dear '.$email.'</p>';
        $message .='<p>Thank you for registering with Elite Private Staff! We are very pleased that you have chosen us for your Private Staff needs.<br>So what�s the next step?<br>Now that your documents have been validated by our agents, you can now make the &pound;19.99 payment and make your profile live.<br>Please <strong><a href="'.base_url('products/buy').'/'.$userid.'">click here</a></strong> to make the 19.99 payment.</p>';
        $message .='<p>Once again thank you for choosing Elite.</p><hr>';
        $message .='<p>Regards</p>';
        $message .='</body></html>';
        $this->email->message($message);
        if($this->email->send()){
            return true;
        }else{
            return false;
        }
    }
    
    public function toggle_favorite($candidate, $parent){
        $data['candidate_user_id_fk'] = $candidate;
        $data['parent_user_id_fk'] = $parent;
        $this->db->select('*');
        $this->db->from('tbl_parent_favourite');
        $this->db->where("parent_user_id_fk",$parent);
        $this->db->where("candidate_user_id_fk",$candidate);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $this->db->where("parent_user_id_fk",$parent);
            $this->db->where("candidate_user_id_fk",$candidate);
            $result = $this->db->delete('tbl_parent_favourite');
            return $result;
        }else{
            $this->db->insert('tbl_parent_favourite', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }else{
                return false;
            }
        }
    }
    
    public function check_favourite($candidate, $parent){
        $this->db->select('*');
        $this->db->from('tbl_parent_favourite');
        $this->db->where("parent_user_id_fk",$parent);
        $this->db->where("candidate_user_id_fk",$candidate);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return 1;
        }else{
            return 0;
        }
    }
    
    public function mark_notification_read($id,$type="user"){
        $data['interested_status'] = 1;
        if($type == "user"){
            $this->db->where("parent_user_id_fk", $id);
            $this->db->update('tbl_can_interested', $data);
        }else{
//             $this->db->where("id", $id);
//             $this->db->update('tbl_can_interested', $data);
        }
        if ($this->db->affected_rows() > 0) {
            return true;
        }else{
            return false;
        }
    }
    
}

?>