<?php

Class Candidate_model extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        ob_clean();
    }
    
    public function add_personal_details($data) {
        
        $this->db->insert('tbl_can_personal_details', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else{
            return false;
        }
        
    }
    
    public function add_job_requirements($data) {
        
        $this->db->insert('tbl_can_job_requirement', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else{
            return false;
        }
        
    }
    
    public function add_documents($data) {
        $this->db->select('*');
        $this->db->from('tbl_can_documents');
        $this->db->where("user_id_fk",$data['user_id_fk']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $this->db->where("user_id_fk",$data['user_id_fk']);
            $this->db->update('tbl_can_documents', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }else{
                return false;
            }
        }else{
            $this->db->insert('tbl_can_documents', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }else{
                return false;
            }
        }
        
    }
    
    public function add_video($data) {
        
        $data['can_video'] = "";
        $this->db->where("user_id_fk", $data['user_id_fk']);
        $this->db->update('tbl_can_documents', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else{
            return false;
        }
        
    }
    
    public function registration_status($data){
        $status = 1;
        $this->db->select('*');
        $this->db->from('tbl_can_personal_details');
        $this->db->where("user_id_fk",$data['user_id_fk']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $status = 2;
            $this->db->select('*');
            $this->db->from('tbl_can_job_requirement');
            $this->db->where("user_id_fk",$data['user_id_fk']);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $status = 3;
                $this->db->select('*');
                $this->db->from('tbl_can_documents');
                $this->db->where("user_id_fk",$data['user_id_fk']);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    $status = 4;
                    $res = $query->result_array();
                    if($res[0]['can_video'] != ""){
                        $status = 5;
                    }else{
                        $status = 4;
                    }
                }else{
                    $status = 3;
                }
            }else{
                $status = 2;
            }
        }else{
            $status = 1;
        }
        return $status;
    }
    
    public function change_user_live_status($userid, $status){
        $data['user_live'] = $status;
        $this->db->where("user_id", $userid);
        $this->db->update('tbl_user', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else{
            return false;
        }
    }
    
    public function check_live_status($userid){
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->where("user_id", $userid);
        $this->db->where("user_live", 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return true;
        }else{
            return false;
        }
    }
    
    public function get_jobs($list = ""){
        $this->db->select('bi.*,jr.*');
        $this->db->from('tbl_parent_basic_info bi');
        $this->db->join("tbl_parent_job_requirement jr","bi.user_id_fk = jr.user_id_fk AND bi.tag = jr.tag");
        $this->db->where("bi.live_status",1);
        if($list != ""){
            $this->db->where("bi.tag IN ($list) ");
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            if($result){
                $new = array();
                foreach ($result as $res){
                    $shortlisted = $this->check_shortlisted($this->session->userdata['user']['user_id'], $res['tag']);
                    $res['shortlisted'] = $shortlisted;
                    array_push($new, $res);
                }
                $result = $new;
                return $result;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public function search_jobs($data){
        $this->db->select('bi.*,jr.*');
        $this->db->from('tbl_parent_basic_info bi');
        $this->db->join("tbl_parent_job_requirement jr","bi.user_id_fk = jr.user_id_fk AND bi.tag = jr.tag");
        $this->db->where("bi.live_status",1);
        if($data['keyword'] != ""){
            $word = $data['keyword'];
            //$this->db->where("bi.tag IN ($list) ");
            $w1 = "bi.parent_job_title LIKE '%$word%'";
            $w2 = "jr.parent_job_description LIKE '%$word%'";
            $w3 = "jr.parent_job_requirements LIKE '%$word%'";
            $w4 = "jr.parent_benefits LIKE '%$word%'";
            $this->db->where("$w1 OR $w2 OR $w3 OR $w4");
        }
        if($data['category'] != ""){
            $this->db->where("bi.parent_position", $data['category']);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            if($result){
                $new = array();
                foreach ($result as $res){
                    $shortlisted = $this->check_shortlisted($this->session->userdata['user']['user_id'], $res['tag']);
                    $res['shortlisted'] = $shortlisted;
                    array_push($new, $res);
                }
                $result = $new;
                return $result;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public function get_shortlisted_jobs($id) {
        
        $this->db->select('*');
        $this->db->from('tbl_can_shortlisted');
        $this->db->where("candidate_user_id_fk",$id);
        $query = $this->db->get();
        $ids = array();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $res){
                array_push($ids, $res['tag_fk']);
            }
            if(count($ids) != 0){
                $list = "'" . implode("', '", $ids) ."'";
                $result = $this->get_jobs($list);
                return $result;
            }else{
                return $ids;
            }
        }else{
            return $ids;
        }
        
    }
    
    public function toggle_shortlisted($candidate, $tag){
        $data['candidate_user_id_fk'] = $candidate;
        $data['tag_fk'] = $tag;
        $this->db->select('*');
        $this->db->from('tbl_can_shortlisted');
        $this->db->where("tag_fk",$tag);
        $this->db->where("candidate_user_id_fk",$candidate);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $this->db->where("tag_fk",$tag);
            $this->db->where("candidate_user_id_fk",$candidate);
            $result = $this->db->delete('tbl_can_shortlisted');
            return $result;
        }else{
            $this->db->insert('tbl_can_shortlisted', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }else{
                return false;
            }
        }
    }
    
    public function show_interest($candidate, $tag){
        $data['candidate_user_id_fk'] = $candidate;
        $data['tag_fk'] = $tag;
        $this->db->select('*');
        $this->db->from('tbl_parent_basic_info');
        $this->db->where("tag",$tag);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $data['parent_user_id_fk'] = $result['user_id_fk'];
            $this->db->insert('tbl_can_interested', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }else{
                return false;
            }
        }else{
            
        }
    }
    
    public function check_shortlisted($candidate, $tag){
        $data['candidate_user_id_fk'] = $candidate;
        $data['tag_fk'] = $tag;
        $this->db->select('*');
        $this->db->from('tbl_can_shortlisted');
        $this->db->where("tag_fk",$tag);
        $this->db->where("candidate_user_id_fk",$candidate);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return 1;
        }else{
            return 0;
        }
    }
    
    public function send_registration_email($data){
        $userid = $data['user_id'];
        $email = $data['user_email'];
        
        $this->load->library('email');
        $this->email->set_mailtype('html');
        $this->email->from('info@elite.co.uk');
        $this->email->to($email);
        $this->email->subject('Please activate your account');
        $message ='<!Doctype html> <html><head></head><body>';
        $message .='<p>Dear '.$email.'</p>';
        $message .='<p>Thank you for registering with Elite Private Staff! We are very pleased that you have chosen us for your Private Staff needs.<br>So what�s the next step?<br>Now that your documents have been validated by our agents, you can now make the &pound;19.99 payment and make your profile live.<br>Please <strong><a href="'.base_url('products/buy').'/'.$userid.'">click here</a></strong> to make the 19.99 payment.</p>';
        $message .='<p>Once again thank you for choosing Elite.</p><hr>';
        $message .='<p>Regards</p>';
        $message .='</body></html>';
        $this->email->message($message);
        if($this->email->send()){
            return true;
        }else{
            return false;
        }
    }
    
}

?>